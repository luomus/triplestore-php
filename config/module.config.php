<?php
return [
    'service_manager' => [
        'aliases' => [
            'Triplestore\Service' => 'Triplestore\Oracle',
        ],
        'factories' => [
            'Triplestore\Oracle' => 'Triplestore\Factory\OracleFactory',
            'Triplestore\Options\ModuleOptions' => 'Triplestore\Factory\ModuleOptionsFactory',
            'Triplestore\Logger' => 'Triplestore\Factory\LoggerFactory',
            'Triplestore\Service\GeneratorService' => 'Triplestore\Factory\GeneratorFactory',
            'Triplestore\ObjectManager' => 'Triplestore\Factory\ObjectManagerFactory',
            'Triplestore\Db\Adapter\Array' => 'Triplestore\Db\Adapter\Factory\Oci8ArrayAdapterFactory',
            'Triplestore\Db\Adapter\Model' => 'Triplestore\Db\Adapter\Factory\Oci8ModelAdapterFactory',
        ],
        'invokables' => [

        ]
    ],
    'view_helpers' => [
        'invokables' => [
            'Metadata' => 'Triplestore\View\Helper\Metadata',
        ]
    ],
    'controllers' => [
        'invokables' => [
            'Triplestore\Controller\Generator' => 'Triplestore\Controller\GeneratorController'
        ]
    ],
    'console' => [
        'router' => [
            'routes' => [
                'triplestore-generate' => [
                    'options' => [
                        'route' => 'triplestore generate [classes|hydrators|context|form|test|base-test]:action',
                        'defaults' => [
                            'controller' => 'Triplestore\Controller\Generator'
                        ]
                    ]
                ]
            ]
        ]
    ]
];