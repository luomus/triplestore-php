<?php

namespace Triplestore\Controller;


use Triplestore\Db\Adapter\Oci8\ArrayResult;
use Triplestore\Db\Adapter\Oci8\ModelResult;
use Triplestore\Db\Adapter\Oci8\Result;
use Triplestore\Db\Adapter\Oci8\TripleSetResult;
use Triplestore\Db\Oracle;
use Triplestore\Model\TripleSet;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\HydratingResultSet;
use Zend\Mvc\Controller\AbstractConsoleController;
use Zend\ProgressBar\Adapter\Console;
use Zend\ProgressBar\ProgressBar;
use Zend\View\Model\ConsoleModel;

class GeneratorController extends AbstractConsoleController{

    public function classesAction() {
        /** @var \Triplestore\Service\ObjectManager $om */
        $om = $this->getServiceLocator()->get('Triplestore\ObjectManager');
        /** @var \Triplestore\Service\GeneratorService $generator */
        $generator = $this->getServiceLocator()->get('Triplestore\Service\GeneratorService');
        $classes = $om->getMetadataService()->getAllClasses();
        $progressBar = new ProgressBar(new Console(), 0, count($classes));
        $viewModel = new ConsoleModel();
        foreach ($classes as $class => $metadata) {
            $progressBar->next();
            $generator->generateClass($class, $metadata);
            $generator->generateHydrator($class, $metadata, false);
            $generator->generateHydrator($class, $metadata, true);
        }
        $progressBar->finish();
        $viewModel->setResult("Done\n");
        return $viewModel;
    }

    public function contextAction() {
        /** @var \Triplestore\Service\ObjectManager $om */
        $om = $this->getServiceLocator()->get('Triplestore\ObjectManager');
        /** @var \Triplestore\Service\GeneratorService $generator */
        $generator = $this->getServiceLocator()->get('Triplestore\Service\GeneratorService');
        $classes = $om->getMetadataService()->getAllClasses();
        $progressBar = new ProgressBar(new Console(), 0, count($classes));
        $viewModel = new ConsoleModel();
        foreach ($classes as $class => $metadata) {
            $progressBar->next();
            $generator->generateJsonLDContext($class, $metadata);
        }
        $progressBar->finish();
        $viewModel->setResult("Done\n");
        return $viewModel;
    }

    public function formsAction() {
        /** @var \Triplestore\Service\ObjectManager $om */
        $om = $this->getServiceLocator()->get('Triplestore\ObjectManager');
        $forms = $om->getFormService();
    }


    public function baseTestAction() {
        /** @var \Triplestore\Service\ObjectManager $om */
        $om = $this->getServiceLocator()->get('Triplestore\ObjectManager');
        $om->disableHydrator();

        /** @var Oracle $oracle */
        $oracle = $om->getConnection();
        $oracle->setUseResultSet(true);
        /** @var Adapter $adapter */
        $adapter = $this->serviceLocator->get('Triplestore\Db\Adapter\Array');

        $bench = 10;
        $counter = [];
        for($i = 0; $i < $bench; $i++) {
            $mark2 = microtime(true);
            $resultSet = $oracle->executeSelectSql("select SUBJECTNAME from rdf_statementview WHERE PREDICATENAME = 'MY.collectionID' AND OBJECTNAME = 'HR.407'");
            $mark3 = microtime(true);
            $cnt = 0;
            foreach($resultSet as $result) {
                //$resultSet = $adapter->query("'{$result['SUBJECTNAME']}'")->execute();
                //$model = $resultSet->current();
                $model = $om->fetch($result['SUBJECTNAME']);
                //var_dump($model);
                //break;
                /** @var $result TripleSet */
                //var_dump($result);
                //echo "\n New \n";
            }
            $mark4 = microtime(true);
            $counter[0][] = $mark3 - $mark2;
            $counter[1][] = $mark4 - $mark3;
            echo "Query time "   . ($mark3 - $mark2) . "\n";
            echo "Iterate time " . ($mark4 - $mark3) . "\n";
        }
        echo "Average query of $bench: " . (array_sum($counter[0]) / $bench) . "\n";
        echo "Average iterator of $bench: " . (array_sum($counter[1]) / $bench) . "\n";

        $viewModel = new ConsoleModel();
        $viewModel->setResult("\nDone\n");
        return $viewModel;

    }

    public function testAction() {
        $hydratingResultSet = new HydratingResultSet();
        $adapter = $this->serviceLocator->get('Triplestore\Db\Adapter\Array');

        $sql = "SELECT ROOTNAME FROM RDF_DOCTREE WHERE PREDICATENAME = 'MY.collectionID' AND OBJECTNAME = 'HR.407'";
        //$sql = "SELECT ROOTNAME FROM RDF_DOCTREE WHERE PREDICATENAME = 'rdf:type' AND OBJECTNAME = 'MY.document'";
        $bench = 10;
        $counter = [];
        for($i = 0; $i < $bench; $i++) {
            $mark2 = microtime(true);
            $resultSet = $adapter->query($sql)->execute();
            //$resultSet = $adapter->query("SELECT 'GV.2' as ROOTNAME from DUAL")->execute();
            //$resultSet = $adapter->query("select * from rdf_statementview where subjectname in ($sql) order by subjectid")->execute();
            //$resultSet = $adapter->query("select * from rdf_statementview_compact where subjectname in ($innerSql) order by subjectid")->execute();
            $mark3 = microtime(true);
            $cnt = 0;
            foreach($resultSet as $result) {
                //var_dump($result);
                //break;
                /** @var $result TripleSet */
                //var_dump($result);
                //echo "\n New \n";

            }
            $mark4 = microtime(true);
            $counter[0][] = $mark3 - $mark2;
            $counter[1][] = $mark4 - $mark3;
            echo "Query time "   . ($mark3 - $mark2) . "\n";
            echo "Iterate time " . ($mark4 - $mark3) . "\n";
        }
        echo "Average query of $bench: " . (array_sum($counter[0]) / $bench) . "\n";
        echo "Average iterator of $bench: " . (array_sum($counter[1]) / $bench) . "\n";

        $viewModel = new ConsoleModel();
        $viewModel->setResult("\nDone\n");
        return $viewModel;
    }



}