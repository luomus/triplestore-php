<?php

namespace Triplestore\Exception;

class BadMethodCallException extends \BadMethodCallException implements ExceptionInterface
{
}
