<?php

namespace Triplestore\Exception;

class UnexpectedValueException extends \UnexpectedValueException implements ExceptionInterface
{
}
