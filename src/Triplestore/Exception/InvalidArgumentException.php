<?php

namespace Triplestore\Exception;

class InvalidArgumentException extends \InvalidArgumentException implements ExceptionInterface
{
}
