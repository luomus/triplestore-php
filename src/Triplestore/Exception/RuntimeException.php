<?php

namespace Triplestore\Exception;

class RuntimeException extends \RuntimeException implements ExceptionInterface
{
}
