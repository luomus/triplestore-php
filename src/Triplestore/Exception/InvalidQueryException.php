<?php

namespace Triplestore\Exception;

class InvalidQueryException extends UnexpectedValueException implements ExceptionInterface
{
}
