<?php

namespace Triplestore\Exception;

class ErrorException extends \Exception implements ExceptionInterface
{
}
