<?php

namespace Triplestore\Factory;

use Triplestore\Service\GeneratorService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class GeneratorFactory is a factory class for the generator service
 * @package KotkaConsole\Service
 */
class GeneratorFactory implements FactoryInterface
{
    /**
     * Creates the service
     * @param ServiceLocatorInterface $serviceLocator
     * @return GeneratorService
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        /** @var \Triplestore\Options\ModuleOptions $moduleOptions */
        $moduleOptions = $serviceLocator->get('Triplestore\Options\ModuleOptions');
        $generatorConfig = $moduleOptions->getGenerator();
        $classTemplateFile     = realpath(__DIR__ . '/../Template/Class.tpl');
        $hydratorTemplateFile  = realpath(__DIR__ . '/../Template/Hydrator.tpl');
        $interfaceTemplateFile = realpath(__DIR__ . '/../Template/Interface.tpl');
        $classFolder           = realpath(__DIR__ . '/../Classes');
        $hydratorFolder        = realpath(__DIR__ . '/../Classes/Hydrator');
        $contextPath           = $this->getPath($generatorConfig, 'context', null);

        return new GeneratorService($classTemplateFile, $interfaceTemplateFile, $hydratorTemplateFile, $classFolder, $hydratorFolder, $contextPath);
    }

    private function getPath($config, $type, $default) {
        if (isset($config[$type]['path'])) {
            return realpath($config[$type]['path']);
        }
        return $default;
    }
}
