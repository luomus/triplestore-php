<?php

namespace Triplestore\Factory;

use Triplestore\Service\ObjectManager;
use Zend\Cache\StorageFactory;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class ObjectManagerFactory is a factory class for the object manager
 *
 * @package Triplestore\Service
 */
class ObjectManagerFactory implements FactoryInterface
{
    private $cache = [
        'adapter' => [
            'name' => 'filesystem',
            'options' => [
                'cache_dir'   => 'data/cache/triplestore',
                'key_pattern' => '/^[a-z0-9_\+\-\.]*$/Di',
                'ttl'         => 432000,
            ]
        ],
        'plugins' => [
            'exception_handler' => [
                'throw_exceptions' => false
            ],
            'serializer' => [
                'serializer' => 'Zend\Serializer\Adapter\PhpSerialize',
            ]
        ]
    ];

    /**
     * Creates the service
     * @param ServiceLocatorInterface $serviceLocator
     * @return ObjectManager
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        /** @var \Triplestore\Options\ModuleOptions $options */
        $options = $serviceLocator->get('Triplestore\Options\ModuleOptions');
        $db = $serviceLocator->get('Triplestore\Service');
        $cache = StorageFactory::factory($this->cache);

        return new ObjectManager($db, $options, $cache, $serviceLocator);
    }
}
