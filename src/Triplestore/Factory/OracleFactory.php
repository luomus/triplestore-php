<?php

namespace Triplestore\Factory;

use Triplestore\Db\Oracle;
use Triplestore\Options\ModuleOptions;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class OracleFactory is a factory class for the oracle database implementation
 * @package Triplestore\Db
 */
class OracleFactory implements FactoryInterface
{
    /**
     * Creates the service
     * @param ServiceLocatorInterface $serviceLocator
     * @return Oracle
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        /** @var ModuleOptions $options */
        $options = $serviceLocator->get('Triplestore\Options\ModuleOptions');
        $dbOptions = $options->getDatabase();
        $db   = $dbOptions->getDatabase();
        return new Oracle($dbOptions, $db);
    }
}
