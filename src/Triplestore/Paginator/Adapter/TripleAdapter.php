<?php

namespace Triplestore\Paginator\Adapter;

use Triplestore\Service\ObjectManager;
use Zend\Paginator\Adapter\AdapterInterface;

/**
 * Class TripleAdapter is paginator class for the triplestore directly
 *
 * @package Triplestore\Paginator\Adapter
 */
class TripleAdapter implements AdapterInterface {

    private $count;
    private $om;
    private $criteria;

    /**
     * Initializes the object
     *
     * @param ObjectManager $om
     * @param               $criteria
     */
    public function __construct(ObjectManager $om, $criteria) {
        $this->om = $om;
        $this->count = $om->count($criteria);
        $this->criteria = $criteria;
    }

    /**
     * Returns an collection of items for a page.
     *
     * @param  int $offset Page offset
     * @param  int $itemCountPerPage Number of items per page
     * @return array
     */
    public function getItems($offset, $itemCountPerPage)
    {
        return $this->om->findBy($this->criteria, null, $itemCountPerPage, $offset, array(ObjectManager::PREDICATE_TYPE));
    }

    /**
     * (PHP 5 &gt;= 5.1.0)<br/>
     * Count elements of an object
     * @link http://php.net/manual/en/countable.count.php
     * @return int The custom count as an integer.
     * </p>
     * <p>
     * The return value is cast to an integer.
     */
    public function count()
    {
        return $this->count;
    }
}