<?php

namespace Triplestore\Db;


class Criteria {

    protected $and = [];
    protected $or = [];

    /**
     * @return array
     */
    public function getAnd()
    {
        return $this->and;
    }

    /**
     * @param array $and
     */
    public function setAnd($and)
    {
        $this->and = $and;
    }

    /**
     * @return array
     */
    public function getOr()
    {
        return $this->or;
    }

    /**
     * @param array $or
     */
    public function setOr($or)
    {
        $this->or = $or;
    }
}