<?php

namespace Triplestore\Db;

class Sql {

    private $query;
    private $bind;

    /**
     * @return mixed
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * @param mixed $query
     */
    public function setQuery($query)
    {
        $this->query = $query;
    }

    /**
     * @return mixed
     */
    public function getBind()
    {
        return $this->bind;
    }

    /**
     * @param mixed $bind
     */
    public function setBind($bind)
    {
        $this->bind = $bind;
    }



}