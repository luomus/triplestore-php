<?php

namespace Triplestore\Db;

use Common\Service\IdService;
use Common\Db\SequenceGeneratorInterface;
use Triplestore\Exception;
use Triplestore\Model\Model;
use Triplestore\Model\TripleObject;
use Triplestore\Model\Predicate;
use Triplestore\Service\MetadataService;
use Triplestore\Service\ObjectManager;
use Zend\Db\Adapter\Driver\Oci8\Result;
use Zend\Db\Adapter\Driver\ResultInterface;

/**
 * Class Oracle is triplestore backend for Oracle
 *
 * @package Triplestore\Db
 */

/** @noinspection PhpDocSignatureInspection */
class Oracle implements TriplestoreInterface, SequenceGeneratorInterface {

    const MAX_DEPTH     = 3;
    const MAX_IN_CLAUSE = 1000;

    private static $conn;
    /** @var \Triplestore\Options\DatabaseOptions */
    private static $options;
    private static $inIdx     = 0;
    private static $stmtCache = array();

    private $keepHistory      = true;
    private $modelHistory     = array();
    private $database;

    private static $cnt       = 0;
    private $inTransaction    = false;
    private $executionFailed  = false;
    private $useResultSet     = false;

    /**
     * Initializes the Oracle class
     *
     * @param $options
     * @param $database
     */
    public function __construct($options, $database)
    {
        $this->database = $database;
        self::$options = $options;
    }

    private static function getConnection() {
        if (self::$conn === null) {
            self::$conn = oci_connect(
                self::$options->getUsername(),
                self::$options->getPassword(),
                self::$options->getHost(), 'AL32UTF8'
            );
            if (self::$conn === false) {
                throw new Exception\RuntimeException("Could not connect to triplestore.");
            }
        }
        return self::$conn;
    }

    /**
     * Starts transaction
     */
    public function startTransaction() {
        $this->inTransaction = true;
        $this->executionFailed = false;
    }

    /**
     * @return string
     */
    public function getDatabase()
    {
        return $this->database;
    }

    /**
     * Saves the model to database.
     *
     * @param Model $model
     * @param string $user qname of the user
     * @param bool $autocommit
     * @param bool $deep
     * @return bool|mixed
     * @throws \Triplestore\Exception\InvalidArgumentException
     * @throws \Triplestore\Exception\InvalidQueryException
     */
    public function store(Model $model, $user = null, $autocommit = false, $deep = false)
    {
        self::$cnt++;
        $addSql   = "INSERT INTO ".$this->database.".rdf_statement_cache(SubjectName,PredicateName,ObjectName,ObjectLiteral,ObjectLangCode) SELECT * FROM (";
        $count    = 0;
        $triplets = [];
        $dels     = [];
        $this->processModel($model, $triplets, $addSql, $dels, $count, $deep);
        $addSql .= ")";
        $stmtKey = 'saveModel' . $count;
        if (!isset(self::$stmtCache[$stmtKey])) {
            self::$stmtCache[$stmtKey]  = oci_parse(self::getConnection(), $addSql);
        }
        $stid = self::$stmtCache[$stmtKey];
        foreach ($triplets as $key => $triple) {
            oci_bind_by_name($stid, ':subject' . $key, $triple['subject']);
            oci_bind_by_name($stid, ':predicate' . $key, $triple['predicate']);
            oci_bind_by_name($stid, ':object' . $key, $triple['object']);
            oci_bind_by_name($stid, ':objectLiteral' . $key, $triple['objectLiteral']);
            oci_bind_by_name($stid, ':objectLangCode' . $key, $triple['objectLangCode']);
        }

        $this->clearCache();                 // Empties the statement cache
        $insert = $this->execute($stid);     // Inserts new statements to the statement cache
        //$this->showCache();                // var_dumps the content of the statement cache
        //$this->persistStatementCache();    // Copies the content of the cache to temp_statement_cache table
        $link   = $deep ? 'MY.isPartOf' : null;
        $commit = $this->commitCache($user, $link); // Calls the commit statement cache

        return ($insert && $commit);
    }

    private function persistStatementCache() {
        $sql = 'TRUNCATE TABLE '.$this->database.'.TEMP_STATEMENT_CACHE';
        $truncateStmt = oci_parse(self::getConnection(), $sql);
        $this->execute($truncateStmt);
        $sql = 'INSERT INTO '.$this->database.'.TEMP_STATEMENT_CACHE(SubjectName,PredicateName,ObjectName,ObjectLiteral,ObjectLangCode, ContextName)  SELECT * FROM rdf_statement_cache';
        $insertStmt = oci_parse(self::getConnection(), $sql);
        $this->execute($insertStmt);
    }

    private function inClause($field, array $in, $not = false) {
        $inCount = count($in);
        $bind    = [];
        if ($inCount == 0) {
            return ['sql' => ' 1 == 0', 'bind' => []];
        }
        if ($inCount == 1) {
            $start = $not ? '!=' : '=';
            $end = '';
        } else {
            $start = $not ? 'NOT IN (' : 'IN (';
            $end = ')';
        }
        $sql = ' ' . $field . ' ' .  $start;
        if ($inCount > self::MAX_IN_CLAUSE) {
            $parts = array_chunk($in, self::MAX_IN_CLAUSE);
            foreach ($parts as $key => $value) {
                $parts[$key] = $this->in($value, $bind);
            }
            if ($not) {
                $sql .= implode(') AND ' . $field . ' NOT IN (', $parts) . ')';
            } else {
                $sql .= implode(') OR ' . $field . ' IN (', $parts) . ')';
            }
        } else {
            $sql .= $this->in($in, $bind);
            $sql .= $end;
        }
        return ['sql' => $sql, 'bind' => $bind];
    }

    private function in($in, &$bind) {
        $sql = [];
        foreach($in as $value) {
            $idx   = self::$inIdx ++;
            $sql[] = ':i' . $idx;
            $bind['i'.$idx] = $value;
        }
        return implode(', ',$sql);
    }

    /**
     * Deletes the model from the triplestore
     *
     * @param Model $model
     *
     * @return bool
     */
    public function remove(Model $model) {
        $delSql  = "DELETE FROM ".$this->database.".RDF_STATEMENT WHERE STATEMENTID IN (%s)";
        $stmtIds =  array_keys($this->extractStatementIds($model));

        if (count($stmtIds) == 0) {
            return true;
        }

        $delSql = sprintf($delSql, implode(', ', $stmtIds));
        $stmt = oci_parse(self::getConnection(), $delSql);
        $this->execute($stmt);

        return true;
    }

    /**
     * Returns all the statement ids used in the model and it's children (if they are connected to the model)
     *
     * @param Model $model
     * @param array $ids
     *
     * @return array
     */
    private function extractStatementIds(Model $model, array & $ids = null) {
        if ($ids === null) {
            $ids = array();
        }
        foreach ($model as $predicate) {
            foreach ($predicate as $object) {
                /** @var TripleObject $object */
                $value = $object->getValue();
                if ($value instanceof Model) {
                    $this->extractStatementIds($value, $ids);
                }
                $stmtId = $object->getStatementId();
                if ($stmtId !== null && $stmtId !== '') {
                    $ids[$stmtId] = true;
                }
            }
        }

        return $ids;
    }

    /**
     * Commits the transactions
     */
    public function commit() {
        $this->inTransaction = false;
        if (!$this->executionFailed) {
            oci_commit(self::getConnection());
        }
        $this->executionFailed = false;
    }

    /**
     * Clears the local cache
     */
    public function clear() {
        $this->modelHistory = array();
    }

    /**
     * Fetches one model based on the qname given
     *
     * @param $subject
     * @param $deep
     * @return Model|null
     * @throws \Triplestore\Exception\InvalidQueryException
     */
    public function fetch($subject, $deep = false)
    {
        self::$inIdx = 0;
        $isList  = is_array($subject);
        if ($deep) {
            if (is_string($subject)) {
                $models = $this->findSingleDeep($subject);
                return array_pop($models);
            }
            if ($isList) {
                return $this->findDeep($subject);
            }
        }
        $subject = $isList ? $subject : [$subject];
        $sql = "SELECT STATEMENTID, SUBJECTNAME, PREDICATENAME, OBJECTNAME, RESOURCELITERAL, LANGCODEFK
                FROM ".$this->database.".RDF_STATEMENTVIEW
                WHERE %s";
        $in = $this->inClause('SUBJECTNAME', $subject);
        $sql = sprintf($sql, $in['sql']);

        $stid = $this->prepareStatement($sql, $in['bind'], !$isList);
        $this->execute($stid);
        $models = $this->populateModel($stid);

        if (count($models) > 0) {
            if (!$isList) {
                $models = reset($models);
            }
            return $models;
        }
        return null;
    }

    private function prepareStatement($sql, array $bind = [], $remember = true) {
        $stmtKey = md5($sql);
        if ($remember) {
            if (!isset(self::$stmtCache[$stmtKey])) {
                self::$stmtCache[$stmtKey] = oci_parse(self::getConnection(), $sql);
            }
            $stid = self::$stmtCache[$stmtKey];
        } else {
            $stid = oci_parse(self::getConnection(), $sql);
        }

        foreach($bind as $key => $value) {
            $$key = $value;
            oci_bind_by_name($stid, ':'.$key, $$key);
        }
        return $stid;
    }

    /**
     * This fetches models that match the given criteria
     *
     * @param array|Criteria $criteria
     * @param array $orderBy
     * @param int $limit
     * @param int $offset
     * @param array $fields
     * @return array|null
     * @throws \Triplestore\Exception\InvalidQueryException
     */
    public function findBy($criteria, array $orderBy = null, $limit = null, $offset = null, array $fields = null) {
        $sql = "SELECT STATEMENTID, SUBJECTNAME, PREDICATENAME, OBJECTNAME, RESOURCELITERAL, LANGCODEFK
                    FROM ".$this->database.".RDF_STATEMENTVIEW
                    WHERE SUBJECTNAME IN (%s)";
        $bounds = array();
        $inner = $this->getCriteriaSql($criteria, $bounds);
        if (empty($inner)) {
            return null;
        }
        $sql   = sprintf($sql, $inner);
        if (is_array($fields) && count($fields) > 0) {
            $list = $this->toStrList($fields);
            $sql .= sprintf(" AND PREDICATENAME IN (%s)", $list);
        }
        $sql  .= ' ORDER BY STATEMENTID';

        if ($offset !== null || $limit !== null) {
            $minRow = (int) $offset;
            $maxRow = (int) $limit + $offset;
            $offsetSql = "select *
                              from
                            ( select rownum rnum, a.*
                                from (%s) a
                               where rownum <= $maxRow )
                            where rnum >= $minRow";
            $sql = sprintf($offsetSql, $sql);
        }

        $stid  = oci_parse(self::getConnection(), $sql);
        foreach ($bounds as $key => $value) {
            if (isset($value['predicate'])) {
                oci_bind_by_name($stid, ':key'. $key, $value['predicate']);
            }
            if (array_key_exists('object', $value)) {
                oci_bind_by_name($stid, ':value'. $key, $value['object']);
            }
        }
        $this->execute($stid);
        return $this->populateModel($stid);
    }

    /**
     * Returns sql string that will return subjects of all those that match the given
     * criteria
     *
     * @param array|Criteria $criteria
     * @param $bounds
     * @return string
     * @throws \Exception
     */
    private function getCriteriaSql($criteria, &$bounds) {
        $cnt = 0;
        if ($criteria instanceof Criteria) {
            $sql = $this->processCriteria($criteria->getAnd(), $bounds, $cnt);
            $sql .= $this->processCriteria($criteria->getOr(), $bounds, $cnt, 'UNION');
        } else if (is_array($criteria)) {
            $sql = $this->processCriteria($criteria, $bounds, $cnt);
        } else {
            throw new \Exception('Invalid criteria given.');
        }
        return $sql;
    }

    private function processCriteria($criteria, &$bounds, &$cnt, $type = 'INTERSECT') {
        $wheres = [];
        $start  = $cnt;
        foreach ($criteria as $key => $value) {
            if (is_array($value)) {
                foreach($value as $val) {
                    $wheres[]     = "SELECT DISTINCT SUBJECTNAME FROM ".$this->database.".RDF_STATEMENTVIEW WHERE PREDICATENAME = :key{$cnt} AND (RESOURCELITERAL = :value{$cnt} OR OBJECTNAME = :value{$cnt})";
                    $bounds[$cnt] = array('predicate' => $key, 'object' => $val);
                    $cnt++;
                }
            } else {
                if ($value === ObjectManager::ANY_OBJECT_VALUE) {
                    $wheres[]     = "SELECT DISTINCT SUBJECTNAME FROM ".$this->database.".RDF_STATEMENTVIEW WHERE PREDICATENAME = :key{$cnt}";
                    $bounds[$cnt] = array('predicate' => $key);
                } else {
                    $wheres[]     = "SELECT DISTINCT SUBJECTNAME FROM ".$this->database.".RDF_STATEMENTVIEW WHERE PREDICATENAME = :key{$cnt} AND (RESOURCELITERAL = :value{$cnt} OR OBJECTNAME = :value{$cnt})";
                    $bounds[$cnt] = array('predicate' => $key, 'object' => $value);
                }
                $cnt++;
            }
        }
        if ($cnt > $start) {
            return '(' . implode(' ' . $type . ' ', $wheres) . ')';
        }
        return '';
    }

    public function fetchFromHistory($subject, $timestamp, $class = null, MetadataService $metadataService = null) {
        $sql = "WITH A AS (
                    SELECT STATEMENTID, SUBJECTNAME, PREDICATENAME, OBJECTNAME, RESOURCELITERAL, LANGCODEFK, created, deleted
                      FROM ".$this->database.".RDF_STATEMENTVIEW_HISTORY
                      WHERE SUBJECTNAME = :subject
                    UNION ALL
                    SELECT STATEMENTID, SUBJECTNAME, PREDICATENAME, OBJECTNAME, RESOURCELITERAL, LANGCODEFK, created, null as deleted
                      FROM ".$this->database.".RDF_STATEMENTVIEW
                      WHERE SUBJECTNAME = :subject
                )
                SELECT STATEMENTID, SUBJECTNAME, PREDICATENAME, OBJECTNAME, RESOURCELITERAL, LANGCODEFK, TO_CHAR(created,'YYYYMMDDHH24MISS') as created, TO_CHAR(deleted,'YYYYMMDDHH24MISS') as deleted
                  FROM a
                  WHERE created <= TO_DATE(:timestamp,'YYYYMMDDHH24MISS')
                    AND (deleted >= TO_DATE(:timestamp,'YYYYMMDDHH24MISS') OR deleted IS NULL)
                  ORDER BY STATEMENTID";
        $stid = oci_parse(self::getConnection(), $sql);
        oci_bind_by_name($stid, ':subject', $subject);
        oci_bind_by_name($stid, ':timestamp', $timestamp);
        $this->execute($stid);
        $models = $this->populateModel($stid);
        /** @var Model $model */
        $model = reset($models);

        if (!$model instanceof Model) {
            return null;
        }

        if ($class !== null) {
            $metadata = $metadataService->getMetadataFor($class);
            $children = $metadata->getChildrenWithPredicate();
            foreach ($children as $childClass => $predicate) {
                $childData = $this->getHistoryChildren($subject, $predicate, $timestamp);
                $predicate = $model->getOrCreatePredicate($childClass);
                foreach ($childData as $childSubject => $value) {
                    if (in_array($childClass, $this->checkSubjectType($childSubject))) {
                        $data = $this->fetchFromHistory($childSubject, $timestamp, $childClass, $metadataService);
                        if ($data instanceof Model && $data->count() > 0) {
                            $predicate->addObject(new TripleObject($data));
                        }
                    }
                }

            }
        }

        return $model;
    }

    public function getHistoryVersions($subject, $class = null, array $exclude = null, MetadataService $metadataService = null, array $data = []) {
        self::$inIdx = 0;
        $sql = "SELECT  TO_CHAR(created,'YYYYMMDD') as created_date,
                        TO_CHAR(created,'HH24MISS') as created_time,
                        TO_CHAR(created + 1/86400,'YYYYMMDDHH24MISS') as created_whole,
                        TO_CHAR(deleted,'YYYYMMDD') as deleted_date,
                        TO_CHAR(deleted,'HH24MISS') as deleted_time,
                        TO_CHAR(deleted + 1/86400,'YYYYMMDDHH24MISS') as deleted_whole
                  FROM ".$this->database.".RDF_STATEMENTVIEW_HISTORY
                  WHERE SUBJECTNAME = :subject
                    %s
                UNION ALL
                SELECT  TO_CHAR(created,'YYYYMMDD') as created_date,
                        TO_CHAR(created,'HH24MISS') as created_time,
                        TO_CHAR(created + 1/86400,'YYYYMMDDHH24MISS') as created_whole,
                        null as deleted_date, null as deleted_time, null as deleted_whole
                  FROM ".$this->database.".RDF_STATEMENTVIEW
                  WHERE SUBJECTNAME = :subject
                    %s";
        if (is_array($exclude)) {
            $in = $this->inClause('PREDICATENAME', $exclude, true);
            $sql = sprintf($sql, ' AND ' . $in['sql'], ' AND ' . $in['sql']);
            $bind = $in['bind'];
        } else {
            $bind = [];
            $sql = sprintf($sql, '', '');
        }
        $bind['subject'] = $subject;
        $stid = $this->prepareStatement($sql, $bind);
        $this->execute($stid);
        while ($row = oci_fetch_array($stid)) {
            $key = $row['CREATED_DATE'];
            $data[$key][(int)$row['CREATED_TIME']] = [$row['CREATED_TIME'], $row['CREATED_WHOLE']];
            if (isset($row['DELETED_DATE'])) {
                $key = $row['DELETED_DATE'];
                $data[$key][(int)$row['DELETED_TIME']] = [$row['DELETED_TIME'], $row['DELETED_WHOLE']];
            }
        }
        $result = [];
        foreach ($data as $day => $dValue) {
            foreach($dValue as $intVal => $data) {
                if (!isset($dValue[($intVal  + 1)])) {
                    $whole = $day . $data[0];
                    $result[$data[1]] = $whole;
                }
            }
        }

        return $result;
    }

    private function getHistoryChildren($subject, $predicate, $timestamp = null) {
        $sql = "WITH A AS (
                    SELECT SUBJECTNAME, STATEMENTID, created, deleted
                      FROM ".$this->database.".RDF_STATEMENTVIEW_HISTORY
                      WHERE OBJECTNAME = :subject
                        AND PREDICATENAME = :predicate
                    UNION ALL
                    SELECT SUBJECTNAME, STATEMENTID, created, null as deleted
                      FROM ".$this->database.".RDF_STATEMENTVIEW
                      WHERE OBJECTNAME = :subject
                        AND PREDICATENAME = :predicate
                )
                SELECT SUBJECTNAME, STATEMENTID, created, TO_CHAR(deleted,'YYYYMMDDHH24MISS') as deleted
                  FROM A";
        if ($timestamp !== null) {
            $sql .= " WHERE created <= TO_DATE(:timestamp,'YYYYMMDDHH24MISS')
                        AND (deleted >= TO_DATE(:timestamp,'YYYYMMDDHH24MISS') OR deleted IS NULL) ORDER BY STATEMENTID";
            $stid = oci_parse(self::getConnection(), $sql);
            oci_bind_by_name($stid, ':subject', $subject);
            oci_bind_by_name($stid, ':predicate', $predicate);
            oci_bind_by_name($stid, ':timestamp', $timestamp);
        } else {
            $sql .= " ORDER BY STATEMENTID";
            $stid = oci_parse(self::getConnection(), $sql);
            oci_bind_by_name($stid, ':subject', $subject);
            oci_bind_by_name($stid, ':predicate', $predicate);
        }

        $this->execute($stid);
        $data = [];

        while ($row = oci_fetch_array($stid)) {
            $data[$row['SUBJECTNAME']] = ['DELETED' => $row['DELETED']];
        }

        return $data;
    }

    private function checkSubjectType($subject) {
        $sql = "WITH A AS (
                    SELECT OBJECTNAME
                      FROM ".$this->database.".RDF_STATEMENTVIEW_HISTORY
                      WHERE SUBJECTNAME = :subject
                        AND PREDICATENAME = 'rdf:type'
                    union all
                    SELECT OBJECTNAME
                      FROM ".$this->database.".RDF_STATEMENTVIEW
                      WHERE SUBJECTNAME = :subject
                        AND PREDICATENAME = 'rdf:type'
                )
                SELECT OBJECTNAME
                  FROM A";
        $stid = oci_parse(self::getConnection(), $sql);
        oci_bind_by_name($stid, ':subject', $subject);
        $this->execute($stid);
        $data = [];
        while ($row = oci_fetch_array($stid)) {
            $data[$row['OBJECTNAME']] = $row['OBJECTNAME'];
        }
        return $data;

    }

    /**
     * Returns the count that match the criteria
     *
     * @param array $criteria
     *
     * @return int|null
     * @throws \Triplestore\Exception\InvalidQueryException
     */
    public function count(array $criteria){
        if (count($criteria) > 1) {
            throw new Exception\InvalidQueryException("Only one criteria can be used at the moment!");
        }
        $sql = "SELECT COUNT(*)
                    FROM ".$this->database.".RDF_STATEMENTVIEW
                    WHERE %s";
        $wheres = array();
        $bounds = array();
        if (count($criteria) > 0) {
            $cnt = 0;
            foreach ($criteria as $key => $value) {
                $wheres[]     = "(PREDICATENAME = :key{$cnt} AND (OBJECTNAME = :value{$cnt} OR RESOURCELITERAL = :value{$cnt}))";
                $bounds[$cnt] = array('predicate' => $key, 'object' => $value);
                $cnt++;
            }
        }

        if (empty($wheres)) {
            return null;
        }
        $inner = implode(' AND ', $wheres);
        $sql   = sprintf($sql, $inner);
        $stid  = oci_parse(self::getConnection(), $sql);
        foreach ($bounds as $key => $value) {
            oci_bind_by_name($stid, ':key'. $key, $value['predicate']);
            oci_bind_by_name($stid, ':value'. $key, $value['object']);
        }
        $this->execute($stid);
        $row = oci_fetch_array($stid);
        return $row[0];
    }

    /**
     * Dumps the content of the cache
     */
    public function showCache() {
        $sql = "select * from ".$this->database.".rdf_statement_cache";
        $stid = oci_parse(self::getConnection(), $sql);
        if (!oci_execute($stid)) {
            $e = oci_error($stid);
            throw new Exception\InvalidQueryException($e['message'], $e['code']);
        }
        while ($row = oci_fetch_assoc($stid)) {
            var_dump($row);
        }
    }

    /**
     * Clears the commit cache
     */
    public function clearCache()
    {
        if (!isset(self::$stmtCache['clearCache'])) {
            $sql = "BEGIN ".$this->database.".ClearStatementCache(); END;";
            self::$stmtCache['clearCache'] = oci_parse(self::getConnection(), $sql);
        }

        $this->execute(self::$stmtCache['clearCache']);
    }

    /**
     * Checks to see if there are commit cache has valid data.
     * If there would be a predicate or object that isn't yet resource this will return a string with the missed fields.
     *
     * @return string
     */
    public function checkCacheIntegrity() {
        $sql = "SELECT SUBJECTNAME,PREDICATENAME,OBJECTNAME,OBJECTLITERAL,OBJECTLANGCODE from ".$this->database.".rdf_statement_cache";
        $stid = oci_parse(self::getConnection(), $sql);
        $this->execute($stid);
        $errors = array();
        $subjects = array();
        while ($row = oci_fetch_assoc($stid)) {
            $subject = $row['SUBJECTNAME'];
            if (!isset($subjects[$subject])) {
                if (!$this->hasResource($subject)) {
                    $errors[] = 'subject: "' . $subject . '"';
                }
                $subjects[$subject] = true;
            }
            if (!$this->hasResource($row['PREDICATENAME'])) {
                $errors[] = 'Missing predicate: "' . $row['PREDICATENAME'] . '"';
            }
            if ($row['OBJECTNAME'] !== null && !$this->hasResource($row['OBJECTNAME'])) {
                $errors[] = 'Missing object resource "' . $row['OBJECTNAME'] . '" in predicate "' .$row['PREDICATENAME'] . '"';
            }
            if ($this->hasStatement($row['SUBJECTNAME'], $row['PREDICATENAME'], $row['OBJECTNAME'], $row['OBJECTLITERAL'], $row['OBJECTLANGCODE'] )) {
                $errors[] = 'Duplicate statement! in subject "' . $row['SUBJECTNAME'] . '"';
            }
        }
        if (count($errors) > 0) {
            return implode(' and ', $errors);
        }
        return '';
    }

    /**
     * Checks if the statement exist
     *
     * @param string $subject
     * @param string $predicate
     * @param string|null $objectName
     * @param string|null $objectLiteral
     * @param string|null $objectLang
     *
     * @return bool
     */
    public function hasStatement($subject, $predicate, $objectName, $objectLiteral, $objectLang) {
        if (!isset(self::$stmtCache['hasStatement'])) {
            $sql = "SELECT 1 FROM ".$this->database.".rdf_statementview WHERE
                        SUBJECTNAME = :subject AND
                        PREDICATENAME = :pred AND
                        OBJECTNAME = :res AND
                        RESOURCELITERAL = :lit AND
                        LANGCODEFK = :lang";
            self::$stmtCache['hasStatement'] = oci_parse(self::getConnection(), $sql);
        }
        $stmt = self::$stmtCache['hasStatement'];
        oci_bind_by_name($stmt, ':subject', $subject);
        oci_bind_by_name($stmt, ':pred', $predicate);
        oci_bind_by_name($stmt, ':res', $objectName);
        oci_bind_by_name($stmt, ':lit', $objectLiteral);
        oci_bind_by_name($stmt, ':lang', $objectLang);

        return $this->has($stmt);
    }

    /**
     * Checks that the resource exists
     *
     * @param $resourceName
     *
     * @return bool
     */
    public function hasResource($resourceName)
    {
        if (!isset(self::$stmtCache['hasResource'])) {
            $sql = "SELECT 1 FROM ".$this->database.".RDF_RESOURCE WHERE RESOURCENAME = :r1";
            self::$stmtCache['hasResource'] = oci_parse(self::getConnection(), $sql);
        }
        $stmt = self::$stmtCache['hasResource'];
        oci_bind_by_name($stmt, ':r1', $resourceName);

        return $this->has($stmt);
    }

    /**
     * This checks that the database has data with the given statement
     * Used to check that resource or statement exists
     *
     * @param $stmt
     *
     * @return bool
     */
    private function has($stmt) {
        $this->execute($stmt);
        $row = oci_fetch_array($stmt);
        return (isset($row[0]) && '1' === $row[0]);
    }

    /**
     * Adds new resource to the database
     *
     * @param string $resource
     *
     * @return bool
     */
    public function addResource($resource)
    {
        if (!isset(self::$stmtCache['addResource'])) {
            $sql = "BEGIN ".$this->database.".addResource(:subject); END;";
            self::$stmtCache['addResource']  = oci_parse(self::getConnection(), $sql);
        }
        oci_bind_by_name(self::$stmtCache['addResource'], ':subject', $resource);
        $this->execute(self::$stmtCache['addResource']);

        return true;
    }

    /**
     * This is for generating the sequences and this implements the SequenceGeneratorInterface
     *
     * @param $namespace
     *
     * @return string
     */
    public function getNextSeqVal($namespace)
    {
        $sql = "SELECT ".$this->database.".rdf_{$namespace}_seq.nextval FROM DUAL";
        $stid = oci_parse(self::getConnection(),$sql);
        $this->execute($stid);
        $row = oci_fetch_array($stid);

        return $row[0];
    }

    /**
     * Execute custom sql
     *
     * @param $sql
     * @param $bind;
     * @param $onlySubject
     * @param $limit
     * @return array|ResultInterface
     */
    public function executeSelectSql($sql, array $bind = [], $onlySubject = false, $limit = false) {
        $stid = oci_parse(self::getConnection(),$sql);
        foreach ($bind as $key => $value) {
            $$key = $value;
            oci_bind_by_name($stid, ':'.$key, $$key);
        }
        $this->execute($stid);
        $data = [];
        $cnt  = 0;
        if ($this->useResultSet) {
            $resultSet = new Result();
            $resultSet->initialize($stid);

            return $resultSet;
        }

        while ($row = oci_fetch_array($stid)) {
            $cnt++;
            if ($limit !== false && $cnt > $limit) {
                break;
            }
            $data[] = $onlySubject ? $row['SUBJECTNAME'] : $row;
        }
        oci_free_statement($stid);

        return $data;
    }

    /**
     * Executes the statements and handles rolling back if in transaction
     *
     * @param $stid
     *
     * @return bool
     * @throws \Triplestore\Exception\InvalidQueryException
     */
    private function execute($stid) {
        $mode = $this->inTransaction ? OCI_NO_AUTO_COMMIT : OCI_COMMIT_ON_SUCCESS;
        try {
            if (!oci_execute($stid, $mode)) {
                $e = oci_error($stid);
                throw new Exception\InvalidQueryException($e['message'], $e['code']);
            }
        } catch (\Exception $e) {
            $this->clear();
            $reason = $this->getReasonForFailure();
            if ($this->inTransaction) {
                oci_rollback(self::getConnection());
            }
            $e = oci_error($stid);
            if ($reason) {
                $e['message'] .= "\n" . $reason;
            }
            throw new Exception\InvalidQueryException($e['message'], $e['code']);
        }
        return true;
    }

    /**
     * Tries to get the reason fro the failure
     * @return string
     */
    private function getReasonForFailure() {
        if ($this->executionFailed === false) {
            $this->executionFailed = true;
            return $this->checkCacheIntegrity();
        }
        return '';
    }

    /**
     * Commits the statement cache by calling CommitStatementCache3 procedure in the oracle database.
     *
     * @param string|null $userQName
     * @param string|null $link
     *
     * @return bool
     */
    private function commitCache($userQName = null, $link = null)
    {
        if (!isset(self::$stmtCache['commitCache'])) {
            $sql = "BEGIN ".$this->database.".CommitStatementCache_KOTKA(:userQname, :link); END;";
            self::$stmtCache['commitCache'] = oci_parse(self::getConnection(), $sql);
        }
        oci_bind_by_name(self::$stmtCache['commitCache'], ':userQname', $userQName);
        oci_bind_by_name(self::$stmtCache['commitCache'], ':link',      $link);
        return $this->execute(self::$stmtCache['commitCache']);
    }

    /**
     * This finds the children that should be linked to the object
     *
     * @param array  $linker
     * @param array  $parents
     * @param string $parentPredicate
     * @param int    $level
     */
    private function findHas(array $linker, array $parents, $parentPredicate = ObjectManager::PREDICATE_PARENT, $level = 1) {
        self::$inIdx = 0;
        $sql = "SELECT STATEMENTID, SUBJECTNAME, PREDICATENAME, OBJECTNAME, RESOURCELITERAL, LANGCODEFK
                    FROM ".$this->database.".RDF_STATEMENTVIEW
                    WHERE SUBJECTNAME IN (
                      SELECT SUBJECTNAME
                        FROM ".$this->database.".RDF_STATEMENTVIEW
                        WHERE PREDICATENAME = '" . $parentPredicate . "' AND %s
                    ) ORDER BY STATEMENTID";
        $in  = $this->inClause('OBJECTNAME', $parents);
        $sql = sprintf($sql, $in['sql']);
        $stid = $this->prepareStatement($sql, $in['bind'], false);
        $this->execute($stid);
        $models  = $this->populateModel($stid);
        $newParents = array();
        foreach ($models as $subject => $model) {
            /** @var \Triplestore\Model\Model $model */
            if (!isset($parents[$subject])) {
                $newParents[$subject] = $subject;
            }
            $linker[$subject] = $model;
            $partOf = $model->getPredicate($parentPredicate)->getResourceValue();
            foreach($partOf as $part) {
                if (!isset($linker[$part])) {
                    continue;
                }
                /** @var \Triplestore\Model\Model $parent */
                $parent = $linker[$part];
                $typePred = $model->getOrCreatePredicate(ObjectManager::PREDICATE_TYPE);
                foreach ($typePred as $typeObject) {
                    /** @var \Triplestore\Model\TripleObject $typeObject */
                    $parent->getOrCreatePredicate($typeObject->getValue())
                        ->addObject(new TripleObject($model));
                }
            }
        }
        if ($level >= self::MAX_DEPTH) {
            return;
        }
        if (count($newParents) > 0) {
            $this->findHas($linker, $newParents, $parentPredicate, ++$level);
        }
    }

    public function findDeep(array $subjects, $parentPredicate = ObjectManager::PREDICATE_PARENT) {
        $sql = "FROM ".$this->database.".RDF_STATEMENTVIEW where SUBJECTNAME in
                (
                  SELECT SUBJECTNAME FROM ".$this->database.".RDF_STATEMENTVIEW WHERE %s
                UNION ALL
                  SELECT SUBJECTNAME FROM ".$this->database.".RDF_STATEMENTVIEW WHERE PREDICATENAME = '".$parentPredicate."' AND %s
                UNION ALL
                  SELECT SUBJECTNAME from ".$this->database.".RDF_STATEMENTVIEW WHERE PREDICATENAME = '".$parentPredicate."' AND OBJECTNAME in (
                    SELECT SUBJECTNAME
                      FROM ".$this->database.".RDF_STATEMENTVIEW
                      WHERE PREDICATENAME = '".$parentPredicate."' AND %s
                  )
                UNION ALL
                  SELECT SUBJECTNAME from ".$this->database.".RDF_STATEMENTVIEW WHERE PREDICATENAME = '".$parentPredicate."' AND OBJECTNAME in (
                    SELECT SUBJECTNAME from ".$this->database.".RDF_STATEMENTVIEW WHERE PREDICATENAME = '".$parentPredicate."' AND OBJECTNAME in (
                      SELECT SUBJECTNAME
                        FROM ".$this->database.".RDF_STATEMENTVIEW
                        WHERE PREDICATENAME = '".$parentPredicate."' AND %s
                    )
                  )
                UNION ALL
                  SELECT SUBJECTNAME from ".$this->database.".RDF_STATEMENTVIEW WHERE PREDICATENAME = '".$parentPredicate."' AND OBJECTNAME in (
                    SELECT SUBJECTNAME from ".$this->database.".RDF_STATEMENTVIEW WHERE PREDICATENAME = '".$parentPredicate."' AND OBJECTNAME in (
                      SELECT SUBJECTNAME FROM ".$this->database.".RDF_STATEMENTVIEW WHERE PREDICATENAME = '".$parentPredicate."' AND OBJECTNAME in (
                        SELECT SUBJECTNAME
                          FROM ".$this->database.".RDF_STATEMENTVIEW
                          WHERE PREDICATENAME = '".$parentPredicate."' AND %s                      
                      )
                    )
                  )
                )";
        $objectIn  = $this->inClause('OBJECTNAME', $subjects);
        $subjectIn = $this->inClause('SUBJECTNAME', $subjects);
        $sql = sprintf($sql, $subjectIn['sql'], $objectIn['sql'], $objectIn['sql'], $objectIn['sql'], $objectIn['sql']);
        $bind = array_merge($subjectIn['bind'], $objectIn['bind']);

        $sql = "SELECT STATEMENTID, SUBJECTNAME, PREDICATENAME, OBJECTNAME, RESOURCELITERAL, LANGCODEFK "
            . $sql
            . " ORDER BY STATEMENTID";

        $stid = $this->prepareStatement($sql, $bind);
        $this->execute($stid);
        $models = $this->populateModel($stid);
        $this->linkMultiLevelModels($models, $parentPredicate);
        return $models;
    }

    public function findSingleDeep($subject, $parentPredicate = ObjectManager::PREDICATE_PARENT) {
        $sql = "FROM ".$this->database.".RDF_STATEMENTVIEW where SUBJECTNAME in
                (
                  SELECT :subjectname FROM DUAL
                UNION ALL
                  SELECT SUBJECTNAME FROM ".$this->database.".RDF_STATEMENTVIEW WHERE PREDICATENAME = '".$parentPredicate."' AND OBJECTNAME = :subjectname
                UNION ALL
                  SELECT SUBJECTNAME FROM ".$this->database.".RDF_STATEMENTVIEW WHERE PREDICATENAME = '".$parentPredicate."' AND OBJECTNAME IN (
                    SELECT SUBJECTNAME
                      FROM ".$this->database.".RDF_STATEMENTVIEW
                      WHERE PREDICATENAME = '".$parentPredicate."' AND OBJECTNAME = :subjectname
                  )
                UNION ALL
                  SELECT SUBJECTNAME FROM ".$this->database.".RDF_STATEMENTVIEW WHERE PREDICATENAME = '".$parentPredicate."' AND OBJECTNAME IN (
                    SELECT SUBJECTNAME FROM ".$this->database.".RDF_STATEMENTVIEW WHERE PREDICATENAME = '".$parentPredicate."' AND OBJECTNAME IN (
                      SELECT SUBJECTNAME
                        FROM ".$this->database.".RDF_STATEMENTVIEW
                        WHERE PREDICATENAME = '".$parentPredicate."' AND OBJECTNAME = :subjectname
                    )
                  )
                UNION ALL
                  SELECT SUBJECTNAME FROM ".$this->database.".RDF_STATEMENTVIEW WHERE PREDICATENAME = '".$parentPredicate."' AND OBJECTNAME IN (
                    SELECT SUBJECTNAME FROM ".$this->database.".RDF_STATEMENTVIEW WHERE PREDICATENAME = '".$parentPredicate."' AND OBJECTNAME IN (
                      SELECT SUBJECTNAME FROM ".$this->database.".RDF_STATEMENTVIEW WHERE PREDICATENAME = '".$parentPredicate."' AND OBJECTNAME IN (
                          SELECT SUBJECTNAME
                            FROM ".$this->database.".RDF_STATEMENTVIEW
                            WHERE PREDICATENAME = '".$parentPredicate."' AND OBJECTNAME = :subjectname
                      )
                    )
                  )
                )";
        $sql = "SELECT STATEMENTID, SUBJECTNAME, PREDICATENAME, OBJECTNAME, RESOURCELITERAL, LANGCODEFK "
            . $sql
            . " ORDER BY STATEMENTID";

        $stid = oci_parse(self::getConnection(), $sql);
        oci_bind_by_name($stid, ':subjectname', $subject);
        $this->execute($stid);
        $models = $this->populateModel($stid);
        $this->linkMultiLevelModels($models, $parentPredicate);
        return $models;
    }

    private function linkMultiLevelModels(&$models, $parentPredicate) {
        $linker = $models;
        foreach ($models as $subject => $model) {
            /** @var \Triplestore\Model\Model $model */
            $partOf = $model->getPredicate($parentPredicate);
            if ($partOf === null || $partOf->count() == 0) {
                continue;
            }
            $hasParent = false;
            foreach($partOf as $part) {
                $parentSubject = $part->getValue();
                if (!isset($linker[$parentSubject])) {
                    continue;
                }
                $hasParent = true;
                /** @var \Triplestore\Model\Model $parent */
                $parent = $linker[$parentSubject];
                $typePred = $model->getOrCreatePredicate(ObjectManager::PREDICATE_TYPE);
                foreach ($typePred as $typeObject) {
                    $type = $typeObject->getValue();
                    /** @var \Triplestore\Model\TripleObject $typeObject */
                    $parent->getOrCreatePredicate(isset(MetadataService::$classMap[$type]) ? MetadataService::$classMap[$type] : $type)
                        ->addObject(new TripleObject($model));
                }
            }
            if ($hasParent) {
                unset($models[$subject]);
            }
        }
    }

    /**
     * Returns distinct values of the given predicate
     *
     * @param string $field
     * @return array mixed
     */
    public function fetchDistinct($field, $resource = false, $criteria = null) {
        if ($resource) {
            $sql = 'SELECT DISTINCT(OBJECTNAME) FROM ' . $this->database . '.RDF_STATEMENTVIEW WHERE PREDICATENAME = \'' . $field . '\'';
        } else {
            $sql = 'SELECT DISTINCT(RESOURCELITERAL) FROM ' . $this->database . '.RDF_STATEMENTVIEW WHERE PREDICATENAME = \'' . $field . '\'';
        }

        $bounds = array();
        if (is_array($criteria) && count($criteria) > 0) {
            $sql  .= ' AND SUBJECTNAME IN (%s)';
            $inner = $this->getCriteriaSql($criteria, $bounds);
            $sql   = sprintf($sql, $inner);
        }

        $stid = oci_parse(self::getConnection(), $sql);
        foreach ($bounds as $key => $value) {
            if (isset($value['predicate'])) {
                oci_bind_by_name($stid, ':key'. $key, $value['predicate']);
            }
            oci_bind_by_name($stid, ':value'. $key, $value['object']);
        }

        $this->execute($stid);
        $result = array();
        oci_fetch_all($stid, $result);

        return array_pop($result);
    }

    /**
     * Populates the model with teh data from the database
     *
     * @param $stid
     *
     * @return array
     */
    private function populateModel($stid) {
        $models = array();
        while ($row = oci_fetch_assoc($stid)) {
            $sId         = $row['STATEMENTID'];
            $subject     = $row['SUBJECTNAME'];
            $predicate   = $row['PREDICATENAME'];
            $objResource = $row['OBJECTNAME'];
            $objLiteral  = $row['RESOURCELITERAL'];
            $lang        = $row['LANGCODEFK'];
            if (!isset($models[$subject])) {
                $models[$subject] = new Model($subject);
                $models[$subject]->setFromDB(true);
            }
            /** @var \Triplestore\Model\Model $model */
            $model = $models[$subject];
            $pred  = $model->getOrCreatePredicate($predicate);
            if ($objLiteral === null) {
                $pred->addObject(new TripleObject($objResource, null, $sId));
            } else {
                $pred->addObject(new TripleObject($objLiteral, $lang, $sId, TripleObject::LITERAL));
            }
        }

        return $models;
    }

    /**
     * Processes the model for storing it's information
     *
     * @param Model $model
     * @param array $insertTriplets
     * @param array $addSql
     * @param array $dels
     * @param int $count
     * @param bool $deep
     * @internal param array $delLinks
     * @internal param \Triplestore\Model\Model $history
     */
    private function processModel(Model $model, array &$insertTriplets, &$addSql, array &$dels, &$count, $deep = false)
    {
        $subject = $model->getSubject();
        $subject = IdService::getQName($subject, true);
        $curCnt  = $count;
        if (!$this->hasResource($subject)) {
            $this->addResource($subject);
        }
        foreach ($model as $predicate) {
            /** @var \Triplestore\Model\Predicate $predicate */
            $predicateName = $predicate->getName();
            foreach ($predicate as $object) {
                /** @var \Triplestore\Model\TripleObject $object */
                $value = $object->getValue();
                $chk = ''.$object->toString();
                if ($value instanceof Model) {
                    $this->processModel($value, $insertTriplets, $addSql, $dels, $count);
                    continue;
                }

                // only string values can be store to ontology db
                if (is_numeric($value)) {
                    $value = '' .$value;
                }
                if (!is_string($value) || trim($value) === '') {
                    continue;
                }
                if (isset($checks[$chk]) && array_pop($checks[$chk]) !== null) {
                    continue;
                }

                if ($object->isLiteral()) {
                    $lang           = $object->getLang();
                    $objectLiteral  = trim($value);
                    $objectResource = null;
                } else {
                    $lang           = null;
                    $objectLiteral  = null;
                    $objectResource = IdService::getQName($value, true);
                }

                if ($count > 0) {
                    $addSql .= ' UNION ALL ';
                }
                $addSql .= "SELECT :subject{$count},:predicate{$count},:object{$count},:objectLiteral{$count},:objectLangCode{$count} FROM DUAL";
                $insertTriplets[$count] = array(
                    'subject'        => $subject,
                    'predicate'      => $predicateName,
                    'object'         => $objectResource,
                    'objectLiteral'  => $objectLiteral,
                    'objectLangCode' => $lang,
                );
                $count++;
            }
        }
        if (($count - $curCnt)== 0) {
            if ($count > 0) {
                $addSql .= ' UNION ALL ';
            }
            $addSql .= "SELECT :subject{$count},:predicate{$count},:object{$count},:objectLiteral{$count},:objectLangCode{$count} FROM DUAL";
            $insertTriplets[$count] = array(
                'subject'        => $subject,
                'predicate'      => '',
                'object'         => '',
                'objectLiteral'  => '',
                'objectLangCode' => null,
            );
            $count++;
        }

    }

    /**
     * Converts the array to list that can be used in sql statements
     *
     * Note:
     *  This is not sql save method so it should be used with great care and not with any data given by the user.
     *
     * @param      $list
     * @param bool $isArray
     *
     * @return string
     * @throws \ErrorException
     */
    private function toStrList($list, $isArray = true) {
        if ($isArray) {
            if (array_walk($list, array($this, 'toSafeString'))) {
                return "'" . implode("', '", $list). "'";
            }
            throw new \ErrorException("Could not make the query safe!");
        }
        $list = $this->toSafeString($list);
        return "'$list'";
    }

    /**
     * This makes queries sql injection safe
     *
     * If at some point fetching is done with the whole uri then this will need modification
     *
     * @param $value
     *
     * @return string
     */
    private function toSafeString($value) {
        return preg_replace('/[^\w\d\.:_\-]/i', '', $value);
    }

    /**
     * Updates a value of single property
     *
     * This first removes the whole predicate from the subject and then adds it with the new value
     *
     * @param string     $subject
     * @param Predicate  $predicate
     *
     * @return mixed
     */
    public function updatePredicate($subject, Predicate $predicate, $user = null)
    {
        $name = $predicate->getName();
        if (empty($subject) || !is_string($subject) || empty($name)) {
            return false;
        }
        if (!isset(self::$stmtCache['delStatement'])) {
            $delSql = "DELETE FROM ".$this->database.".RDF_STATEMENT WHERE STATEMENTID IN (
                SELECT STATEMENTID FROM ".$this->database.".RDF_STATEMENTVIEW
                WHERE SUBJECTNAME = :subject and PREDICATENAME = :predicate
            )";
            self::$stmtCache['delStatement'] = oci_parse(self::getConnection(), $delSql);
        }
        oci_bind_by_name(self::$stmtCache['delStatement'], ':subject', $subject);
        oci_bind_by_name(self::$stmtCache['delStatement'], ':predicate', $name);
        $this->startTransaction();
        $this->execute(self::$stmtCache['delStatement']);
        foreach ($predicate as $object) {
            /** @var \Triplestore\Model\TripleObject $object */
            if ($object->isLiteral()) {
                $this->addLiteralStatement($subject, $name, $object->getValue(), $object->getLang(), $user);
            } else {
                $this->addResourceStatement($subject, $name, $object->getValue(), $user);
            }
        }
        $this->commit();
        if (isset($this->modelHistory[$subject])) {
            unset($this->modelHistory[$subject]);
        }

        return true;
    }

    private function addResourceStatement($subject, $predicate, $value, $userQName) {
        if ($value === null || $value === '') {
            return true;
        }
        if (!isset(self::$stmtCache['addResource'])) {
            $sql = "BEGIN ".$this->database.".AddStatement(:subject, :predicateName, :objectName, :userQname); END;";
            self::$stmtCache['addResource'] = oci_parse(self::getConnection(), $sql);
        }
        $stid = self::$stmtCache['addResource'];
        oci_bind_by_name($stid, ':subject', $subject);
        oci_bind_by_name($stid, ':predicateName', $predicate);
        oci_bind_by_name($stid, ':objectName', $value);
        oci_bind_by_name($stid, ':userQname', $userQName);
        $this->execute($stid);
        return true;
    }

    private function addLiteralStatement($subject, $predicate, $value, $lang, $userQName) {
        if ($value === null || $value === '') {
            return true;
        }
        if (!isset(self::$stmtCache['addLiteral'])) {
            $sql = "BEGIN ".$this->database.".AddStatementL(:subject, :predicateName, :literal, :lang, :userQname); END;";
            self::$stmtCache['addLiteral'] = oci_parse(self::getConnection(), $sql);
        }
        $stid = self::$stmtCache['addLiteral'];
        oci_bind_by_name($stid, ':subject', $subject);
        oci_bind_by_name($stid, ':predicateName', $predicate);
        oci_bind_by_name($stid, ':literal', $value);
        oci_bind_by_name($stid, ':lang', $lang);
        oci_bind_by_name($stid, ':userQname', $userQName);
        $this->execute($stid);
        return true;
    }

    /**
     * Returns the value of singe statement from the database
     *
     * @param string $subject
     * @param string $property
     *
     * @return mixed
     */
    public function getPredicate($subject, $property)
    {
        if (!$this->hasResource($subject)) {
            return null;
        }
        $sql = "SELECT OBJECTNAME, RESOURCELITERAL, LANGCODEFK
                    FROM ".$this->database.".RDF_STATEMENTVIEW
                    WHERE SUBJECTNAME = :subject AND PREDICATENAME = :predicate";

        $stid = oci_parse(self::getConnection(), $sql);
        oci_bind_by_name($stid, ':subject', $subject);
        oci_bind_by_name($stid, ':predicate', $property);

        $this->execute($stid);
        $predicate = new Predicate($property);
        while ($row = oci_fetch_assoc($stid)) {
            $objResource = $row['OBJECTNAME'];
            $objLiteral  = $row['RESOURCELITERAL'];
            $lang        = $row['LANGCODEFK'];
            if ($objLiteral === null) {
                $predicate->addObject(new TripleObject($objResource));
            } else {
                $predicate->addObject(new TripleObject($objLiteral, $lang, null, TripleObject::LITERAL));
            }
        }
        return $predicate;
    }

    public function setKeepHistory($keep) {
        $this->keepHistory = $keep;
    }

    public function getKeepHistory() {
        return $this->keepHistory;
    }

    /**
     * @return boolean
     */
    public function isUseResultSet()
    {
        return $this->useResultSet;
    }

    /**
     * @param boolean $useResultSet
     */
    public function setUseResultSet($useResultSet)
    {
        $this->useResultSet = $useResultSet;
    }
}
