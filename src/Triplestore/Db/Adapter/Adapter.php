<?php

namespace Triplestore\Db\Adapter;

use Triplestore\Db\Adapter\Oci8\Statement;
use Zend\Db\Adapter\Adapter as DbAdapter;
use Zend\Db\Adapter\Driver\Oci8\Oci8;

class Adapter extends DbAdapter {

    public function getDbName()
    {
        $driver = $this->getDriver();
        if ($driver instanceof Oci8) {
            $statement = $driver->getStatementPrototype();
            if ($statement instanceof Statement) {
                return $statement->getDbName();
            }
        }
        throw new \Exception('Default database name is not given');
    }

}