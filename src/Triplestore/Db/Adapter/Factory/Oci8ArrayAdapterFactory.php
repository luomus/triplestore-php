<?php

namespace Triplestore\Db\Adapter\Factory;

use Triplestore\Db\Adapter\Adapter;
use Triplestore\Db\Adapter\Oci8\Result\ArrayResult;
use Triplestore\Db\Adapter\Oci8\Statement;
use Triplestore\Options\ModuleOptions;
use Zend\Db\Adapter\Driver\Oci8\Oci8;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class Oci8ArrayAdapterFactory is factory class for creating db adapter that returns db result as an array.
 *
 * @package Triplestore\Service
 */
class Oci8ArrayAdapterFactory implements FactoryInterface
{

    /**
     * Creates the service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return Adapter
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $dbResultSet = new ArrayResult();
        $statement = new Statement();
        $om = $serviceLocator->get('Triplestore\ObjectManager');

        $properties = $om->getMetadataService()->getAllProperties();
        $dbResultSet->setProperties($properties);

        /** @var ModuleOptions $options */
        $options = $serviceLocator->get('Triplestore\Options\ModuleOptions');
        $dbOptions = $options->getDatabase();
        $statement->setDbName($dbOptions->getDatabase());

        $driver = new Oci8([
            'username' => $dbOptions->getUsername(),
            'password' => $dbOptions->getPassword(),
            'connection_string' => $dbOptions->getHost(),
            'charset' => $dbOptions->getCharset()
        ], $statement, $dbResultSet);

        return new Adapter($driver);
    }
}
