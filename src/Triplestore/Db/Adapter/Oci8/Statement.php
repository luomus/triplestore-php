<?php
namespace Triplestore\Db\Adapter\Oci8;

class Statement extends \Zend\Db\Adapter\Driver\Oci8\Statement {

    const SQL_WRAP = "SELECT * FROM %s.RDF_DOCTREE WHERE ROOTNAME IN (%s) ORDER BY ROOTNAME, STATEMENTID";

    private $isWrapped = false;
    private $dbName = null;

    public function prepare($sql = null, $wrap = true) {
        $sql = ($sql) ?: $this->sql;
        if ($wrap) {
            return parent::prepare($this->wrapSql($sql));
        }
        return parent::prepare($sql);
    }

    public function wrapSql($sql) {
        if ($this->isWrapped) {
            return $sql;
        }
        $this->isWrapped = true;
        if ($this->dbName === null) {
            throw new \Exception('You must set the default database before wrapping sql');
        }
        return sprintf(self::SQL_WRAP, $this->dbName, $sql);
    }

    /**
     * @return null
     */
    public function getDbName()
    {
        return $this->dbName;
    }

    /**
     * @param null $dbName
     */
    public function setDbName($dbName)
    {
        $this->dbName = $dbName;
    }

}