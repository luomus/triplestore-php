<?php

namespace Triplestore\Db\Adapter\Oci8\Result\Single;

use Triplestore\Model\TripleSet;
use Triplestore\Model\Triplet;
use Zend\Db\Adapter\Driver\Oci8\Result;
use Zend\Db\Adapter\Exception;

class TripleSetResult extends Result
{

    /**
     * Load from oci8 result
     *
     * @return bool
     */
    protected function loadData()
    {
        if ($this->currentData !== null) {
            return false;
        }
        $set = new TripleSet();
        while($row = oci_fetch_assoc($this->resource)) {
            $set->addTriplet($this->getTriplet($row));
        }

        if ($set->hasResult()) {
            $this->currentData = $set;
            $this->position++;
            return true;
        }
        return false;
    }

    protected function getTriplet($row) {
        return new Triplet(
            $row['STATEMENTID'],
            $row['SUBJECTNAME'],
            $row['PREDICATENAME'],
            $row['OBJECTNAME'],
            $row['RESOURCELITERAL'],
            $row['LANGCODEFK']
        );
    }

}
