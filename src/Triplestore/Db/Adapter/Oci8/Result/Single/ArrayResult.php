<?php

namespace Triplestore\Db\Adapter\Oci8\Result\Single;

use Triplestore\Model\Properties;
use Zend\Db\Adapter\Driver\Oci8\Result;
use Zend\Db\Adapter\Exception;

class ArrayResult extends Result
{

    /** @var  Properties */
    protected $properties;

    /**
     * @return Properties
     */
    public function getProperties()
    {
        return $this->properties;
    }

    /**
     * @param Properties $properties
     */
    public function setProperties(Properties $properties)
    {
        $this->properties = $properties;
    }

    /**
     * Load from oci8 result
     *
     * @return bool
     */
    protected function loadData()
    {
        $this->currentComplete = true;
        $this->currentData = false;
        if ($this->position > 0 ) {
            return false;
        }
        $subjects = [];
        $types  = [];
        $levels = [];
        $parent = [];
        while($row = oci_fetch_assoc($this->resource)) {
            $subjects[$row['SUBJECTNAME']][] = $row;
            if ($row['PREDICATENAME'] === 'rdf:type') {
                $types[$row['SUBJECTNAME']]  = $row['OBJECTNAME'];
                $levels[$row['NODELEVEL']] = $row['SUBJECTNAME'];
            } elseif ($row['PREDICATENAME'] === 'MY.isPartOf') {
                $parent[$row['OBJECTNAME']][] = $row['SUBJECTNAME'];
            }
        }
        if (empty($subjects)) {
            return false;
        }

        $this->currentData = $this->processLevel($levels[0], $subjects, $types, $parent);
        $this->position++;
        return true;
    }

    protected function processLevel($parentSubject, $subjects, $types, $parent, array $data = []) {
        if (!isset($subjects[$parentSubject])) {
            return $data;
        }
        $data['subject'] = $parentSubject;
        foreach ($subjects[$parentSubject] as $row) {
            $this->addData($row, $data);
        }
        if (isset($parent[$parentSubject])) {
            foreach($parent[$parentSubject] as $key => $childrenSubject) {
                $data[$types[$childrenSubject]][] = $this->processLevel($childrenSubject, $subjects, $types, $parent);
            }
        }
        return $data;
    }

    protected function addData($row, &$itemData) {
        $predicate = $row['PREDICATENAME'];
        if ($predicate === 'rdf:type') {
            $itemData['type'] = $row['OBJECTNAME'];
            return;
        }
        $hasMany   = $this->properties->hasMany($predicate);
        if ($this->properties->isResource($predicate)) {
            if ($hasMany) {
                $itemData[$predicate][] = $row['OBJECTNAME'];
            } else {
                $itemData[$predicate] = $row['OBJECTNAME'];
            }
        } else {
            if ($this->properties->isMultiLanguage($predicate)) {
                if ($hasMany) {
                    $itemData[$predicate][$row['LANGCODEFK']][] = $row['RESOURCELITERAL'];
                } else {
                    $itemData[$predicate][$row['LANGCODEFK']] = $row['RESOURCELITERAL'];
                }
            } else {
                if ($hasMany) {
                    $itemData[$predicate][] = $row['RESOURCELITERAL'];
                } else {
                    $itemData[$predicate] = $row['RESOURCELITERAL'];
                }
            }

        }
    }
}
