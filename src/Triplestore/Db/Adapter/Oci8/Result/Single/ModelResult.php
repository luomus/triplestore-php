<?php

namespace Triplestore\Db\Adapter\Oci8\Result\Single;

use Triplestore\Model\Model;
use Zend\Db\Adapter\Driver\Oci8\Result;
use Zend\Db\Adapter\Exception;

class ModelResult extends Result
{
    protected $hasMore = true;

    protected $lastRow = null;

    /**
     * Initialize
     * @param resource $resource
     * @return Result
     */
    public function initialize($resource /*, $generatedValue, $isBuffered = null*/)
    {
        if (!is_resource($resource) && get_resource_type($resource) !== 'oci8 statement') {
            throw new Exception\InvalidArgumentException('Invalid resource provided.');
        }
        $this->resource = $resource;
        return $this;
    }

    /**
     * Load from oci8 result
     *
     * @return bool
     */
    protected function loadData()
    {
        $this->currentComplete = true;
        $this->currentData = false;
        if ($this->hasMore === false) {
            return false;
        }
        $hasData = false;
        $model = new Model();
        if ($this->lastRow === null) {
            $this->lastRow = oci_fetch_assoc($this->resource);
            if ($this->lastRow !== false) {
                $hasData = true;
                $model->setSubject($this->lastRow['SUBJECTNAME']);
                $this->processRow($this->lastRow, $model);
            }
        } else {
            $hasData = true;
            $model->setSubject($this->lastRow['SUBJECTNAME']);
            $this->processRow($this->lastRow, $model);
        }
        while($row = oci_fetch_assoc($this->resource)) {
            if ($this->lastRow['SUBJECTNAME'] !== $row['SUBJECTNAME']) {
                $this->lastRow = $row;
                break;
            }
            $this->processRow($this->lastRow, $model);
        }
        if ($this->lastRow === false || $row === false) {
            $this->hasMore = false;
        }

        if ($hasData) {
            $this->currentData = $model;
            $this->position++;
            return true;
        }
        return false;
    }

    protected function processRow($row, Model $model) {
        $predicate = $model->getOrCreatePredicate($row['PREDICATENAME']);
        if ($row['RESOURCELITERAL'] === null) {
            $predicate->addResourceValue($row['OBJECTNAME']);
        } else {
            $predicate->addLiteralValue($row['RESOURCELITERAL'], $row['LANGCODEFK']);
        }
    }
}
