<?php

namespace Triplestore\Db\Adapter\Oci8\Result;

use Triplestore\Model\Properties;
use Zend\Db\Adapter\Driver\Oci8\Result;
use Zend\Db\Adapter\Exception;

class ArrayResult extends Result
{

    /** @var  Properties */
    protected $properties;

    protected $hasMore = true;

    protected $firstRow;

    /**
     * @return Properties
     */
    public function getProperties()
    {
        return $this->properties;
    }

    /**
     * @param Properties $properties
     */
    public function setProperties(Properties $properties)
    {
        $this->properties = $properties;
    }


    protected function loadData()
    {
        $this->currentComplete = true;
        $this->currentData = false;
        if (!$this->hasMore) {
            return false;
        }
        $subjects = [];
        $types    = [];
        $levels   = [];
        $parent   = [];
        if ($this->firstRow === null) {
            $this->firstRow = oci_fetch_assoc($this->resource);
        }
        if ($this->firstRow === false) {
            return false;
        }
        $this->processRow($this->firstRow, $subjects, $types, $levels, $parent);
        $braked = false;
        while($row = oci_fetch_assoc($this->resource)) {
            if ($this->firstRow['ROOTNAME'] !== $row['ROOTNAME']) {
                $this->firstRow = $row;
                $braked = true;
                break;
            }
            $this->processRow($row, $subjects, $types, $levels, $parent);
        }
        if (!$braked) {
            $this->hasMore = false;
        }
        $this->currentData = $this->processDocument($levels[0], $subjects, $types, $parent);

        if ($this->currentData !== false) {
            $this->position++;
            return true;
        }
        return false;
    }

    protected function processRow($row, &$subjects, &$types, &$levels, &$parent) {
        $subjects[$row['SUBJECTNAME']][] = $row;
        if ($row['PREDICATENAME'] === 'rdf:type') {
            $types[$row['SUBJECTNAME']]  = $row['OBJECTNAME'];
            $levels[$row['NODELEVEL']] = $row['SUBJECTNAME'];
        } elseif ($row['PREDICATENAME'] === 'MY.isPartOf') {
            $parent[$row['OBJECTNAME']][] = $row['SUBJECTNAME'];
        }
    }


    protected function processDocument($parentSubject, $subjects, $types, $parent, array $data = []) {
        if (!isset($subjects[$parentSubject])) {
            return $data;
        }
        $data['subject'] = $parentSubject;
        foreach ($subjects[$parentSubject] as $row) {
            $this->addData($row, $data);
        }
        if (isset($parent[$parentSubject])) {
            foreach($parent[$parentSubject] as $key => $childrenSubject) {
                $data[$types[$childrenSubject]][] = $this->processDocument($childrenSubject, $subjects, $types, $parent);
            }
        }
        return $data;
    }

    protected function addData($row, &$itemData) {
        $predicate = $row['PREDICATENAME'];
        if ($predicate === 'rdf:type') {
            $itemData['type'] = $row['OBJECTNAME'];
            return;
        }
        $hasMany = $this->properties->hasMany($predicate);
        $value   = $row['RESOURCELITERAL'] == null ? $row['OBJECTNAME'] : $row['RESOURCELITERAL'];
        if ($this->properties->isMultiLanguage($predicate)) {
            if ($hasMany) {
                $itemData[$predicate][$row['LANGCODEFK']][] = $value;
            } else {
                $itemData[$predicate][$row['LANGCODEFK']] = $value;
            }
        } else {
            if ($hasMany) {
                $itemData[$predicate][] = $value;
            } else {
                $itemData[$predicate] = $value;
            }
        }
    }

}
