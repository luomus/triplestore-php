<?php

namespace Triplestore\Db\Adapter\Oci8\Result;

use Triplestore\Classes;
use Triplestore\Model\Properties;
use Triplestore\Service\ConverterService;
use Zend\Db\Adapter\Exception;

class ModelResult extends ArrayResult
{
    /**
     * @var ConverterService
     */
    private $converter;

    public function __construct() {
        $this->converter = new ConverterService();
    }

    /**
     * @param Properties $properties
     */
    public function setProperties(Properties $properties)
    {
        $this->properties = $properties;
        $properties->initAliases();
    }

    protected function processDocument($parentSubject, $subjects, $types, $parent, array $data = []) {
        if (!isset($subjects[$parentSubject])) {
            return null;
        }
        $class = 'Triplestore\Classes\\' . $this->converter->TripleToPhpVariable($types[$parentSubject]);
        if (!class_exists($class)) {
            Throw new \Exception("Can not find class '$class'");
        }
        /** @var Classes\AbstractOntology $object */
        $object = new $class;
        $object->setSubject($parentSubject);
        foreach ($subjects[$parentSubject] as $row) {
            $this->addData($row, $object);
        }
        if (isset($parent[$parentSubject])) {
            $children = [];
            foreach($parent[$parentSubject] as $key => $childrenSubject) {
                $children[$types[$childrenSubject]][] = $this->processDocument($childrenSubject, $subjects, $types, $parent);
            }
            foreach($children as $field => $value) {
                $object[$field] = $value;
            }
        }
        return $object;
    }

    protected function addData($row, Classes\AbstractOntology $object) {
        $predicate = $row['PREDICATENAME'];
        if ($predicate === 'rdf:type') {
            return;
        }
        if (!$object->hasProperty($predicate)) {
            return;
        }
        //$save = $this->properties->getAlias($predicate);
        $save = $object->getProperty($predicate);
        $save = ucfirst($save);
        if ($this->properties->hasMany($predicate)) {
            $setMethod = 'add' . $save;
        } else {
            $setMethod = 'set' . $save;
        }
        $value = $row['RESOURCELITERAL'] == null ? $row['OBJECTNAME'] : $row['RESOURCELITERAL'];
        $object->$setMethod($value, $row['LANGCODEFK']);
    }

}
