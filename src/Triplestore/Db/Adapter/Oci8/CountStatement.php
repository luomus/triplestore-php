<?php
namespace Triplestore\Db\Adapter\Oci8;


class CountStatement {
    const SQL_WRAP = "SELECT count(*) as \"count\" FROM RDF_DOCTREE WHERE ROOTNAME IN (%s) ORDER BY ROOTNAME, STATEMENTID";
}