<?php

namespace Triplestore\Db;

use Triplestore\Model\Model;
use Triplestore\Model\Predicate;

/**
 * Interface TriplestoreInterface is used to bind together all the triplestore backends.
 *
 * @package Triplestore\Db
 */
interface TriplestoreInterface {

    /**
     * Persists the model to database.
     *
     * @param Model $model
     * @param string $user qname
     * @return bool|mixed
     */
    public function store(Model $model, $user = null);

    /**
     * Deletes the model from the triplestore
     *
     * @param Model $model
     *
     * @return bool
     */
    public function remove(Model $model);

    /**
     * Fetches one model based on the qname given
     *
     * @param $subject
     * @param $deep
     * @return Model|null
     * @throws \Triplestore\Exception\InvalidQueryException
     */
    public function fetch($subject, $deep);

    /**
     * This fetches models that match the given criteria
     *
     * @param array|Criteria $criteria
     * @param array $orderBy
     * @param int $limit
     * @param int $offset
     * @param array $fields
     * @return array|null
     * @throws \Triplestore\Exception\InvalidQueryException
     */
    public function findBy($criteria, array $orderBy = null, $limit = null, $offset = null, array $fields = null);

    /**
     * Checks that the resource exists
     *
     * @param string $resource
     *
     * @return bool
     */
    public function hasResource($resource);

    /**
     * Adds new resource to the database
     *
     * @param string $resource
     *
     * @return bool
     */
    public function addResource($resource);

    /**
     * Starts transaction
     */
    public function startTransaction();

    /**
     * Commits the transactions
     */
    public function commit();

    /**
     * Returns the count that match the criteria
     *
     * @param array $criteria
     *
     * @return mixed
     */
    public function count(array $criteria);

    /**
     * Clears the local cache
     */
    public function clear();


    /**
     * Returns the name of the database
     *
     * @return string
     */
    public function getDatabase();

    /**
     * Updates a value of single property
     *
     * This first removes the whole predicate from the subject and then adds it with the new value
     *
     * @param string $subject
     * @param string     $subject
     * @param Predicate  $perdicate
     *
     * @return boolean
     */
    public function updatePredicate($subject, Predicate $perdicate);

    /**
     * Returns the value of singe statement from the database
     *
     * @param string $subject
     * @param string $property
     *
     * @return mixed
     */
    public function getPredicate($subject, $property);

}
