<?php

namespace Triplestore\Options;


use Zend\Stdlib\AbstractOptions;

class ModuleOptions extends AbstractOptions {

    protected $database;
    protected $repositoryNS;
    protected $classNS;
    protected $classDir;
    protected $generator = [];

    /**
     * @return DatabaseOptions
     */
    public function getDatabase()
    {
        if ($this->database === null) {
            $this->database = new DatabaseOptions();
        }
        return $this->database;
    }

    /**
     * @param array $database
     */
    public function setDatabase(array $database)
    {
        $this->database = new DatabaseOptions($database);
    }

    /**
     * @return string
     */
    public function getClassNS()
    {
        return $this->classNS;
    }

    /**
     * @param string $classNS
     */
    public function setClassNS($classNS)
    {
        $this->classNS = $classNS;
    }

    /**
     * @return string
     */
    public function getRepositoryNS()
    {
        return $this->repositoryNS;
    }

    /**
     * @param string $repositoryNS
     */
    public function setRepositoryNS($repositoryNS)
    {
        $this->repositoryNS = $repositoryNS;
    }

    /**
     * This is here for legacy reasons only and can be removed when every system has been updated
     * to use new kind of way to create the triplestore classes
     *
     * @return string
     */
    public function getClassDir()
    {
        return $this->classDir;
    }

    /**
     * @see ModuleOptions::getClassDir
     *
     * @param string $classDir
     */
    public function setClassDir($classDir)
    {
        $this->classDir = $classDir;
    }

    /**
     * @return array
     */
    public function getGenerator()
    {
        return $this->generator;
    }

    /**
     * @param array $generator
     */
    public function setGenerator(array $generator)
    {
        $this->generator = $generator;
    }
}