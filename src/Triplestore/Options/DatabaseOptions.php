<?php

namespace Triplestore\Options;

use Zend\Stdlib\AbstractOptions;

class DatabaseOptions extends AbstractOptions {

    /** @var String */
    protected $host;
    /** @var String */
    protected $database;
    /** @var String */
    protected $username;
    /** @var String */
    protected $password;
    /** @var String */
    protected $charset;

    /**
     * @return String
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @param String $host
     */
    public function setHost($host)
    {
        $this->host = $host;
    }

    /**
     * @return String
     */
    public function getDatabase()
    {
        return $this->database;
    }

    /**
     * @param String $database
     */
    public function setDatabase($database)
    {
        $this->database = $database;
    }

    /**
     * @return String
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param String $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return String
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param String $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return String
     */
    public function getCharset()
    {
        return $this->charset;
    }

    /**
     * @param String $charset
     */
    public function setCharset($charset)
    {
        $this->charset = $charset;
    }

}