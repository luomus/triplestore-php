<?php

namespace Triplestore\View\Helper;


use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\View\Helper\AbstractHelper;

/**
 * Class Metadata view helper
 *
 * @package Triplestore\View\Helper
 */
class Metadata extends AbstractHelper implements ServiceLocatorAwareInterface {

    private $serviceLocator;
    /**
     * @var \Triplestore\Service\MetadataService
     */
    private $metadataService;

    /**
     * Returns the metadata for the class or this if no parameters given.
     *
     * @param string|null $class
     * @return self|\Triplestore\Model\Metadata
     */
    public function __invoke($class = null) {
        if ($class === null) {
            return $this;
        }
        return $this->getMetadataService()->getMetadataFor($class);
    }

    /**
     * Returns the metadataService
     *
     * @return \Triplestore\Service\MetadataService
     */
    public function getMetadataService() {
        if ($this->metadataService === null) {
            /** @var \Triplestore\Service\ObjectManager $om */
            $om = $this->getServiceLocator()->getServiceLocator()->get('Triplestore\ObjectManager');
            $this->metadataService = $om->getMetadataService();
        }
        return $this->metadataService;
    }


    /**
     * This makes sure that if view echoing this it will not crash
     *
     * @return string
     */
    public function __toString() {
        return '';
    }


    /**
     * Set service locator
     *
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    /**
     * Get service locator
     *
     * @return ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }
}