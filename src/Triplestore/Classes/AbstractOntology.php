<?php

namespace Triplestore\Classes;


use ArrayAccess;
use Triplestore\Exception;
use Triplestore\Stdlib\OntologyInterface;

/**
 * Class AbstractOntology has all the basic functionality that every triplestore class needs
 *
 * @package Triplestore\Stdlib
 */
abstract class AbstractOntology implements OntologyInterface, ArrayAccess {

    protected $map = [];

    /**
     * Subject of the object
     * @var string
     */
    protected $subject;

    protected $children = array();

    /**
     * Returns subject
     *
     * @return null|string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Sets subject of the model
     *
     * @param $subject
     * @return $this
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Returns the property names that have children
     *
     * @return array
     */
    public function getChildren() {
        return $this->children;
    }

    /**
     * Magic method to enable language specific method calls.
     * Language can be used by simply adding two letter language code to the end of method
     * (Please note that language code is camel cased. So fi becomes Fi when is part of the
     * method)
     *
     * If the method is not found in the object this is then called. This subtracts the
     * last two letters and checks if method without the two last letters can be found
     * if not null is returned. If the method exists this calls the method with the
     * parameter it was called and adds language code as the second parameter.
     *
     * It doesn't matter if the two letter code is capitalized or not. Both will work.
     *
     * @param $name
     * @param $args
     * @return mixed
     * @throws \Triplestore\Exception\BadMethodCallException
     */
    public function __call($name, $args) {
        $method = substr($name, 0, -2);
        if (!method_exists($this, $method)) {
            throw new Exception\BadMethodCallException("Invalid method called '$name'!");
        }
        $lang = lcfirst(substr($name, -2));
        if (isset($args[0])) {
            return $this->$method($args[0], $lang);
        } else {
            return $this->$method($lang);
        }
    }

    public function offsetSet($offset, $value) {
        if (is_null($offset)) {
            throw new \Exception("Cannot set to null key in " . get_class($this));
        } else {
            $method = 'set' . ucfirst($this->getProperty($offset));
            $this->$method($value);
        }
    }

    public function offsetExists($offset) {
        try {
            $property = $this->getProperty($offset);
            return $this->$property !== null;
        } catch (\Exception $e) {
            return false;
        }
    }

    public function offsetUnset($offset) {
        $method = 'set' . ucfirst($this->getProperty($offset));
        $this->$method(null);
    }

    public function offsetGet($offset) {
        $method = 'get' . ucfirst($this->getProperty($offset));
        return $this->$method();
    }

    public function hasProperty($property) {
        return isset($this->map[$property]);
    }

    public function getProperty($property) {
        if ($this->hasProperty($property)) {
            return $this->map[$property];
        }
        if (method_exists($this, $property)) {
            return $property;
        }
        throw new \Exception("Could not find property $property from " . get_class($this));
    }

} 