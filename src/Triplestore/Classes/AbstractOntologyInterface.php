<?php

namespace Triplestore\Classes;

use Triplestore\Stdlib\OntologyInterface;

/**
 * Class AbstractOntology has all the basic functionality that every triplestore class needs
 *
 * @package Triplestore\Stdlib
 */
interface AbstractOntologyInterface extends  OntologyInterface {
    public function getSubject();
    public function setSubject($subject);
    public function getChildren();
} 