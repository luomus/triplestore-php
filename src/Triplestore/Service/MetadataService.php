<?php

namespace Triplestore\Service;


use Triplestore\Exception;
use Triplestore\Model\Metadata;
use Triplestore\Model\Model;
use Triplestore\Model\Predicate;
use Triplestore\Model\Properties;
use Triplestore\Stdlib\Hydrator\HydratorInterface;
use Zend\Cache\Storage\StorageInterface;
use Zend\Log\Logger;
use Zend\ServiceManager\ServiceLocatorInterface;

class MetadataService {

    public static $classMap = [
        'MY.measurementClass' => 'MY.measurement',
        'MF.preparationClass' => 'MF.preparation',
        'HRA.permitClass' => 'HRA.permit'
    ];

    const CACHE_KEY = '__classmetadata__';
    const PROPERTY_KEY = '__propertymetadata__';
    const ALT_KEY = '__altmetadata__';
    const ALIAS_KEY = '__alias__';
    const EXTRA_KEY = '__extra__';

    private $classes = [];
    private $alts = [];
    private $extra = [];

    private $specials = array(
        'MY.document' => ['MY.gathering' => 'MY.isPartOf'],
        'MY.gathering' => ['MY.unit' => 'MY.isPartOf'],
        'MY.unit' => ['MY.identification' => 'MY.isPartOf', 'MY.typeSpecimen' => 'MY.isPartOf', 'MF.sample' => 'MY.isPartOf', 'MY.measurement' => 'MY.isPartOf'],
        'MF.sample' => ['MY.measurement' => 'MY.isPartOf', 'MF.preparation' => 'MY.isPartOf'],
        'HRA.transaction' => ['HRA.permit' => 'MY.isPartOf'],
    );

    private $om;
    private $cache;
    private $serviceLocator;

    public function __construct(ObjectManager $om, StorageInterface $cache, ServiceLocatorInterface $sl) {
        $this->om = $om;
        $this->cache = $cache;
        $this->serviceLocator = $sl;
        $this->initMetadata();
    }

    private function initMetadata()
    {
        if ($this->cache->hasItem(self::CACHE_KEY)) {
            $this->cache->getItem(self::PROPERTY_KEY);
            $this->classes = $this->cache->getItem(self::CACHE_KEY);
            $this->alts = $this->cache->getItem(self::ALT_KEY);
            $this->extra = $this->cache->getItem(self::EXTRA_KEY);
            return;
        }
        $this->om->disableHydrator();

        // Parse class data
        $classTypes = [
            [ObjectManager::PREDICATE_TYPE => ObjectManager::OBJ_TYPE_CLASS],
            [ObjectManager::PREDICATE_SUBCLASS => ObjectManager::ANY_OBJECT_VALUE]
        ];
        $childPredicates = $this->om->findBy(array(ObjectManager::TYPE_SUBPROPERTY => ObjectManager::PREDICATE_PARENT_ALL));
        $children = array();
        $aliases = array();
        $properties = Properties::instance();
        foreach ($classTypes as $types) {
            $classModels = $this->om->findBy($types);
            foreach ($classModels as $model) {
                /** @var \Triplestore\Model\Model $model */
                $originalClass = $model->getSubject();
                $class = $originalClass;
                if (isset(MetadataService::$classMap[$originalClass])) {
                    $class = MetadataService::$classMap[$originalClass];
                }
                $metadata = new Metadata();
                $metadata->setClassName($class);
                if ($model->hasPredicate(ObjectManager::PREDICATE_SUBCLASS)) {
                    $extends = $model->getPredicate(ObjectManager::PREDICATE_SUBCLASS)->getFirst()->getValue();
                    $metadata->setExtends($extends);
                }
                $predicateModels = $this->om->findBy(array(ObjectManager::PREDICATE_DOMAIN => $originalClass));
                foreach ($predicateModels as $predicateModel) {
                    /** @var \Triplestore\Model\Model $predicateModel */
                    $property = $predicateModel->getSubject();
                    $metadata->addProperty($property);
                    foreach ($predicateModel as $predicate) {
                        /** @var \Triplestore\Model\Predicate $predicate */
                        if ($predicate->getName() !== ObjectManager::PREDICATE_RANGE) {
                            continue;
                        }
                        $value = $this->getOneValue($predicate, $property);
                        if ($value === null) {
                            break;
                        }
                        if (strpos($value, 'xsd:') === false) {
                            // Children only if the property is subproperty of MZ.isPartOf
                            if (isset($childPredicates[$property])) {
                                if (!isset($children[$value])) {
                                    $children[$value] = array();
                                }
                                if (!isset($children[$value][$property])) {
                                    $children[$value][$property] = array();
                                }
                                $children[$value][$property][] = $class;
                            }
                        }
                    }
                }
                $phpSave = PhpVariableConverter::toPhpClassName($class);
                $this->classes[$class] = $metadata;
                $aliases[$phpSave] = $class;
            }
        }

        // Parse property data
        $propertyTypes = [
            [ObjectManager::PREDICATE_TYPE => ObjectManager::OBJ_TYPE_PROPERTY],
            [ObjectManager::TYPE_SUBPROPERTY => ObjectManager::ANY_OBJECT_VALUE]
        ];
        foreach ($propertyTypes as $types) {
            $predicateModels = $this->om->findBy($types);
            foreach ($predicateModels as $predicateModel) {
                /** @var \Triplestore\Model\Model $predicateModel */
                $property = $predicateModel->getSubject();
                if (!$properties->hasProperty($property)) {
                    $properties->addProperty($property);
                }
                foreach ($predicateModel as $predicate) {
                    /** @var \Triplestore\Model\Predicate $predicate */
                    switch ($predicate->getName()) {
                        case (ObjectManager::TYPE_SUBPROPERTY):
                            $value = $this->getOneValue($predicate, $property);
                            $properties->setSubPropertyOf($property, $value);
                            break;
                        case (ObjectManager::EMBEDDABLE):
                            $value = $this->getOneValue($predicate, $property);
                            $value = is_null($value) ? false : filter_var($value, FILTER_VALIDATE_BOOLEAN);
                            $properties->setEmbeddable($property, $value);
                            break;
                        case (ObjectManager::MULTI_LANGUAGE):
                            $value = $this->getOneValue($predicate, $property);
                            $value = is_null($value) ? false : filter_var($value, FILTER_VALIDATE_BOOLEAN);
                            $properties->setMultiLanguage($property, $value);
                            break;
                        case (ObjectManager::PREDICATE_ORDER):
                            $value = $this->getOneValue($predicate, $property);
                            $value = is_null($value) ? 1 : (int)$value;
                            $properties->setOrder($property, $value);
                            break;
                        case (ObjectManager::PREDICATE_MAX_OCCURS):
                            $value = $this->getOneValue($predicate, $property);
                            $value = is_null($value) ? 1 : (int)$value;
                            $properties->setMax($property, $value);
                            break;
                        case (ObjectManager::PREDICATE_MIN_OCCURS):
                            $value = $this->getOneValue($predicate, $property);
                            $value = is_null($value) ? 1 : (int)$value;
                            $properties->setMin($property, $value);
                            break;
                        case (ObjectManager::PREDICATE_RANGE):
                            $value = $this->getOneValue($predicate, $property);
                            if ($value === null) {
                                break;
                            }
                            if (strpos($value, 'xsd:') !== false) {
                                $hydrClass = ucfirst(str_replace('xsd:', '', $value));
                                $hydrClass = '\Triplestore\Stdlib\Hydrator\\' . $hydrClass;
                                if (class_exists($hydrClass)) {
                                    $obj = new $hydrClass();
                                    if (!$obj instanceof HydratorInterface) {
                                        throw new \Exception('Got invalid type. Perhaps you have just forgotten to use correct interface for type!' . get_class($obj));
                                    }
                                    $properties->setConvertible($property, $hydrClass);
                                }
                            }
                            $properties->setType($property, $value);
                            break;
                        case (ObjectManager::PREDICATE_LABEL):
                            foreach ($predicate as $object) {
                                if (!$object->isLiteral()) {
                                    continue;
                                }
                                $properties->setLabel($property, $object->getValue(), $object->getLang());
                            }
                            break;
                        case (ObjectManager::PREDICATE_COMMENT):
                            $lang = $predicate->getLiteralValue('en');
                            $value = (count($lang) > 1) ? implode('; ', $lang) : current($lang);
                            if (empty($value)) {
                                $lang = $predicate->getLiteralValue('fi');
                                $value = implode('; ', $lang);
                            }

                            $properties->setHelp($property, $value);
                            break;
                    }
                }
            }
        }

        $properties->arrangeProperties();

        // Add items that have resource range to possible children
        if (isset($children[ObjectManager::RANGE_RESOURCE])) {
            $resource = $children[ObjectManager::RANGE_RESOURCE];
            unset($children[ObjectManager::RANGE_RESOURCE]);
            foreach ($this->classes as $class => $metadata) {
                if (!isset($children[$class])) {
                    $children[$class] = $resource;
                } else {
                    $children[$class] = array_merge($children[$class], $resource);
                }
            }
        }

        // Populate the metadata with correct information
        foreach ($children as $range => $value) {
            if (!isset($this->specials[$range])) {
                continue;
            }
            if (is_array($this->specials[$range]) && isset($this->classes[$range])) {
                $specials = $this->specials[$range];
                /** @var Metadata $metadata */
                $metadata = $this->classes[$range];
                $metadata->setChildrensProperties(array_keys(array_flip($specials)));
                $metadata->setChildren($specials);
            } else if (isset($this->classes[$range])) {
                /** @var Metadata $metadata */
                $metadata = $this->classes[$range];
                $metadata->setChildrensProperties(array_keys($value));
                $flatted = $this->flatten($value);
                $metadata->setChildren($flatted);
            }
        }
        $this->alts = $this->getAllAlts();
        $this->classes[self::ALIAS_KEY] = $aliases;
        $this->cache->setItem(self::PROPERTY_KEY, Properties::instance());
        $this->cache->setItem(self::CACHE_KEY, $this->classes);
        $this->cache->setItem(self::ALT_KEY, $this->alts);
        $this->cache->setItem(self::EXTRA_KEY, $this->extra);
        $this->om->enableHydrator();
    }

    protected function getAllAlts() {
        $alts = $this->om->findBy(array(ObjectManager::PREDICATE_TYPE => ObjectManager::OBJ_TYPE_SELECT));
        $result = [];
        foreach($alts as $alt => $model) {
            $result[$alt] = $this->buildSelect($alt);
        }
        return $result;
    }

    public function altExists($altName) {
        return isset($this->alts[$altName]);
    }

    public function getAlt($name, $lang = null) {
        if ($this->altExists($name)) {
            if ($lang === null) {
                return $this->alts[$name];
            }
            $result = [];
            foreach($this->alts[$name] as $key => $values) {
                $result[$key] = isset($values[$lang]) ? $values[$lang] : $key;
            }
            return $result;
        }
        return null;
    }

    public function getExtra($name) {
        if ($this->altExists($name)) {
            return $this->extra[$name];
        }
        return null;
    }

    public function hasExtra($name) {
        return isset($this->extra[$name]);
    }

    public function classExists($className) {
        return $this->getMetadataFor($className) !== null;
    }

    /**
     * Gets the metadata for class
     *
     * @param string $class
     * @return Metadata
     */
    public function getMetadataFor($class) {
        $alias = strrpos($class, '\\');
        if ($alias === false) {
            $alias = $class;
        } else {
            $alias = substr($class, $alias + 1);
        }
        if (isset($this->classes[self::ALIAS_KEY][$alias])) {
            $class = $this->classes[self::ALIAS_KEY][$alias];
        } else if (isset(MetadataService::$classMap[$alias])) {
            $class = MetadataService::$classMap[$alias];
        }
        if (isset($this->classes[$class])) {
            return $this->classes[$class];
        }
        return null;
    }

    /**
     * @return Properties
     */
    public function getAllProperties() {
        reset($this->classes);
        $key = key($this->classes);
        if (isset($this->classes[$key]) && $this->classes[$key] instanceof Metadata) {
            return $this->classes[$key]->getAllProperties();
        }
        return Properties::instance();
    }

    public function getAllClasses() {
        $return = $this->classes;
        unset($return[self::ALIAS_KEY]);

        return $return;
    }

    private function flatten(array $array) {
        $return = array();
        array_walk_recursive($array, function($a) use (&$return) { $return[$a] = true; });
        return $return;
    }

    private function getOneValue(Predicate $predicate, $property) {
        if ($predicate->count() > 1) {
            if ($this->serviceLocator->has('Logger')) {
                /** @var Logger $logger */
                $logger = $this->serviceLocator->get('Logger');
                $logger->crit("Instruction for '$property' is not ambiguous");
            }
            return null;
        }
        foreach ($predicate as $object) {
            return $object->getValue();
        }
        return null;
    }

    private function buildSelect($alt)
    {
        $om = $this->om;
        $om->disableHydrator();
        $mainModel = $om->fetch($alt, false);
        if ($mainModel === null) {
            throw new \RuntimeException(sprintf("No values found for %s in triplestore.", $alt));
        }
        $type = $mainModel->getPredicate(ObjectManager::PREDICATE_TYPE)->current()->getValue();
        if ($type === ObjectManager::OBJ_TYPE_SELECT || $this->classExists($alt)) {
            $fieldsModels = $mainModel;
        } else {
            throw new \RuntimeException(sprintf("Incorrect type spevified for '%s'.", $alt));
        }

        $ordered = [];
        $extras = [];
        foreach ($fieldsModels as $predicate) {
            /** @var \Triplestore\Model\Predicate $predicate */
            $name = $predicate->getName();
            if (strpos($name, 'rdf:_') !== 0) {
                continue;
            }
            $spot = (int)str_replace('rdf:_', '', $name);
            if ($predicate->count() > 1) {
                throw new \RuntimeException("Found ambiguous information for '$alt' $name'");
            }
            $value = $predicate->current()->getValue();
            /** @var Model $optionModel */
            $optionModel = $om->fetch($value, false);
            $labels = [];
            if ($optionModel !== null) {
                $labelPred = $optionModel->getOrCreatePredicate(preg_match("/^MX\.[0-9]+$/", $value) ?
                    ObjectManager::PREDICATE_VERNACULAR_NAME :
                    ObjectManager::PREDICATE_LABEL
                );
                foreach($labelPred as $object) {
                    /** @var \Triplestore\Model\TripleObject $object */
                    if ($object->isLiteral()) {
                        $labels[$object->getLang()] = $object->getValue();
                    }
                }
                $parentPred = $optionModel->getOrCreatePredicate(ObjectManager::PREDICATE_SELECT_PARENT);
                foreach($parentPred as $object) {
                    /** @var \Triplestore\Model\TripleObject $object */
                    if (!isset($extras['altParent'])) {
                        $extras['altParent'] = [];
                    }
                    if (!isset($extras['altParent'][$value])) {
                        $extras['altParent'][$value] = [];
                    }
                    $extras['altParent'][$value][] = $object->getValue();
                }
            }
            if (empty($labels) || !isset($labels[''])) {
                $labels[''] = $value;
            }
            $ordered[$spot] = array(
                'key' => $value,
                'value' => $labels
            );
        }
        if (count($ordered) === 0) {
            return [];
        }
        //$this->extra[$alt]['altParent'][$value] = $object->getValue();
        $hasExtra = false;
        if (count($extras)) {
            $hasExtra = true;
            if (isset($extras['altParent'])) {
                $this->extra[$alt]['altParent'] = [];
            }
        }
        ksort($ordered, SORT_NUMERIC);
        $return = [];
        foreach ($ordered as $data) {
            $return[$data['key']] = $data['value'];
            if ($hasExtra) {
                if (isset($extras['altParent']) && isset($extras['altParent'][$data['key']])) {
                    $this->extra[$alt]['altParent'][$data['key']] = $extras['altParent'][$data['key']];
                }
            }
        }

        return $return;
    }

    protected function getLiteralValue(Predicate $predicate, $lang, $default, $onlyFirst = true) {
        $literal = $predicate->getLiteralValue($lang);
        if (empty($literal)) {
            return $default;
        }
        if ($onlyFirst) {
            $literal = current($literal);
        }
        return $literal;
    }

}