<?php

namespace Triplestore\Service;


use Zend\Cache\Storage\StorageInterface;

class FormService {

    protected $om;
    protected $cache;

    public function __construct(ObjectManager $om, StorageInterface $cache) {
        $this->om = $om;
        $this->cache = $cache;
        $this->initForms();
    }

    protected function initForms() {
        $this->om->enableHydrator();
        $this->om->className('Triplestore\Classes\MHLForm');
        $forms = $this->om->findBy(array('rdf:type' => 'MHL.form'));
        foreach ($forms as $model) {
            var_dump($model);
        }
    }

}