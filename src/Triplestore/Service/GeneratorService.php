<?php

namespace Triplestore\Service;

use Common\Service\IdService;
use Triplestore\Model\Metadata;
use Zend\Code\Generator\ClassGenerator;
use Zend\Code\Generator\DocBlock\Tag;
use Zend\Code\Generator\DocBlockGenerator;
use Zend\Code\Generator\FileGenerator;
use Zend\Code\Generator\MethodGenerator;
use Zend\Code\Generator\ParameterGenerator;
use Zend\Code\Generator\PropertyGenerator;
use Zend\Code\Generator\ValueGenerator;
use Zend\Json\Json;

class GeneratorService
{

    const IN_LINE = '    ';
    const DOT_HYDRATOR_SUFFIX = 'Db';

    private $defaultExtend = 'AbstractOntology';
    private $classTplFile;
    private $hydratorTplFile;
    private $interfaceTplFile;
    private $classFolder;
    private $hydratorFolder;
    private $contextFolder;
    private $converter;
    private $children = array();
    private $useOldNs = false;

    public function __construct($classTplFile, $interfaceTemplateFile, $hydratorTplFile, $classFolder, $hydratorFolder, $contextFolder = null)
    {
        $this->classTplFile     = $classTplFile;
        $this->interfaceTplFile = $interfaceTemplateFile;
        $this->hydratorTplFile  = $hydratorTplFile;
        $this->classFolder      = rtrim($classFolder, '\\/');
        $this->hydratorFolder   = rtrim($hydratorFolder, '\\/');
        $this->converter        = new ConverterService();
        $this->checkFolderPermissions($this->classFolder);
        $this->checkFolderPermissions($this->hydratorFolder);
        if ($contextFolder !== null) {
            $this->contextFolder = rtrim($contextFolder, '\\/');
            $this->checkFolderPermissions($this->contextFolder);
        }
    }

    private function checkFolderPermissions($folder) {
        if (!is_dir($folder)) {
            throw new \Exception("Could not find folder: '{$folder}'");
        }
        if (!is_writable($folder)) {
            throw new \Exception("Folder '{$folder}' is not writable");
        }
    }

    public function generateJsonLDContext($class, Metadata $metadata) {
        if ($this->contextFolder === null) {
            throw new \Exception("You have to set the context folder before generating it");
        }
        $class = $this->getDotless($class);
        $data = $this->getContextBody($metadata);
        if (count($data) < 3) {
            return;
        }
        //$json = Json::encode($data, false, [JSON_UNESCAPED_SLASHES]);
        $json = json_encode($data, JSON_UNESCAPED_SLASHES);
        $filename = $this->contextFolder . DIRECTORY_SEPARATOR . $class . '.jsonld';
        file_put_contents($filename, $json);
    }

    private function getContextBody(Metadata $metadata) {
        $defaultDomain = IdService::getUri(IdService::getDefaultQNamePrefix(). ':');
        $properties = $metadata->getProperties();
        $children = $metadata->getChildren();
        $result = [
            '@vocab' => $defaultDomain,
            'xsd' => 'https://www.w3.org/TR/xmlschema-2/#'
        ];
        foreach($properties as $property) {
            if ($property === 'MY.isPartOf') {
                continue;
            }
            $uri = IdService::getUri($property);
            if ($metadata->isResource($property)) {
                $result[$this->getDotless($property)] = [
                    '@id' => $uri,
                    '@type' => '@vocab'
                ];
            } else {
                $result[$this->getDotless($property)] = [
                    '@id' => $uri,
                    '@type' => $metadata->getType($property)
                ];
            }

        }
        foreach($children as $child) {
            $newName = rtrim($this->getDotless($child), 's') . 's';
            $result[$newName] = [
                '@id' => rtrim(IdService::getUri($child), 's') . 's',
                '@container' => '@set'
            ];
        }
        return $result;
    }

    private function getDotless($value) {
        $pos = strpos($value, '.');
        if ($pos !== false) {
            $value = substr($value, $pos + 1);
        }
        $pos = strpos($value, ':');
        if ($pos !== false) {
            $value = substr($value, $pos + 1);
        }
        return $value;
    }

    public function generateHydrator($class, Metadata $metadata, $original = false) {
        $metadata->getAllProperties()->initAliases();
        $saveClass = $this->converter->TripleToPhpVariable($class);
        $hydrator  = new ClassGenerator(($original ? $saveClass . self::DOT_HYDRATOR_SUFFIX : $saveClass), 'Triplestore\Classes\Hydrator');
        $docblock  = DocBlockGenerator::fromArray(array(
            'shortDescription' => 'Hydrating class for ' . $saveClass,
            'longDescription'  => null,
        ));
        $hydrator->setDocBlock($docblock);
        $hydrator->addUse('Zend\Stdlib\Hydrator\HydratorInterface');
        $hydrator->setImplementedInterfaces(['HydratorInterface']);
        $hydrator->addProperties([
            ['childHydrator', [], PropertyGenerator::FLAG_PRIVATE]
        ]);
        $child = $metadata->getChildren();
        if (!empty($child)) {
            $hydrator->addMethods([
                $this->getHydratorConstructMethod($saveClass, $metadata)
            ]);
        }
        $hydrator->addMethods([
            $this->getExtractMethod($saveClass, $metadata, $original),
            $this->getHydratorMethod($saveClass, $metadata, $original)
        ]);

        $file = FileGenerator::fromArray(array(
            'classes'  => array($hydrator),
            'docblock' => DocBlockGenerator::fromArray(array(
                'shortDescription' => 'Automatically generated class',
                'longDescription'   => 'Do not edit this file this is automatically generated when classes are generated.'
            )),
        ));
        $hydratorFile = $this->classFolder . '/Hydrator/' . ($original ? $saveClass . self::DOT_HYDRATOR_SUFFIX : $saveClass) . '.php';
        file_put_contents($hydratorFile, $file->generate());
    }

    private function getHydratorConstructMethod($class, Metadata $metadata) {
        return new MethodGenerator(
            '__construct',
            [],
            MethodGenerator::FLAG_PUBLIC,
            $this->getHydratorConstructBody($class, $metadata)
        );
    }

    private function getHydratorConstructBody($class, Metadata $metadata) {
        $body = '';
        $children = $metadata->getChildren();
        $propertyHelper = $metadata->getAllProperties();
        foreach($children as $child) {
            $child = $propertyHelper->getAlias($child);
            $body .= '$this->childHydrator[\''.$child.'\'] = new \\'
                . $this->getHydratorName($child, true) . '();' . "\n";
        }
        return $body;
    }

    private function getHydratorMethod($class, Metadata $metadata, $original = false) {
        return new MethodGenerator(
            'hydrate',
            [
                new ParameterGenerator('data', 'array'),
                'object',
                new ParameterGenerator('subject', 'string', new ValueGenerator(null, ValueGenerator::TYPE_NULL))
            ],
            MethodGenerator::FLAG_PUBLIC,
            $this->getHydratorBody($class, $metadata, $original),
            DocBlockGenerator::fromArray([
                'shortDescription' => 'Hydrate $object with the provided $data.',
                'longDescription'  => null,
                'tags'             => [
                    new Tag\ParamTag('data', 'array'),
                    new Tag\ParamTag('object', '\\' . $this->getInterfacedName($class, true)),
                    new Tag\ParamTag('subject', 'string', 'key for the subject source'),
                    new Tag\ReturnTag(['\\' . $this->getInterfacedName($class, true)]),
                ],
            ])
        );
    }

    private function getExtractMethod($class, Metadata $metadata, $original = false) {
        return new MethodGenerator(
            'extract',
            [
                'object',
                new ParameterGenerator('subject', 'string', 'qname')
            ],
            MethodGenerator::FLAG_PUBLIC,
            $this->getExtractBody($class, $metadata, $original),
            DocBlockGenerator::fromArray([
                'shortDescription' => 'Extract values from an object.',
                'longDescription'  => null,
                'tags'             => [
                    new Tag\ParamTag('object', '\\' . $this->getInterfacedName($class, true)),
                    new Tag\ParamTag('subject', 'string', 'key where the subject will be placed'),
                    new Tag\ReturnTag(['array']),
                ],
            ])
        );
    }

    private function getExtractBody($class, Metadata $metadata, $original = false) {
        $body = "\$extract = [];\n";
        $properties = $metadata->getProperties();
        $propertyHelper = $metadata->getAllProperties();
        $body .= "\$extract[\$subject] = \$object->getSubject();\n";
        foreach($properties as $property) {
            $origProperty = $property;
            $property = $propertyHelper->getAlias($property);
            $arrayKey = $original ? $origProperty : $property;
            $method = 'get' . ucfirst($property);
            if ($metadata->hasChildren($origProperty)) {
                if ($metadata->hasMany($origProperty)) {
                    $body .= "\$extract['$arrayKey'] = [];\n";
                    $body .= "foreach(\$object->$method() as \$key => \$child) {\n";
                    $body .= "    \$extract['$arrayKey'][\$key] = \$this->childHydrator['$arrayKey']->extract(\$child, \$subject);\n";
                    $body .= "}\n";
                    continue;
                }
                $body .= "\$extract['$arrayKey'] = \$this->childHydrator['$arrayKey']->extract(\$object->$method(), \$subject);\n";
                continue;
            }
            if ($propertyHelper->isMultiLanguage($origProperty)) {
                $body .= "\$extract['\$property'] = [];\n";
                $body .= "\$extract['$arrayKey'][''] = \$object->$method();\n";
                foreach(['en', 'fi', 'sv'] as $lang) {
                    $body .= "\$extract['$arrayKey']['$lang'] = \$object->$method('$lang');\n";
                }
            } else {
                $body .= "\$extract['$arrayKey'] = \$object->$method();\n";
            }
        }
        $body .= 'return $extract;';
        return $body;
    }

    private function getHydratorBody($class, Metadata $metadata, $original = false) {
        $body = '';
        $properties = $metadata->getProperties();
        $propertyHelper = $metadata->getAllProperties();
        $body .= "if (\$subject !== null && isset(\$data[\$subject])) {\n";
        $body .= "    \$object->setSubject(\$data[\$subject]);\n";
        $body .= "}\n";
        foreach($properties as $property) {
            $origProperty = $property;
            $property = $propertyHelper->getAlias($property);
            $arrayKey = $original ? $origProperty : $property;
            $setMethod = 'set' . ucfirst($property);
            $getMethod = 'get' . ucfirst($property);
            if ($metadata->hasChildren($origProperty)) {
                if ($metadata->hasMany($origProperty)) {
                    $body .= "if (isset(\$data['$arrayKey'])) {\n";
                    $body .= "    \$children = \$object->$getMethod();\n";
                    $body .= "    \$all      = [];\n";
                    $body .= "    foreach(\$data['$arrayKey'] as \$key => \$childData) {\n";
                    $body .= "        \$child = isset(\$children[\$key]) ? \$children[\$key] : new \\{$this->getClassName($origProperty, true, '', true)}();\n";
                    $body .= "        \$all[\$key] = \$this->childHydrator['$arrayKey']->hydrate(\$childData, \$child, \$subject);\n";
                    $body .= "    }\n";
                    $body .= "    \$object->$setMethod(\$all);\n";
                    $body .= "}\n";
                    continue;
                }
                $body .= "if (isset(\$data['$arrayKey'])) {\n";
                $body .= "    \$children = \$object->$getMethod();\n";
                $body .= "    \$child = \$object->$getMethod() ? \$object->$getMethod() : new \\{$this->getClassName($origProperty, true, '', true)}();\n";
                $body .= "    \$object->$setMethod(\$this->childHydrator['$arrayKey']->hydrate(\$data['$arrayKey'], \$child, \$subject));\n";
                $body .= "}\n";
                continue;
            }
            if ($propertyHelper->getOrder($property) < 0) {
                continue;
            }
            if ($propertyHelper->isMultiLanguage($origProperty)) {
                foreach(['en', 'fi', 'sv'] as $lang) {
                    $body .= "if (isset(\$data['{$arrayKey}".ucfirst($lang)."'])) {\n";
                    $body .= "    \$object->$setMethod(\$data['$arrayKey'], '$lang');\n";
                    $body .= "}\n";
                }
            }
            $body .= "if (isset(\$data['$arrayKey'])) {\n";
            $body .= "    \$object->$setMethod(\$data['$arrayKey']);\n";
            $body .= "}\n";
        }
        $body .= 'return $object;';

        return $body;
    }

    private function getClassName($class, $withNS = false, $prefix = '', $useOld = false) {
        $saveClass = $this->converter->TripleToPhpVariable($class);
        if ($withNS) {
            $ns = $useOld ? 'Kotka\Triple\\' : 'Triplestore\Classes\\';
            $saveClass = $ns . $prefix . $saveClass;
        }
        return $saveClass;
    }

    private function getInterfacedName($class, $withNS = false) {
        return $this->getClassName($class, $withNS) . 'Interface';
    }

    private function getHydratorName($class, $withNS = false) {
        return $this->getClassName($class, $withNS, 'Hydrator\\');
    }

    public function generateClass($class, Metadata $metadata)
    {
        $classTpl    = file_get_contents($this->classTplFile);
        $interTpl    = file_get_contents($this->interfaceTplFile);
        $classMap    = array_flip(MetadataService::$classMap);
        $saveClass   = $this->getClassName($class);
        $interClass  = $this->getInterfacedName($class);
        $extends     = $metadata->getExtends() !== null ?
            $this->converter->TripleToPhpVariable($metadata->getExtends()) :
            $this->defaultExtend;
        $interExtend = $extends . 'Interface';
        $classFile   = $this->classFolder . DIRECTORY_SEPARATOR . $saveClass . '.php';
        $interFile   = $this->classFolder . DIRECTORY_SEPARATOR . $interClass . '.php';
        $strMethod   = '';
        $interMethod = '';
        $strProperty = '';
        $mapStr      = '';
        $properties  = $metadata->getProperties();
        $this->children = array();
        foreach ($properties as $property) {
            $phpSaveProperty  = $this->converter->TripleToPhpVariable($property);
            $mapStr          .= $this->mapLine($property, $phpSaveProperty);
            $strMethod       .= $this->methodLine($property, $phpSaveProperty, $metadata);
            $interMethod     .= $this->interFaceMethodLine($property, $phpSaveProperty, $metadata);
            $strProperty     .= $this->propertyLine($property,$phpSaveProperty, $metadata);
        }
        if (count($this->children) > 0) {
            $strProperty .= $this->childProperties($this->children);
        }
        $classTpl = str_replace('%getters-n-setters%', $strMethod, $classTpl);
        $classTpl = str_replace('%properties%', $strProperty, $classTpl);
        $classTpl = str_replace('%map%', $mapStr, $classTpl);
        $classTpl = str_replace('%class%', $saveClass, $classTpl);
        $classTpl = str_replace('%extends%', $extends, $classTpl);
        $classTpl = str_replace('%interfaceClass%', $interClass, $classTpl);
        $classTpl = str_replace('%origClass%', isset($classMap[$class]) ? $classMap[$class] : $class, $classTpl);

        $interTpl = str_replace('%getters-n-setters%', $interMethod, $interTpl);
        $interTpl = str_replace('%class%', $interClass, $interTpl);
        $interTpl = str_replace('%extends%', $interExtend, $interTpl);

        file_put_contents($interFile, $interTpl);
        file_put_contents($classFile, $classTpl);
    }

    private function mapLine($property, $phpSaveProperty) {
        return "
        '$property' => '$phpSaveProperty',";
    }

    private function propertyLine($property, $phpSaveProperty, Metadata $metadata) {

        $annotation       = 'Predicate';
        if ($metadata->hasChildren($property)) {
            $annotation       = 'class';
            $this->children[] = $phpSaveProperty;
        }
        $value = '';
        if ($metadata->hasMany($property)) {
            $value = ' = []';
        }
        if ($metadata->isMultiLanguage($property)) {
            $value = " = []";
        }
        return "/** @$annotation($property) */
    protected \${$phpSaveProperty}{$value};
    ";
    }

    private function childProperties($list) {
        return "protected \$children = ['". implode("', '", $list) ."'];" . PHP_EOL . self::IN_LINE;
    }

    private function interFaceMethodLine($property, $phpSave, Metadata $metadata)
    {
        $cast = $metadata->hasMany($property) ? 'array ': '';
        return "
    public function get$phpSave();
    public function set$phpSave($cast\$value);";
    }

    private function methodLine($property, $phpSave, Metadata $metadata)
    {
        $hasMany = $metadata->hasMany($property);
        $line = '';
        if ($metadata->isMultiLanguage($property)) {
            $defaultLang = $metadata->getDefaultLang($property);
            if (!is_string($defaultLang)) {
                $defaultLang = 'null';
            } else {
                $defaultLang = "'$defaultLang'";
            }
            $cast = $hasMany ? 'array ':'';
            $line .= $this->literalMethod($phpSave, $defaultLang, $cast);
        } else {
            $cast = $hasMany ? 'array ':'';
            $line .= $this->simpleMethod($phpSave, $cast);
        }
        if ($hasMany) {
            $line .= $this->addMethod($phpSave);
        }
        return $line;
    }

    private function addMethod($phpSave) {
        return "
    public function add$phpSave(\$value = null)
    {
        if (!is_array(\$this->$phpSave)) {
            \$this->$phpSave = [];
        }
        \$this->{$phpSave}[] = \$value;
    }";
    }

    private function simpleMethod($phpSave, $cast) {
        return "
    public function get$phpSave()
    {
        return \$this->$phpSave;
    }

    public function set$phpSave($cast \$value = null)
    {
        \$this->$phpSave = \$value;
    }";
    }

    private function literalMethod($phpSave, $defaultLang, $cast) {
        return "
    public function get$phpSave(\$lang = $defaultLang)
    {
        if (!isset(\$this->{$phpSave}[\$lang])) {
            return null;
        }
        return \$this->{$phpSave}[\$lang];
    }

    public function set$phpSave($cast\$value, \$lang = $defaultLang)
    {
        \$this->{$phpSave}[\$lang] = \$value;
    }";
    }

}

