<?php

namespace Triplestore\Service;


use Zend\Filter\Word\UnderscoreToCamelCase;

/**
 * Class PhpVariableConverter converts the triplestore variables to something that php can use
 *
 * @package Triplestore\Service
 */
class PhpVariableConverter {

    /** @var \Zend\Filter\Word\UnderscoreToCamelCase  */
    protected static $filter;

    public static function init() {
    }

    /**
     * Converts the class nnames
     *
     * @param $value
     *
     * @return string
     */
    public static function toPhpClassName($value) {
        return self::toPhpMethod($value);
    }

    /**
     * Removes the special chars and capitalises the letter after the character
     *
     * @param $value
     *
     * @return string
     */
    public static function toPhpMethod($value)
    {
        if (self::$filter == null) {
            self::$filter = new UnderscoreToCamelCase();
        }
        $value =  str_replace(array('.', ':', '-'),'_',$value);
        return ucfirst(self::$filter->filter($value));
    }

} 