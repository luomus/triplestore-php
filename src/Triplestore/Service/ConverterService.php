<?php

namespace Triplestore\Service;

use Zend\Filter\Word\UnderscoreToCamelCase;

class ConverterService {

    private $filter;

    public function __construct() {
        $this->filter = new UnderscoreToCamelCase();
    }

    public function TripleToPhpVariable($value) {
        $value = str_replace(array('.', ':', '-'),'_',$value);
        return $this->filter->filter($value);
    }
}