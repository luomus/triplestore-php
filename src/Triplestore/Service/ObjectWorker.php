<?php

namespace Triplestore\Service;

use Triplestore\Options\ModuleOptions;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;

class ObjectWorker implements ServiceLocatorAwareInterface{
    use ServiceLocatorAwareTrait;

    protected $options;

    protected $modelAdapter;
    protected $adapter;

    /**
     * @return ModuleOptions
     */
    protected function getOptions() {
        if ($this->options === null) {
            $this->options = $this->serviceLocator->get('Triplestore\Options\ModuleOptions');
        }
        return $this->options;
    }

    protected function getAdapter() {

    }
}