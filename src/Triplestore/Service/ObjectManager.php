<?php
namespace Triplestore\Service;

use Common\Service\IdService;
use Triplestore\Db\TriplestoreInterface;
use Triplestore\Exception;
use Triplestore\Model\Model;
use Triplestore\Model\Predicate;
use Triplestore\Options\ModuleOptions;
use Triplestore\Stdlib\AbstractOntology;
use Triplestore\Stdlib\Hydrator\HydratorInterface;
use Triplestore\Stdlib\ModelAwareInterface;
use Triplestore\Stdlib\ModelSerializableInterface;
use Triplestore\Stdlib\SubjectAwareInterface;
use Zend\Cache\Storage\StorageInterface;
use Zend\Log\Logger;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class ObjectManager handles all the conversation between Models and the database.
 *
 * @package Triplestore\Service
 */
class ObjectManager {

    const PREDICATE_TYPE       = 'rdf:type';
    const PREDICATE_SUBCLASS   = 'rdfs:subClassOf';
    const PREDICATE_PARENT     = 'MY.isPartOf';
    const PREDICATE_PARENT_ALL = 'MZ.isPartOf';
    const PREDICATE_LABEL      = 'rdfs:label';
    const PREDICATE_VERNACULAR_NAME = 'MX.vernacularName';
    const PREDICATE_SELECT_PARENT = 'altParent';
    const PREDICATE_COMMENT    = 'rdfs:comment';
    const PREDICATE_DOMAIN     = 'rdfs:domain';
    const PREDICATE_RANGE      = 'rdfs:range';
    const PREDICATE_MIN_OCCURS = 'xsd:minOccurs';
    const PREDICATE_MAX_OCCURS = 'xsd:maxOccurs';
    const PREDICATE_SELECT_SPOT_PRE_FIX = 'rdf:_';
    const PREDICATE_ORDER      = 'sortOrder';
    const PREDICATE_EDITED     = 'MZ.dateEdited';

    const OBJ_TYPE_SELECT   = 'rdf:Alt';
    const OBJ_TYPE_CLASS    = 'rdfs:Class';
    const OBJ_TYPE_PROPERTY = 'rdf:Property';
    const TYPE_SUBPROPERTY  = 'rdfs:subPropertyOf';

    const LANG_FI = 'fi';
    const LANG_EN = 'en';
    const LANG_SV = 'sv';

    const RANGE_DATE      = 'xsd:date';
    const RANGE_DATE_TIME = 'xsd:dateTime';
    const RANGE_INTEGER   = 'xsd:integer';
    const RANGE_QNAME     = 'xsd:QName';
    const RANGE_STRING    = 'xsd:string';
    const RANGE_DECIMAL   = 'xsd:decimal';
    const RANGE_BOOLEAN   = 'xsd:boolean';
    const RANGE_RESOURCE  = 'rdfs:Resource';

    const EMBEDDABLE      = 'MZ.embeddable';
    const MULTI_LANGUAGE  = 'MZ.multiLanguage';

    const ANY_OBJECT_VALUE = '__any_object_value__';

    const OCCURS_UNBOUND = 'unbounded';

    private $db;
    private $hydrators = array();
    private $use_hydrator = true;
    private $classNS;
    private $repositoryNS;
    private $objectCache = array();
    private $cacheMaxSize = 100;


    private $className;
    private $repositorySuffix = 'Repository';
    private $metadataService;
    private $formService;
    private $cache;
    private $user = false;
    private $inTransaction = false;

    private $serviceLocator;

    private $reasonForError;


    /**
     * Initializes the object manager.
     * This is initialized in the ObjectManagerFactory
     *
     * @see \Triplestore\Service\ObjectManagerFactory
     *
     * @param TriplestoreInterface    $db
     * @param ModuleOptions           $options
     * @param StorageInterface        $cache
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function __construct(TriplestoreInterface $db, ModuleOptions $options, StorageInterface $cache, $serviceLocator = null) {
        PhpVariableConverter::init();
        $this->db = $db;
        $this->cache = $cache;
        $this->serviceLocator = $serviceLocator;
        $this->classNS = $this->canonicalNSFormat($options->getClassNS());
        $this->repositoryNS = $this->canonicalNSFormat($options->getRepositoryNS());
    }

    /**
     * Fetches a model based on the subject given.
     *
     * @param $subject
     * @param bool $deep are the children also wanted
     * @return null|Model|array
     * @throws \Triplestore\Exception\ErrorException
     */
    public function fetch($subject, $deep = true) {
        if (empty($subject)) {
            return null;
        }
        if (is_array($subject)) {
            $subject = IdService::getQName($subject, true);
            $models = $this->db->fetch($subject, $deep);
            if ($this->use_hydrator == false || $models === null) {
                return $models;
            }
            $result = array();
            foreach ($models as $model) {
                $object = $this->hydrateToClass($model);
                if ($object === null) {
                    continue;
                }
                $result[$model->getSubject()] = $object;
            }
            return $result;
        } else {
            $subject = IdService::getQName($subject, true);
            $model = $this->db->fetch($subject, $deep);
            return $this->hydrateToClass($model);
        }
    }

    private function hydrateToClass(Model $model = null, $cache = true) {
        if ($this->use_hydrator == false || $model === null) {
            return $model;
        }
        $class = $this->className === null ?
            $this->getClassByType($model) :
            $this->className;
        // In case of no correct type found
        if ($class === false) {
            return null;
        }
        $object = new $class();
        if ($object instanceof AbstractOntology) {
            $type = $object->getType();
            $typePredicate = $model->getPredicate(self::PREDICATE_TYPE);
            if ($typePredicate === null || !in_array($type, $typePredicate->getResourceValue())) {
                return null;
            }
        }
        if ($object instanceof ModelSerializableInterface) {
            $this->hydrateModel($model, $class);
            $object->exchangeModel($model);
        } else if ($object instanceof ModelAwareInterface) {
            $this->hydrateModel($model, $class);
            $object->setModel($model);
        } else {
            throw new Exception\ErrorException(
                "You should always use model aware interface with objects that you use for fetching"
            );
        }
        if ($cache) {
            $this->objectCache[$model->getSubject()] = $object;
        }
        return $object;
    }

    /**
     * Remove item from the triplestore
     *
     * @param string|ModelSerializableInterface|Model $object
     *
     * @return bool
     */
    public function remove($object) {
        $model = null;
        $isModelSerializable = $object instanceof ModelSerializableInterface;
        if ($isModelSerializable || $object instanceof Model) {
            $model = $isModelSerializable ? $object->getModelCopy() : $object;
            if (!$model->isFromDB()) {
                return $this->remove($model->getSubject());
            }
        } else if (is_string($object)) {
            $object = IdService::getQName($object, true);
            $model  = $this->db->fetch($object, true);
            if ($model == null) {
                return true;
            }
        } else {
            throw new Exception\InvalidArgumentException("Invalid type of object given to object manager!");
        }
        if (isset($this->objectCache[$model->getSubject()])) {
            unset($this->objectCache[$model->getSubject()]);
        }

        return $this->db->remove($model);
    }

    /**
     * Saves the model to the database.
     *
     * Note:
     *  If you use autocommit false. You will have to call commit manually.
     *  If you have used autocommit false and then add another with autocommit true the items are not save until you call commit!
     *
     * @param ModelSerializableInterface|Model $object
     * @param bool                             $autocommit
     * @param bool                             $deep
     *
     * @return bool
     */
    public function save($object, $autocommit = true, $deep = false) {
        $user  = $this->getUserQname();
        $model = $this->extractModels($object);

        $subject = $model->getSubject();
        if ($subject == null || trim($subject) == '' ) {
            throw new Exception\InvalidArgumentException("Model is missing subject!");
        }
        if ($autocommit === false && $this->inTransaction == false) {
            $this->inTransaction = true;
            $this->db->startTransaction();
        }

        try {
            if ($this->inTransaction == false) {
                $this->db->startTransaction();
            }
            $this->db->store($model, $user, $autocommit, $deep);
            if ($this->inTransaction == false) {
                $this->db->commit();
            }
        } catch (\Exception $e) {
            $reason = "Saving $subject to triplestore failed. " . $e->getMessage();
            if ($this->serviceLocator->has('Logger')) {
                /** @var Logger $logger */
                $logger = $this->serviceLocator->get('Logger');
                $logger->crit($reason);
            }
            $this->reasonForError = $reason;
            return false;
        }
        return true;
    }

    /**
     * @return string
     */
    public function getReasonForError()
    {
        return $this->reasonForError;
    }

    /**
     * @param string $reasonForError
     */
    public function setReasonForError($reasonForError)
    {
        $this->reasonForError = $reasonForError;
    }



    /**
     * Updates predicate of the subject
     *
     * @param      $subject
     * @param      $predicate
     * @param      $type
     * @param null $user
     *
     * @return boolean
     */
    public function updatePredicate($subject, Predicate $predicate, $type = null, $user = null) {
        if ($user == null) {
            $user = $this->getUserQname();
        }
        $subject = IdService::getQName($subject, true);
        if (isset($this->objectCache[$subject])) {
            unset($this->objectCache[$subject]);
        }
        if ($type !== null) {
            $this->hydratePredicate($predicate, $type, HydratorInterface::EXTRACT);
        }
        return $this->db->updatePredicate($subject, $predicate, $user);
    }

    public function getHistoryVersions($subject, $class = null, $exclude = null) {
        /** @var \Triplestore\Db\Oracle $db */
        $db = $this->db;
        $subject = IdService::getQName($subject, true);
        return $db->getHistoryVersions($subject, $class, $exclude, $this->getMetadataService());
    }

    public function fetchFromHistory($subject, $timestamp, $class = null) {
        /** @var \Triplestore\Db\Oracle $db */
        $db    = $this->db;
        $subject = IdService::getQName($subject, true);
        $model = $db->fetchFromHistory($subject, $timestamp, $class, $this->getMetadataService());

        return $this->hydrateToClass($model);
    }

    /**
     * Returns predicate
     *
     * @param $subject
     * @param $property
     * @param $type
     *
     * @return Predicate
     */
    public function getPredicate($subject, $property, $type = null) {
        $subject = IdService::getQName($subject, true);
        $predicate = $this->db->getPredicate($subject, $property);
        if ($type !== null && $predicate instanceof Predicate) {
            $this->hydratePredicate($predicate, $type, HydratorInterface::HYDRATE);
        }
        return $predicate;
    }

    /**
     * Commits the database transactions
     */
    public function commit() {
        $this->inTransaction = false;
        $this->db->commit();
    }

    /**
     * Returns the connection used
     * @return TriplestoreInterface
     */
    public function getConnection() {
        return $this->db;
    }

    /**
     * Returns the classname
     *
     * @param $className
     *
     * @return $this
     */
    public function className($className) {
        $this->className = $className;

        return $this;
    }

    /**
     * Returns the count based on the criteria
     *
     * @param array $criteria
     *
     * @return mixed
     */
    public function count($criteria = array()) {
        return (int)$this->db->count($criteria);
    }

    /**
     * Finds items based on criteria used and returns an array with the items.
     * Criteria is mapped so that they are PredicateName and Object or Literal values.
     *
     * Example:
     *  array('MY.lifeStage' => 'MY.lifeStageLarva') would return all the models that have larva in them
     *  array('MY.lifeStage' => 'MY.lifeStageLarva', 'MY.country' => 'Finland') would return all larva from Finland
     *
     * @param array $criteria
     * @param array $sort
     * @param null  $limit
     * @param null  $skip
     * @param array $fields
     *
     * @return array|null
     */
    public function findBy($criteria = array(), array $sort = null, $limit = null, $skip = null, array $fields = null) {
        $objects = $this->db->findBy($criteria, $sort, $limit, $skip, $fields);
        if ($objects !== null) {
            if ($this->use_hydrator == false) {
                return $objects;
            }
            foreach ($objects as $key => $model) {
                if (isset($this->className)) {
                    $class = $this->className;
                } else {
                    $class = $this->getClassByType($model);
                }
                if ($class) {
                    $object = new $class();
                    $this->hydrateModel($model, $class);
                    if ($object instanceof ModelSerializableInterface) {
                        $object->exchangeModel($model);
                        $objects[$key] = $object;
                    }
                }
            }
        }
        return $objects;
    }

    /**
     * Used to create repositories that are used to map same type of models together.
     *
     * @param $classname
     *
     * @return mixed
     */
    public function getRepository($classname)
    {
        $fqClass     = $this->repositoryNS . $classname . $this->repositorySuffix;
        $objectClass = $this->classNS . $classname;
        return new $fqClass($this, $objectClass);
    }

    /**
     * Disable the hydrator
     * You mus explicitly re enable them if this is called
     */
    public function disableHydrator() {
        $this->use_hydrator = false;
    }

    /**
     * Enables the hydrator
     */
    public function enableHydrator() {
        $this->use_hydrator = true;
    }

    /**
     * Sets the tiplestore hydrated classes namespaces
     *
     * @param $ns
     */
    public function setClassNamespace($ns) {
        $this->classNS = $this->canonicalNSFormat($ns);
    }

    /**
     * Clears the caches used.
     * When handling large amounts of objects you need to call this to free memory
     */
    public function clear() {
        $this->objectCache = array();
        $this->db->clear();
    }

    /**
     * This removes one model from the cache
     *
     * @param SubjectAwareInterface $model
     */
    public function detach(SubjectAwareInterface $model) {
        $subject = $model->getSubject();
        if (isset($this->objectCache[$subject])) {
            unset($this->objectCache);
        }
    }

    /**
     * Returns the metadata service
     *
     * @return MetadataService
     */
    public function getMetadataService() {
        if ($this->metadataService == null) {
            $this->metadataService = new MetadataService($this, $this->cache, $this->serviceLocator);
        }
        return $this->metadataService;
    }

    /**
     * Returns form service
     *
     * @return FormService
     */
    public function getFormService() {
        if ($this->formService == null) {
            $this->formService = new FormService($this, $this->cache);
        }
        return $this->formService;
    }

    public function getCache() {
        return $this->cache;
    }

    /**
     * Converts the namespaces to a format that this class can use.
     *
     * @param $ns
     *
     * @return string
     */
    private function canonicalNSFormat($ns) {
        return rtrim($ns, '\\') . '\\';
    }

    private function getUserQname() {
        if ($this->user === false) {
            if ($this->serviceLocator->has('user')) {
                $user = $this->serviceLocator->get('user');
                $this->user = $user instanceof SubjectAwareInterface ? $user->getSubject() : null;
            } else {
                return null;
            }
        }
        return $this->user;
    }

    /**
     * Extracts the model from the object
     *
     * @param ModelSerializableInterface|ModelAwareInterface|Model $model
     *
     * @return Model
     * @throws \Triplestore\Exception\InvalidArgumentException
     */
    private function extractModels($model) {
        if ($model instanceof ModelSerializableInterface) {
            $class = get_class($model);
            $model = $model->getModelCopy();
            $this->hydrateModel($model, $class, true);
        } else if ($model instanceof ModelAwareInterface) {
            $model = $model->getModel();
        } else if (!$model instanceof Model) {
            throw new Exception\InvalidArgumentException("Invalid type of object given to object manager!");
        }

        return $model;
    }

    /**
     * Hydrates and extracts the model
     *
     * This makes sure that fields that should converted from string to something are converted and the other way around.
     * Metadata hold the information which fields should be converted and which should not.
     *
     * @param Model  $model
     * @param string $class
     * @param bool   $extract
     */
    private function hydrateModel(Model $model, $class, $extract = false) {
        /** @var \Triplestore\Model\TripleObject $object */
        /** @var \Triplestore\Stdlib\Hydrator\HydratorInterface $hydrator */
        /** @var \Triplestore\Model\Metadata $metadata */
        $metadata = $this->getMetadataService()->getMetadataFor($class);
        if ($metadata == null) {
            return;
        }
        $convertible = $metadata->getConvertibleFields();
        if (count($convertible) == 0) {
            return;
        }
        $method = $extract ? HydratorInterface::EXTRACT : HydratorInterface::HYDRATE;
        foreach ($convertible as $property => $hydratorClass) {
            if (!$model->hasPredicate($property)) {
                continue;
            }
            $predicate = $model->getPredicate($property);
            if (is_integer($hydratorClass)) {
                foreach ($predicate as $object) {
                    $this->hydrateModel($object->getValue(), $predicate->getName(), $extract);
                }
                continue;
            }
            $this->hydratePredicate($predicate, $hydratorClass, $method);
        }
        $children = $metadata->getChildren();
        foreach ($children as $child) {
            $pred = $model->getPredicate($child);
            if ($pred === null) {
                continue;
            }
            foreach($pred as $object) {
                $this->hydrateModel($object->getValue(), $child, $extract);
            }
        }
    }

    private function hydratePredicate(Predicate $predicate, $hydratorClass, $type) {
        $hydrator = $this->getHydrator($hydratorClass);
        foreach ($predicate as $object) {
            $object->setValue($hydrator->$type($object->getValue()));
        }
    }

    /**
     * Return the wanted hydrator class
     *
     * @param $class
     *
     * @return HydratorInterface
     */
    private function getHydrator($class) {
        if (isset($this->hydrators[$class])) {
            return $this->hydrators[$class];
        }
        $this->hydrators[$class] = new $class();
        return $this->hydrators[$class];
    }

    /**
     * Returns the full triplestore class name based on the rdf:type predicate and false if none found
     *
     * @param Model $model
     *
     * @return bool|string
     */
    private function getClassByType(Model $model) {
        $types   = $model->getOrCreatePredicate(self::PREDICATE_TYPE);
        foreach($types as $obj) {
            $save = PhpVariableConverter::toPhpClassName($obj->getValue());
            $class  = '\\' . $this->classNS . $save;
            if (class_exists($class)) {
                return $class;
            }
        }
        return false;
    }

} 