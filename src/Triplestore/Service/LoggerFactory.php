<?php

namespace Havis\Service;

use Zend\Log\Logger;
use Zend\Log\Writer\Stream;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class LoggerFactory is factory class for creating logger.
 *
 * @package Havis\Service
 */
class LoggerFactory implements FactoryInterface
{

    /**
     * Creates the service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return Logger
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $logger   = new Logger();
        $writer   = new Stream('./data/logs/' . date('Y-m-d') . '.log');

        $logger->addWriter($writer);

        return $logger;
    }
}
