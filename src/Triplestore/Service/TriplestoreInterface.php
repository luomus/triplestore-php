<?php

namespace Triplestore\Service;

use Triplestore\Model\Model;

interface TriplestoreInterface {

    const PREDICATE_TYPE       = 'rdf:type';
    const PREDICATE_LABEL      = 'rdfs:label';
    const PREDICATE_COMMENT    = 'rdfs:comment';
    const PREDICATE_DOMAIN     = 'rdfs:domain';
    const PREDICATE_RANGE      = 'rdfs:range';
    const PREDICATE_MIN_OCCURS = 'xsd:minOccurs';
    const PREDICATE_MAX_OCCURS = 'xsd:maxOccurs';
    const PREDICATE_SELECT_SPOT_PRE_FIX = 'rdf:_';

    const OBJ_TYPE_SELECT   = 'rdf:Alt';
    const OBJ_TYPE_OBJECT   = 'rdfs:Class';
    const OBJ_TYPE_PROPERTY = 'rdf:Property';

    const LANG_FI = 'fi';
    const LANG_EN = 'en';
    const LANG_SV = 'SV';

    const RANGE_DATE      = 'xsd:date';
    const RANGE_DATE_TIME = 'xsd:dateTime';
    const RANGE_INTEGER   = 'xsd:integer';
    const RANGE_QNAME     = 'xsd:QName';
    const RANGE_STRING    = 'xsd:string';
    const RANGE_DECIMAL   = 'xsd:decimal';

    const OCCURS_UNBOUND = 'ununbounded';

    public function store(Model $model);

    public function fetch($qname);

    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null, array $fields = null);

    public function hasResource($qname);

    public function addResource($qname);

    public function addStatement($subject, $predicate, $object = null, $objectLiteral = null, $lang = null);

}
