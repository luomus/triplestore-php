<?php

namespace Triplestore\Stdlib;


use Triplestore\Exception;
use Triplestore\Service\ObjectManager;

/**
 * Class EntityRepository handles basic tasks for the repositories.
 *
 * @package Triplestore\Stdlib
 */
class EntityRepository implements ObjectRepository
{
    /**
     * @var string
     */
    protected $entityName;

    protected $deep = true;

    protected $cache = array();

    /**
     * @var ObjectManager
     */
    protected $om;


    /**
     * Initializes a new <tt>EntityRepository</tt>.
     * @param ObjectManager $om
     * @param $className
     */
    public function __construct(ObjectManager $om, $className)
    {
        $this->om = $om;
        $this->entityName = $className;
    }

    /**
     * Finds a document by its identifier
     *
     * @param string|object $subject The identifier
     * @return object The document.
     */
    public function find($subject)
    {
        if ($subject === null) {
            return null;
        }

        return $this->om->className($this->entityName)->fetch($subject, $this->deep);
    }

    /**
     * Finds all documents in the repository.
     *
     * @return array The entities.
     */
    public function findAll()
    {
        return $this->findBy(array());
    }

    /**
     * Finds documents by a set of criteria.
     *
     * @param array        $criteria Query criteria
     * @param array        $sort     Sort array for Cursor::sort()
     * @param integer|null $limit    Limit for Cursor::limit()
     * @param integer|null $skip     Skip for Cursor::skip()
     * @param array|null   $fields
     * @return array
     */
    public function findBy(array $criteria, array $sort = null, $limit = null, $skip = null, array $fields = null)
    {
        return $this->om->className($this->entityName)->findBy($criteria, $sort, $limit, $skip, $fields);
    }

    /**
     * Finds a single document by a set of criteria.
     *
     * @param array $criteria
     * @return object
     */
    public function findOneBy(array $criteria)
    {
        $result = $this->om->className($this->entityName)->findBy($criteria, null, 1);
        if (count($result) > 0) {
            return array_shift($result);
        }

        return null;
    }

    /**
     * Adds support for magic finders.
     *
     * @param string $method
     * @param array $arguments
     * @throws Exception\InvalidArgumentException
     * @throws \BadMethodCallException If the method called is an invalid find* method
     *                                 or no find* method at all and therefore an invalid
     *                                 method call.
     * @return array|object The found document/documents.
     */
    public function __call($method, $arguments)
    {
        if (substr($method, 0, 6) == 'findBy') {
            $by = substr($method, 6, strlen($method));
            $method = 'findBy';
        } elseif (substr($method, 0, 9) == 'findOneBy') {
            $by = substr($method, 9, strlen($method));
            $method = 'findOneBy';
        } else {
            throw new \BadMethodCallException(
                "Undefined method '$method'. The method name must start with " .
                "either findBy or findOneBy!"
            );
        }

        if ( ! isset($arguments[0])) {
            throw new Exception\InvalidArgumentException("$method $by requires parameter!");
        }

        return $this->$method(array($by => $arguments[0]));
    }

    /**
     * @return string
     */
    public function getEntityName()
    {
        return $this->entityName;
    }

    /**
     * @return ObjectManager
     */
    public function getObjectManager()
    {
        return $this->om;
    }

    /**
     * @return string
     */
    public function getClassName()
    {
        return $this->getEntityName();
    }
}
