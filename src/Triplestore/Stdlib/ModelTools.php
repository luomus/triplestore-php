<?php

namespace Triplestore\Stdlib;


use Triplestore\Model\Model;
use Triplestore\Model\TripleObject;
use Triplestore\Model\Predicate;
use Triplestore\Service\MetadataService;
use Triplestore\Service\ObjectManager;

/**
 * Class ModelTools tools for manipulating Models
 *
 * @package Triplestore\Stdlib
 */
class ModelTools {

    /**
     * Converts the model to an array
     *
     * @param Model           $model
     * @param MetadataService $metadataService
     * @param null            $format
     *
     * @return array
     */
    public static function ModelToArray(Model $model = null, MetadataService $metadataService = null, $format = null) {
        $return = array();
        if ($model === null) {
            return null;
        }
        $subject = $model->getSubject();
        if (!empty($subject)) {
            $return['qname'] = $subject;
        }
        $metadata = null;
        if ($metadataService !== null) {
            $typePred = $model->getPredicate(ObjectManager::PREDICATE_TYPE);
            if ($typePred !== null) {
                $type     = $typePred->getResourceValue();
                $metadata = $metadataService->getMetadataFor(array_pop($type));
            }
        }
        foreach($model as $predicate) {
            $name    = $predicate->getName();
            $lValues = [];
            foreach($predicate as $object) {
                /** @var \Triplestore\Model\TripleObject $object */
                $lang = $object->getLang();
                if (!isset($lValues[$lang])) {
                    $lValues[$lang] = [];
                }
                $value = $object->getValue();
                if ($value instanceof Model) {
                    $name  = 'has';
                    if (isset($return['has']) && empty($lValues[$lang])) {
                        $lValues[$lang] = $return['has'];
                    }
                    $value = self::ModelToArray($value, $metadataService, $format);
                }
                if ($metadata !== null && !$metadata->hasMany($name) && $name !== 'has') {
                    $lValues[$lang] = $value;
                } else {
                    $lValues[$lang][] = $value;
                }
            }
            $count = count($lValues);
            if ($count == 1 && array_key_exists('',$lValues)) {
                $return[$name] = array_pop($lValues);
            } else {
                $return[$name] = $lValues;
            }
        }

        return $return;
    }

    /**
     * Compares two models and return true if they have same data of false if not.
     *
     * @param Model $m1
     * @param Model $m2
     *
     * @return bool
     */
    public static function compare(Model $m1 = null, Model $m2 = null) {
        if ($m1 === null || $m2 === null) {
            return $m1 === null && $m2 === null;
        }
        if ($m1->getSubject() !== $m2->getSubject()) {
            return false;
        }
        foreach ($m1 as $p1) {
            /** @var \Triplestore\Model\Predicate $p1 */
            $name = $p1->getName();
            if (!$m2->hasPredicate($name)) {
                return false;
            }
            $result = self::comparePredicate($p1, $m2->getPredicate($name));
            if ($result === false) {
                return false;
            }
        }
        return true;
    }

    /**
     * Compares two predicates with one another nad return true if they hold the same data
     *
     * @param Predicate $p1
     * @param Predicate $p2
     *
     * @return bool
     */
    public static function comparePredicate(Predicate $p1 = null, Predicate $p2 = null) {
        if ($p1 === null || $p2 === null) {
            return $p1 === null && $p2 === null;
        }
        if ($p1->getName() !== $p2->getName()) {
            return false;
        }
        $checks = array();
        foreach ($p1 as $o1) {
            /** @var \Triplestore\Model\TripleObject $o1 */
            $key = '' . $o1->toString();
            $checks[$key] = $o1;
        }
        foreach ($p2 as $o2) {
            /** @var \Triplestore\Model\TripleObject $o2 */
            $key = '' . $o2->toString();
            if (!isset($checks[$key]) || !self::compareObject($checks[$key],$o2)) {
                return false;
            }
            unset($checks[$key]);
        }
        if (count($checks)) {
            return false;
        }
        return true;
    }

    /**
     * Comapres the two objects together and return true if they hold same data.
     *
     * @param $o1
     * @param $o2
     *
     * @return bool
     */
    public static function compareObject(TripleObject $o1 = null, TripleObject $o2 = null) {
        if ($o1 === null || $o2 === null) {
            return $o1 === null && $o2 === null;
        }
        $value1 = $o1->getValue();
        $value2 = $o2->getValue();
        if ($value1 instanceof Model) {
            if (!$value2 instanceof Model) {
                return false;
            }
            return self::compare($value1, $value2);
        }
        if ($value1 !== $value2 ||
            $o1->getLang() !== $o2->getLang() ||
            $o1->getType() !== $o2->getType()) {
            return false;
        }
        return true;
    }

    public static function getType(Model $model) {
        $predicate = $model->getPredicate(ObjectManager::PREDICATE_TYPE);
        if ($predicate === null) {
            return null;
        }
        return $predicate->getFirst()->getValue();
    }

    public static function hasType(Model $model = null, $type) {
        if ($model === null) {
            return false;
        }
        $predicate = $model->getPredicate(ObjectManager::PREDICATE_TYPE);
        if ($predicate === null) {
            return false;
        }
        $typeList = array_flip($predicate->getResourceValue());
        if (isset($typeList[$type])) {
            return true;
        }
        return false;
    }

} 