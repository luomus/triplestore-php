<?php

namespace Triplestore\Stdlib;

use Triplestore\Model\Model;

/**
 * Interface ModelAwareInterface
 *
 * @package Triplestore\Stdlib
 */
interface ModelAwareInterface {

    /**
     * Return the model
     * @return Model
     */
    public function getModel();

    /**
     * Sets the model
     * @param Model $model
     * @return $this
     */
    public function setModel(Model $model);
}