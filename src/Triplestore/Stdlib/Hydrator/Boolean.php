<?php
namespace Triplestore\Stdlib\Hydrator;

/**
 * Class Boolean converts value to boolean and to string
 *
 * @package Triplestore\Stdlib\Hydrator
 */
class Boolean implements HydratorInterface {

    /**
     * Converts the value to string
     *
     * @param boolean $value
     *
     * @return string
     */
    public function extract($value)
    {
        if (is_null($value) || $value === '') {
            return $value;
        }
        return $value === true || $value == 'true' ? 'true': 'false';
    }

    /**
     * Converts the value to boolean
     *
     * @param string $value
     *
     * @return bool
     */
    public function hydrate($value)
    {
        return $value === 'true';
    }
}