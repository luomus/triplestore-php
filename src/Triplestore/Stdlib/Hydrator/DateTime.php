<?php
namespace Triplestore\Stdlib\Hydrator;

/**
 * Class DateTime converts the datetime to string
 *
 * @package Triplestore\Stdlib\Hydrator
 */
class DateTime implements HydratorInterface {

    const FORMAT = DATE_ISO8601;

    /**
     * Converts the value from datetime to string
     *
     * @param \DateTime $value
     *
     * @return string
     */
    public function extract($value)
    {
        return $value instanceof \DateTime ? $value->format(self::FORMAT) : $value;

    }

    /**
     * Converts the value from string to datetime
     *
     * @param string $value
     *
     * @return \DateTime|mixed|null
     */
    public function hydrate($value)
    {
        try {
            $datetime = new \DateTime($value);
        } catch (\Exception $e) {
            return null;
        }
        return $datetime;

    }

} 