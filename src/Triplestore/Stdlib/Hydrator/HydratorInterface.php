<?php
namespace Triplestore\Stdlib\Hydrator;


/**
 * Interface HydratorInterface defines interface that can be used for converting data to database format
 *
 * @package Triplestore\Stdlib\Hydrator
 */
interface HydratorInterface {

    const TYPE_BOOL     = '\Triplestore\Stdlib\Hydrator\Boolean';
    const TYPE_DATE     = '\Triplestore\Stdlib\Hydrator\Date';
    const TYPE_DATETIME = '\Triplestore\Stdlib\Hydrator\DateTime';

    const EXTRACT       = 'extract';
    const HYDRATE       = 'hydrate';

    /**
     * Return the value in the format that the database can comprehend.
     *
     * @param mixed $value
     *
     * @return string
     */
    public function extract($value);

    /**
     * Returns the value in format that can be used afterwards.
     *
     * @param string $value
     *
     * @return mixed
     */
    public function hydrate($value);

} 