<?php
namespace Triplestore\Stdlib\Hydrator;

/**
 * Class Date converts the string to datetime class
 * inherits
 *
 * @see DateTime
 *
 * @package Triplestore\Stdlib\Hydrator
 */
class Date extends DateTime {

    /**
     * Converts the value from datetime to date string that is in ISO8601 format and drops the non essential parts
     * away from it.
     *
     * @param \DateTime $value
     *
     * @return string
     */
    public function extract($value)
    {
        $value = parent::extract($value);
        if (!is_string($value)) {
            return $value;
        }
        return substr($value, 0, 10);

    }
}