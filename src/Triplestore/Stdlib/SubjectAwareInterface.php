<?php
namespace Triplestore\Stdlib;

/**
 * Interface SubjectAwareInterface
 *
 * @package Triplestore\Stdlib
 */
interface SubjectAwareInterface {

    /**
     * Returns the subject of the model
     *
     * @return string|null
     */
    public function getSubject();

    /**
     * Sets the subject for the model
     *
     * @param string $subject
     *
     * @return void
     */
    public function setSubject($subject);

} 