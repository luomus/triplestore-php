<?php

namespace Triplestore\Stdlib;


use Triplestore\Exception;

/**
 * Class AbstractOntology has all the basic functionality that every triplestore class needs
 *
 * @package Triplestore\Stdlib
 */
abstract class AbstractOntology implements ModelSerializableInterface, OntologyInterface {

    /**
     * Subject of the object
     * @var string
     */
    protected $subject;

    protected $children = array();

    /**
     * Returns subject
     *
     * @return null|string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Sets subject of the model
     *
     * @param $subject
     * @return $this
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Returns the property names that have children
     *
     * @return array
     */
    public function getChildren() {
        return $this->children;
    }

    /**
     * Magic method to enable language specific method calls.
     * Language can be used by simply adding two letter language code to the end of method
     * (Please note that language code is camel cased. So fi becomes Fi when is part of the
     * method)
     *
     * If the method is not found in the object this is then called. This subtracts the
     * last two letters and checks if method without the two last letters can be found
     * if not null is returned. If the method exists this calls the method with the
     * parameter it was called and adds language code as the second parameter.
     *
     * It doesn't matter if the two letter code is capitalized or not. Both will work.
     *
     * @param $name
     * @param $args
     * @return mixed
     * @throws \Triplestore\Exception\BadMethodCallException
     */
    public function __call($name, $args) {
        $method = substr($name, 0, -2);
        if (!method_exists($this, $method)) {
            throw new Exception\BadMethodCallException("Invalid method called '$name'!");
        }
        $lang = lcfirst(substr($name, -2));
        if (isset($args[0])) {
            return $this->$method($args[0], $lang);
        } else {
            return $this->$method($lang);
        }
    }

    /**
     * Returns the property name based on the field and lang
     *
     * @param string $field
     * @param null   $lang
     *
     * @return string
     */
    protected function getMethod($field, $lang = null) {
        if ($lang === null) {
            return $field;
        }
        $property = $field . ucfirst($lang);
        if (!property_exists($this, $property)) {
            throw new Exception\InvalidArgumentException("Invalid language '$lang' used in field '$field'!");
        }
        return $property;
    }

    public function __clone() {
        if (count($this->children) == 0) {
            return;
        }
        foreach ($this->children as $property) {
            if (!is_array($this->$property)) {
                continue;
            }
            $result = array();
            foreach ($this->$property as $key => $value) {
                if (is_object($value)) {
                    $result[$key] = clone $value;
                }
            }
            $this->$property = $result;
        }
    }

} 