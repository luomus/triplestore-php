<?php

namespace Triplestore\Stdlib;

use Triplestore\Model\Model;
use Triplestore\Service\MetadataService;

/**
 * Interface ModelAwareInterface
 *
 * @package Triplestore\Stdlib
 */
interface ModelHydratorInterface {

    /**
     * Return the model
     * @param OntologyInterface $object
     * @param MetadataService $metadataService
     * @return Model
     */
    public function extract(OntologyInterface $object, MetadataService $metadataService);

    /**
     * Hydrates $object with the provided $model
     *
     * @param Model $model
     * @param OntologyInterface $object
     * @param MetadataService $metadataService
     * @return $this
     */
    public function hydrate(Model $model, OntologyInterface $object, MetadataService $metadataService);
}