<?php

namespace Triplestore\Stdlib;

use Triplestore\Model\Model;

/**
 * Interface ModelSerializableInterface
 *
 * @package Triplestore\Stdlib
 */
interface ModelSerializableInterface
{
    /**
     * Exchange internal values from provided Model
     *
     * @param  Model $array
     * @return void
     */
    public function exchangeModel(Model $array);

    /**
     * Return an Model representation of the object
     *
     * @return Model
     */
    public function getModelCopy();
}