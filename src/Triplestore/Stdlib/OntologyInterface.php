<?php
namespace Triplestore\Stdlib;

/**
 * Interface OntologyInterface
 *
 * @package Triplestore\Stdlib
 */
interface OntologyInterface extends SubjectAwareInterface {

    /**
     * Returns the rdf:type value
     *
     * @return mixed
     */
    public function getType();

}