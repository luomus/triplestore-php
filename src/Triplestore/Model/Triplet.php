<?php
namespace Triplestore\Model;


class Triplet {

    /** @var int */
    protected $statementID;
    /** @var String */
    protected $subject;
    /** @var String */
    protected $predicate;
    /** @var String */
    protected $objectLiteral;
    /** @var String */
    protected $objectResource;
    /** @var String */
    protected $lang;

    public function __construct($stid = null, $subject = null, $predicate = null, $objectResource = null, $objectLiteral = null, $lang = null) {
        $this->statementID = $stid;
        $this->subject = $subject;
        $this->predicate = $predicate;
        $this->objectResource = $objectResource;
        $this->objectLiteral = $objectLiteral;
        $this->lang = $lang;
    }

    /**
     * @return int
     */
    public function getStatementID()
    {
        return $this->statementID;
    }

    /**
     * @return String
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @return String
     */
    public function getPredicate()
    {
        return $this->predicate;
    }

    /**
     * @return String
     */
    public function getObjectLiteral()
    {
        return $this->objectLiteral;
    }

    /**
     * @return String
     */
    public function getObjectResource()
    {
        return $this->objectResource;
    }

    /**
     * @return String
     */
    public function getLang()
    {
        return $this->lang;
    }

    public function getValue() {
        return $this->isLiteral() ? $this->objectLiteral : $this->objectResource;
    }

    public function isLiteral() {
        return isset($this->objectLiteral);
    }

}