<?php

namespace Triplestore\Model;


use Triplestore\Service\ObjectManager;

class TripleSet {

    protected $byType = [];
    protected $byPredicate = [];
    protected $bySubject = [];
    protected $triplets = [];
    protected $typePredicates = [
        ObjectManager::PREDICATE_TYPE => true,
        ObjectManager::PREDICATE_SUBCLASS => true,
    ];

    public function addTriplet(Triplet $triplet) {
        $this->triplets[] = $triplet;
        $predicate = $triplet->getPredicate();
        $subject = $triplet->getSubject();
        $this->byPredicate[$predicate][] = $triplet;
        $this->bySubject[$subject][] = $triplet;
        if (isset($this->typePredicates[$predicate])) {
            $this->byType[$subject] = $triplet->getValue();
        }
    }

    public function getBySubject($subject = null) {
        return $this->getBy('bySubject', $subject);
    }

    public function getByType($subject = null) {
        return $this->getBy('byType', $subject);
    }

    public function getByPredicate($predicate = null) {
        return $this->getBy('byPredicate', $predicate);
    }

    protected function getBy($type, $key = null) {
        if ($key === null) {
            return $this->$type;
        }
        if (!isset($this->$type[$key])) {
            return [];
        }
        return $this->$type[$key];
    }

    public function hasResult() {
        return !empty($this->triplets);
    }

    public function __toString() {
        $str = '';

        foreach ($this->triplets as $triple) {
            /** @var Triplet $triple */
            $str .= $triple->getSubject() . ':' . $triple->getPredicate() . $triple->getLang() . '=' . $triple->getValue() . "\n";
        }

        return $str;
    }

}