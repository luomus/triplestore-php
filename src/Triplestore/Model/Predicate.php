<?php
namespace Triplestore\Model;

/**
 * Class Predicate belongs to model and has objects
 *
 * @package Triplestore\Model
 */
class Predicate implements \Iterator {

    /** @var string predicate name */
    private $predicate;
    /** @var array of objects */
    private $objects = array();
    /** @var array of object by the language */
    private $lang = array();
    /** @var array of all the resources */
    private $resource = array();

    /**
     * Constructor fot the predicate.
     *
     * @param string $predicate
     */
    public function __construct($predicate = null)
    {
        $this->predicate = $predicate;
    }

    /**
     * Return the name of the Predicate
     *
     * @return string
     */
    public function getName()
    {
        return $this->predicate;
    }

    /**
     * Sets the name for the Predicate
     * @param $predicate
     * @return $this
     */
    public function setName($predicate)
    {
        $this->predicate = $predicate;

        return $this;
    }


    /**
     * Sets the object
     *
     * @param \Triplestore\Model\TripleObject $object
     */
    public function setObject($object)
    {
        $this->clear();
        if (is_array($object)) {
            foreach ($object as $obj) {
                $this->addObject($obj);
            }
        } else {
            $this->addObject($object);
        }
    }

    /**
     * Adds the object for the given predicate
     * and creates link to language
     *
     * @param \Triplestore\Model\TripleObject $object
     */
    public function addObject(TripleObject $object)
    {
        $key = spl_object_hash($object);
        $this->objects[$key] = $object;
        if ($object->getType() === TripleObject::LITERAL) {
            $lang = $object->getLang();
            if (!isset($this->lang[$lang])) {
                $this->lang[$lang] = array();
            }
            $this->lang[$lang][$key] = &$this->objects[$key];
        } else {
            $this->resource[$key] = &$this->objects[$key];
        }
    }

    /**
     * Gives all the literal values with the given language
     *
     * @param string $lang
     * @return array
     */
    public function getLiteralValue($lang = null) {
        if (!isset($this->lang[$lang])) {
            return array();
        }
        return $this->getValue($this->lang[$lang]);

    }

    /**
     * Gives all the resource values
     *
     * @return array
     */
    public function getResourceValue() {
        return $this->getValue($this->resource);
    }

    /**
     * Clear all the resources values and replaces it with the ones that are given in the parameters
     *
     * @param mixed $value
     */
    public function setResourceValue($value) {
        $this->clearResource();
        if(!is_array($value)) {
            $value = array($value);
        }
        foreach($value as $val) {
            $this->addResourceValue($val);
        }
    }

    /**
     * Clears the literal values and then populates literal values with the given
     *
     * @param mixed       $value
     * @param null|string $lang
     */
    public function setLiteralValue($value, $lang =null) {
        if (isset($this->lang[$lang])) {
            $this->clearLiteral($lang);
        } else {
            $this->lang[$lang] = array();
        }

        if(!is_array($value)) {
            $value = array($value);
        }
        foreach($value as $val) {
            $this->addLiteralValue($val, $lang);
        }
    }

    /**
     * Adds new resource value to predicate
     *
     * @param $value
     */
    public function addResourceValue($value) {
        $this->addObject(new TripleObject($value, null));
    }

    /**
     * Adds new literal value to predicate
     *
     * @param $value
     * @param string $lang
     */
    public function addLiteralValue($value, $lang = null) {
        $this->addObject(new TripleObject($value, $lang, null, TripleObject::LITERAL));
    }

    /**
     * Return an array that has unique object key and the object it self in the array
     *
     * @return array
     */
    public function getStatementChecks() {
        $ret = array();
        foreach($this->objects as $object) {
            /** @var \Triplestore\Model\TripleObject $object */
            $chk = '' . $object->toString();
            if (!isset($ret[$chk])) {
                $ret[$chk] = array();
            }
            $ret[$chk][] = $object;
        }

        return $ret;
    }

    /**
     * Checks if the value exists in the predicate
     *
     * @param      $value
     * @param null $lang
     *
     * @return bool
     */
    public function hasValue($value, $lang = null) {
        foreach ($this->objects as $object) {
            /** @var \Triplestore\Model\TripleObject $object */
            if ($object->getValue() === $value && $object->getLang() == $lang) {
                return true;
            }
        }
        return false;
    }

    private function clear() {
        $this->objects = array();
        $this->lang = array();
        $this->resource = array();
    }

    public function getFirst() {
        $this->rewind();
        return $this->current();
    }

    /**
     * Returns all the values in an array
     *
     * @param $objects
     *
     * @return array
     */
    private function getValue(&$objects) {
        $ret = array();
        foreach($objects as $object) {
            $ret[] = $object->getValue();
        }
        return $ret;
    }

    /**
     * Empties all the resources
     */
    private function clearResource() {
        foreach ($this->resource as $key => $value) {
            unset($this->objects[$key]);
        }
        $this->resource = array();
    }

    /**
     * Clears all the literal values with the given lang
     * @param null $lang
     */
    private function clearLiteral($lang = null) {
        if (!isset($this->lang[$lang])) {
            return;
        }
        foreach ($this->lang[$lang] as $key => $value) {
            unset($this->objects[$key]);
        }
        $this->lang[$lang] = array();
    }

    /**
     * Return the count of objects in this predicate
     *
     * @return int
     */
    public function count()
    {
        return count($this->objects);
    }


    /**
     * Return the current element
     * @link http://php.net/manual/en/iterator.current.php
     * @return \Triplestore\Model\TripleObject
     */
    public function current()
    {
        return current($this->objects);
    }

    /**
     * Move forward to next element
     * @link http://php.net/manual/en/iterator.next.php
     * @return \Triplestore\Model\TripleObject
     */
    public function next()
    {
        return next($this->objects);
    }

    /**
     * Return the key of the current element
     * @link http://php.net/manual/en/iterator.key.php
     * @return mixed scalar on success, or null on failure.
     */
    public function key()
    {
        return key($this->objects);
    }

    /**
     * Checks if current position is valid
     * @link http://php.net/manual/en/iterator.valid.php
     * @return boolean The return value will be casted to boolean and then evaluated.
     * Returns true on success or false on failure.
     */
    public function valid()
    {
        return key($this->objects) !== null;
    }

    /**
     * Rewind the Iterator to the first element
     * @link http://php.net/manual/en/iterator.rewind.php
     * @return void Any returned value is ignored.
     */
    public function rewind()
    {
        reset($this->objects);
    }

    /**
     * Deep cloning for the object to ensure that everything is cloned
     */
    public function __clone() {
        $objects = $this->objects;
        $this->objects = array();
        foreach ($objects as $object) {
            $this->addObject(clone $object);
        }
    }

} 