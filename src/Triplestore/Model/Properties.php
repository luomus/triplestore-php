<?php
namespace Triplestore\Model;

use Triplestore\Service\PhpVariableConverter;

/**
 * Class Properties holds information about all the properties in the triplestore
 *
 * @package Triplestore\Model
 */
final class Properties {

    static private $instance = null;

    protected $properties = array();
    protected $convertible = array();
    protected $sorted = false;
    protected $defaults = array(
        'hasMany' => false,
    );
    private $aliases = array();
    private $aliasesInitialized = false;
    private $canHaveLangVersions = [
        'xsd:string'
    ];

    /**
     * Adds new property and sets the default values for it
     * @param $property
     */
    public function addProperty($property) {
        $this->properties[$property] = array(
            'max'        => 1,
            'min'        => 1,
            'lang'       => null,
            'order'      => -1,
            'type'       => null,
            'label'      => null,
            'help'       => null,
            'multiLang'  => false,
            'embeddable' => false,
            'subPropertyOf' => null,
        );
    }

    /**
     * @param $property
     *
     * @return bool
     */
    public function hasProperty($property) {
        return isset($this->properties[$property]);
    }

    /**
     * Sets the property as convertible
     *
     * @param $property
     * @param $class
     */
    public function setConvertible($property, $class) {
        $this->convertible[$property] = $class;
    }

    /**
     * Checks if the property is convertible
     *
     * @param $property
     *
     * @return bool
     */
    public function isConvertible($property) {
        return isset($this->convertible[$property]);
    }

    /**
     * Retrurn all the fields that are convertible in this class
     *
     * @return array
     */
    public function getConvertibleFields() {
        return $this->convertible;
    }

    /**
     * Returns the class name that can be used fot converting the value.
     *
     * @param $property
     *
     * @return string
     */
    public function getConverterClassName($property) {
        return $this->convertible[$property];
    }

    /**
     * Returns all the properties
     *
     * @return array
     */
    public function getProperties() {
        if (!$this->sorted) {
            $this->arrangeProperties();
        }

        return array_keys($this->properties);
    }

    /**
     * Returns the order of the property
     *
     * @param $property
     *
     * @return int|null
     */
    public function getOrder($property) {
        if (!isset($this->properties[$property])) {
            return null;
        }
        return $this->properties[$property]['order'];
    }

    /**
     * Sets the order for the property
     *
     * @param $property
     * @param $order
     */
    public function setOrder($property, $order) {
        $this->properties[$property]['order'] = $order;
    }

    /**
     * Get default language for the given property
     *
     * @param $property
     *
     * @return null|string
     */
    public function getDefaultLang($property) {
        if (!isset($this->properties[$property])) {
            return null;
        }
        return $this->properties[$property]['lang'];
    }

    /**
     * Sets the default language of the property
     *
     * @param string $property
     * @param string $lang
     *
     * @return $this
     */
    public function setDefaultLang($property, $lang) {
        $this->properties[$property]['lang'] = $lang;

        return $this;
    }

    /**
     * Returns true if the property has multiple languages
     *
     * @param $property
     *
     * @return bool
     */
    public function isMultiLanguage($property) {
        return (isset($this->properties[$property]) && $this->properties[$property]['multiLang'] === true);
    }

    /**
     * Set properties multi language value
     *
     * @param string $property
     * @param bool   $multiLang
     */
    public function setMultiLanguage($property, $multiLang) {
        $this->properties[$property]['multiLang'] = (bool) $multiLang;
    }

    /**
     * Return true if the property can hold embedded object
     *
     * @param $property
     * @return bool
     */
    public function isEmbeddable($property) {
        return (isset($this->properties[$property]) && $this->properties[$property]['embeddable'] === true);
    }

    /**
     * Set properties embeddable value
     *
     * @param string $property
     * @param bool   $embeddable
     */
    public function setEmbeddable($property, $embeddable) {
        $this->properties[$property]['embeddable'] = (bool) $embeddable;
    }

    /**
     * Return true if the property has subPropertyOf set
     *
     * @param $property
     * @return bool
     */
    public function hasSubPropertyOf($property) {
        return (isset($this->properties[$property]) && $this->properties[$property]['subPropertyOf'] !== null);
    }

    /**
     * Returns the value of subPropertyOf parameter
     *
     * @param $property
     * @return null|string
     */
    public function getSubPropertyOf($property) {
        if (!$this->hasSubPropertyOf($property)) {
            return null;
        }
        return $this->properties[$property]['subPropertyOf'];
    }

    /**
     * Sets property to be subPropertyOf of the given value
     *
     * @param $property
     * @param $subPropertyOf
     */
    public function setSubPropertyOf($property, $subPropertyOf) {
        $this->properties[$property]['subPropertyOf'] = $subPropertyOf;
    }

    /**
     * Sets the label of the property
     *
     * @param $property
     * @param $label
     * @param $lang
     */
    public function setLabel($property, $label, $lang = 'en') {
        $this->properties[$property]['label'][$lang] = $label;
    }

    /**
     * Returns label for the property
     *
     * @param string $property
     * @param string $lang
     * @param bool   $onEmpty
     *
     * @return null|string
     */
    public function getLabel($property, $lang = 'en', $onEmpty = false) {
        if (isset($this->properties[$property])) {
            if (isset($this->properties[$property]['label'][$lang])) {
                return $this->properties[$property]['label'][$lang];
            }
        }
        if (isset($this->aliases[$property])) {
            $alias = $this->aliases[$property];
            if (isset($this->properties[$alias]['label'][$lang])) {
                return $this->properties[$alias]['label'][$lang];
            }
        }

        return $onEmpty === false ? $property : $onEmpty;
    }

    public function getAlias($property) {
        if (isset($this->properties[$property]) && isset($this->properties[$property]['alias'])) {
            return $this->properties[$property]['alias'];
        }
        return null;
    }

    /**
     * Sets help text for the property
     *
     * @param string $property
     * @param string $help
     */
    public function setHelp($property, $help) {
        $this->properties[$property]['help'] = $help;
    }

    /**
     * Gets the help text for the property
     *
     * @param string $property
     *
     * @return null
     */
    public function getHelp($property) {
        if (!isset($this->properties[$property])) {
            return null;
        }
        return $this->properties[$property]['help'];
    }

    /**
     * Returns the type of the property
     *
     * @param $property
     *
     * @return null
     */
    public function getType($property) {
        if (!isset($this->properties[$property])) {
            return null;
        }
        return $this->properties[$property]['type'];
    }

    /**
     * Sets the type of the property
     *
     * @param $property
     * @param $type
     */
    public function setType($property, $type) {
        $this->properties[$property]['type'] = $type;
    }

    /**
     * Return true if the property is literal
     *
     * @param $property
     *
     * @return bool
     * @throws \Exception if property is not defined
     */
    public function isLiteral($property) {
        if (!isset($this->properties[$property])) {
            return false;
            //throw new \Exception("Cannot determinate the type of property $property");
        }
        return (strpos($this->getType($property), 'xsd:') === 0);
    }

    /**
     * Return true if the property is resource
     *
     * @param $property
     *
     * @return bool
     * @throws \Exception if property is not defined
     */
    public function isResource($property) {
        return !$this->isLiteral($property);
    }

    /**
     * Returns true if the property can have multiple values
     *
     * @param $property
     *
     * @return bool
     */
    public function hasMany($property) {
        if (!isset($this->properties[$property])) {
            return $this->defaults['hasMany'];
        }
        return ($this->properties[$property]['max'] > 1 || $this->properties[$property]['max'] == 'unbounded');
    }

    /**
     * Returns true if the property is required
     *
     * @param $property
     *
     * @return bool
     */
    public function isRequired($property) {
        return (isset($this->properties[$property]) && $this->properties[$property]['min'] > 0);
    }

    /**
     * Sets the maximum occurrence of the property
     *
     * @param $property
     * @param $max
     */
    public function setMax($property, $max) {
        $this->properties[$property]['max'] = $max;
    }

    /**
     * Sets the minimum occurrence of the property
     *
     * @param $property
     * @param $min
     */
    public function setMin($property, $min) {
        $this->properties[$property]['min'] = $min;
    }

    /**
     * Arranges properties based on the sortOrder
     */
    public function arrangeProperties() {
        uasort($this->properties, array($this, 'compareOrderArray'));
        $this->sorted = true;
    }

    /**
     * Gets the original field value from alias
     *
     * @param string $field
     * @return null|string
     */
    public function getOriginal($field) {
        if (!$this->aliasesInitialized) {
            $this->initAliases();
        }
        if (isset($this->aliases[$field])) {
            return $this->aliases[$field];
        }
        return null;
    }

    /**
     * Makes the aliases of the properties
     */
    public function initAliases() {
        if (count($this->properties) == count($this->aliases)) {
            return;
        }
        PhpVariableConverter::init();
        foreach ($this->properties as $field => $value) {
            $converted = PhpVariableConverter::toPhpMethod($field);
            $this->aliases[$converted] = $field;
            $this->properties[$field]['alias'] = $converted;
        }
        $this->aliasesInitialized = true;
    }

    /**
     * Compares the sort order of the properties
     *
     * @param string $a
     * @param string $b
     *
     * @return int
     */
    public function compareProperty($a, $b) {
        $a = $this->getOrder($a);
        $b = $this->getOrder($b);
        return $this->compare($a, $b);
    }

    /**
     * Compares property arrays order key
     *
     * @param $a
     * @param $b
     *
     * @return int
     */
    private function compareOrderArray($a, $b) {
        return $this->compare($a['order'], $b['order']);
    }

    /**
     * Compares the integer values
     *
     * @param $a
     * @param $b
     *
     * @return int
     */
    private function compare($a, $b) {
        if ($a == $b) {
            return 0;
        }
        return $a < $b ? -1 : 1;
    }

    /**
     * Gets the instance of the object
     *
     * @return self
     */
    public static function instance()
    {
        if(self::$instance == NULL){
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * destroys the instance
     */
    public function destroy() {
        self::$instance = null;
    }

    /**
     * This is private to ensure that no other instances are used
     */
    private function __construct()
    {

    }

    /**
     * Prevents cloning on the singleton
     */
    public function __clone() {
        trigger_error( "Cannot clone instance of Singleton pattern ...", E_USER_ERROR );
    }

    /**
     * Set's the instance when this is waken up
     */
    public function __wakeup() {
        self::$instance = $this;
    }
} 