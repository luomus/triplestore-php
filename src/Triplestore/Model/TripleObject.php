<?php

namespace Triplestore\Model;


use Triplestore\Exception\InvalidArgumentException;
use Triplestore\Exception\UnexpectedValueException;

/**
 * Class Object contains the value
 *
 * @package Triplestore\Model
 */
class TripleObject {

    const LITERAL  = 'lit';
    const RESOURCE = 'res';

    /** @var string tells what kind of object is this*/
    private $type;
    /** @var mixed value of object. This gets hydrated to different form */
    private $value;
    /** @var string|null language */
    private $lang;
    /** @var string|null statements id value */
    private $statementId;

    /** @var array of allowed types */
    private $allowedTypes = array(
        self::LITERAL,
        self::RESOURCE
    );

    /**
     * Constructor for Object
     *
     * @param string $value
     * @param string $lang
     * @param string $sid
     * @param string $type
     */
    public function __construct($value = null, $lang = null, $sid = null, $type = self::RESOURCE)
    {
        $this->value = $value;
        $this->lang = $lang;
        $this->type = $type;
        $this->statementId = $sid;
    }


    /**
     * Sets the language
     *
     * @param string $lang
     */
    public function setLang($lang)
    {
        $this->lang = $lang;
    }


    /**
     * Return the language
     *
     * @return string|null
     */
    public function getLang()
    {
        return $this->lang;
    }

    /**
     * Sets the type
     *
     * @param string $type
     * @throws \Triplestore\Exception\InvalidArgumentException
     */
    public function setType($type)
    {
        if (!in_array($type, $this->allowedTypes)) {
            throw new InvalidArgumentException(sprintf("Invalid type '%s' given to object", $type));
        }
        $this->type = $type;
    }

    /**
     * Return the type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Sets the value
     *
     * @param $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * Return the value
     *
     * @return string|Model
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Sets the statement id
     *
     * @param string $statement
     */
    public function setStatementId($statement)
    {
        $this->statementId = $statement;
    }

    /**
     * Return the statement id
     *
     * @return null|string
     */
    public function getStatementId()
    {
        return $this->statementId;
    }

    /**
     * Returns true if the object is literal
     *
     * @return bool
     */
    public function isLiteral() {
        return $this->type === self::LITERAL;
    }

    /**
     * String representation of this object
     *
     * @return string
     * @throws \Triplestore\Exception\UnexpectedValueException
     */
    public function toString() {
        if (is_null($this->value) ||
            is_string($this->value) ||
            $this->value instanceof Model) {
            return $this->type .':' . $this->value . ':' . $this->lang;

        }
        throw new UnexpectedValueException('Object value could not be converted to string! ' . json_encode($this->value));
    }

}