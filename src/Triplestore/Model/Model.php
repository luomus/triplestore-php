<?php
namespace Triplestore\Model;


use Triplestore\Stdlib\SubjectAwareInterface;

/**
 * Class Model basic Model that can hold the data from the triplestore
 *
 * @package Triplestore\Model
 */
class Model implements \Iterator,\Countable, SubjectAwareInterface {

    /** @var array of predicates */
    private $predicates = array();
    /** @var string subject name  */
    private $subject;
    private $fromDB = false;
    private $has = array();

    /**
     * This sets the from database value so that if it is we can trust it's statement id's
     *
     * @param boolean $fromDB
     */
    public function setFromDB($fromDB)
    {
        $this->fromDB = (bool)$fromDB;
    }

    /**
     * Return true if the model is fetched from the database
     *
     * @return boolean
     */
    public function isFromDB()
    {
        return $this->fromDB;
    }

    /**
     * Constructor for the Model
     * @param string $subject
     */
    public function __construct($subject = null) {
        $this->subject = $subject;
    }

    /**
     * Sets the subject
     *
     * @param $subject
     * @return $this
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Return the subject
     *
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Adds the predicate to the Model.
     * If one with the same name already exists this will override it
     *
     * @param Predicate $predicate
     * @return \Triplestore\Model\Model
     */
    public function setPredicate(Predicate $predicate) {
        $this->predicates[$predicate->getName()] = $predicate;

        return $this;
    }


    /**
     * Checks if the predicate exists in the model
     *
     * @param $predicateName
     * @return bool
     */
    public function hasPredicate($predicateName) {
        return isset($this->predicates[$predicateName]);
    }

    /**
     * Returns the given predicate and if one doesn't exists creates new one
     * with the predicate name.
     *
     * @param $predicate
     * @return Predicate
     */
    public function getOrCreatePredicate($predicate) {
        if (!isset($this->predicates[$predicate])) {
            $this->predicates[$predicate] = new Predicate($predicate);
        }
        return $this->predicates[$predicate];

    }

    /**
     * Return the given predicate
     *
     * @param $predicate
     * @return Predicate|null
     */
    public function getPredicate($predicate) {
        if (!isset($this->predicates[$predicate])) {
            return null;
        }
        return $this->predicates[$predicate];
    }

    /**
     * Removes predicate from the model
     *
     * @param $predicate
     */
    public function removePredicate($predicate) {
        if (isset($this->predicates[$predicate])) {
            /** @var Predicate $predicate */
            $predicate = $this->predicates[$predicate];
            $predicate->setLiteralValue(array(''));
            $predicate->setResourceValue(array(''));
        }
    }

    /**
     * Return the current element
     * @link http://php.net/manual/en/iterator.current.php
     * @return \Triplestore\Model\Predicate
     */
    public function current()
    {
        return current($this->predicates);
    }

    /**
     * Move forward to next element
     * @link http://php.net/manual/en/iterator.next.php
     * @return \Triplestore\Model\Predicate
     */
    public function next()
    {
        return next($this->predicates);
    }

    /**
     * Return the key of the current element
     * @link http://php.net/manual/en/iterator.key.php
     * @return mixed scalar on success, or null on failure.
     */
    public function key()
    {
        return key($this->predicates);
    }

    /**
     * Checks if current position is valid
     * @link http://php.net/manual/en/iterator.valid.php
     * @return boolean The return value will be casted to boolean and then evaluated.
     * Returns true on success or false on failure.
     */
    public function valid()
    {
        return key($this->predicates) !== null;
    }

    /**
     * Rewind the Iterator to the first element
     * @link http://php.net/manual/en/iterator.rewind.php
     * @return void Any returned value is ignored.
     */
    public function rewind()
    {
        reset($this->predicates);
    }

    /**
     * Deep cloning of the model is necessary for save cloning
     */
    public function __clone() {
        foreach ($this->predicates as $key => $value) {
            $this->predicates[$key] = clone $value;
        }
        foreach ($this->has as $key => $value) {
            $this->has[$key] = clone $value;
        }
    }

    /**
     * Return the unique information for this model
     *
     * @return string
     */
    public function __toString() {
        return "Model:" . $this->subject;
    }

    /**
     * (PHP 5 &gt;= 5.1.0)<br/>
     * Count elements of an object
     *
     * @link http://php.net/manual/en/countable.count.php
     * @return int The custom count as an integer.
     *       </p>
     *       <p>
     *       The return value is cast to an integer.
     */
    public function count()
    {
        return count($this->predicates);
    }
}