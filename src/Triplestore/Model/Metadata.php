<?php

namespace Triplestore\Model;

use Triplestore\Exception;

/**
 * Class Metadata holds the metadata that was gathered from the database
 *
 * @package Triplestore\Model
 */
class Metadata {

    private $classname;
    private $extends;
    private $children = array();
    private $classProperties = array();
    private $childrensProperties = array();
    /** @var  Properties */
    private $properties;
    private $langCodes = ['En','Fi', 'Sv'];

    /**
     * Initializes the object
     */
    public function __construct()
    {
        $this->setProperties();
    }

    /**
     * Magic method that is called when the object is serialized for storing
     *
     * @return array
     */
    public function __sleep() {
        return array('classname', 'extends', 'children', 'classProperties', 'childrensProperties');
    }

    /**
     * Magic method that is called when the object is fetched back
     */
    public function __wakeup() {
        $this->setProperties();
    }

    /**
     * Sets the common properties instance
     */
    public function setProperties() {
        $this->properties = Properties::instance();
    }

    /**
     * Gives properties class that has information about all of the properties in triplestore.
     * @return Properties
     */
    public function getAllProperties() {
        return $this->properties;
    }

    /**
     * Sets the class name
     *
     * @param string $className
     */
    public function setClassName($className) {
        $this->classname = $className;
    }

    /**
     * Returns the class name
     *
     * @return string
     */
    public function getClassName() {
        return $this->classname;
    }

    /**
     * Returns the class name that the class is extending
     *
     * @return string
     */
    public function getExtends()
    {
        return $this->extends;
    }

    /**
     * Sets the class name that the class is extending
     *
     * @param string $extends
     */
    public function setExtends($extends)
    {
        $this->extends = $extends;
    }

    /**
     * Add new property information to the class
     *
     * @param $property
     */
    public function addProperty($property) {
        $this->classProperties[$property] = true;
        $this->properties->addProperty($property);
    }

    /**
     * Adds children that the class can have
     *
     * @param array $children
     *
     * @return $this
     */
    public function setChildren(array $children) {
        $this->children = $children;
        foreach ($children as $child => $value) {
            if (!$this->hasProperty($child)) {
                $this->addProperty($child);
                $this->setType($child, $child);
                $this->setMin($child, 0);
                $this->setMax($child, 'unbounded');
            }
        }

        return $this;
    }

    /**
     * if the property given contains children this return true
     *
     * @param string $property
     *
     * @return bool
     */
    public function hasChildren($property) {
        return isset($this->children[$property]);
    }

    /**
     * Returns all the children
     *
     * @return array
     */
    public function getChildren() {
        return array_keys($this->children);
    }

    /**
     * Returns all children so that the key is the class and value is the predicate holding this value
     *
     * @return array
     */
    public function getChildrenWithPredicate() {
        return $this->children;
    }

    /**
     * Returns an array of all the properties that can be used for finding children to this class
     *
     * @return array
     */
    public function getChildrensProperties()
    {
        return $this->childrensProperties;
    }

    /**
     * Sets array of all the properties that can be used to find children
     *
     * @param array $childProperties
     */
    public function setChildrensProperties($childProperties)
    {
        $this->childrensProperties = $childProperties;
    }

    /**
     * @see \Triplestore\Model\Properties::setConvertible
     */
    public function setConvertible($property, $class) {
        $this->properties->setConvertible($property, $class);
    }

    /**
     * @see \Triplestore\Model\Properties::isConvertible
     */
    public function isConvertible($property) {
        return $this->properties->isConvertible($property);
    }

    /**
     * Retrurn all the fields that are convertible in this class
     *
     * @return array
     */
    public function getConvertibleFields() {
        $convertible = $this->properties->getConvertibleFields();
        return array_intersect_key($convertible, $this->classProperties);
    }

    /**
     * @see \Triplestore\Model\Properties::getConverterClassName
     */
    public function getConverterClassName($property) {
        return $this->properties->getConverterClassName($property);
    }

    /**
     * Returns all the properties used in this class
     *
     * @return array
     */
    public function getProperties() {
        $properties = $this->properties->getProperties();
        $properties = array_intersect_key(array_flip($properties), $this->classProperties);

        return array_keys($properties);
    }

    public function getRequiredProperties($visible = true) {
        $properties = $this->getProperties();
        $properties = array_filter($properties, array($this, 'isRequired'));
        if (!$visible) {
            return $properties;
        }
        $return = [];
        foreach ($properties as $property) {
            if ($this->getOrder($property) > 0) {
                $return[] = $property;
            }
        }
        return $return;
    }

    /**
     * @see \Triplestore\Model\Properties::getOrder
     */
    public function getOrder($property) {
        return $this->properties->getOrder($property);
    }

    /**
     * @see \Triplestore\Model\Properties::setOrder
     */
    public function setOrder($property, $order) {
        $this->properties->setOrder($property, $order);
    }

    /**
     * G@see \Triplestore\Model\Properties::getDefaultLang
     */
    public function getDefaultLang($property) {
        return $this->properties->getDefaultLang($property);
    }

    /**
     * \Triplestore\Model\Properties::setDefaultLang
     */
    public function setDefaultLang($property, $lang) {
        $this->properties->setDefaultLang($property, $lang);

        return $this;
    }

    /**
     * @see \Triplestore\Model\Properties::isMultiLanguage
     */
    public function isMultiLanguage($property) {
        return $this->properties->isMultiLanguage($property);
    }

    /**
     * @see \Triplestore\Model\Properties::setLabel
     */
    public function setLabel($property, $label, $lang = 'en') {
        $this->properties->setLabel($property, $label, $lang);
    }

    /**
     * @see \Triplestore\Model\Properties::getLabel
     */
    public function getLabel($property, $lang = 'en', $onEmpty = false) {
        if (!$this->properties->hasProperty($property) && in_array(substr($property, -2), $this->langCodes)) {
            $property = substr($property, 0, -2);
        }
        return $this->properties->getLabel($property, $lang, $onEmpty);
    }


    /**
     * @see \Triplestore\Model\Properties::setHelp
     */
    public function setHelp($property, $help) {
        $this->properties->setHelp($property, $help);
    }

    /**
     * @see \Triplestore\Model\Properties::getHelp
     */
    public function getHelp($property) {
        return $this->properties->getHelp($property);
    }

    /**
     * @see \Triplestore\Model\Properties::getType
     */
    public function getType($property) {
        return $this->properties->getType($property);
    }

    /**
     * @see \Triplestore\Model\Properties::setType
     */
    public function setType($property, $type) {
        $this->properties->setType($property, $type);
    }

    /**
     * \Triplestore\Model\Properties::isLiteral
     */
    public function isLiteral($property) {
        return $this->properties->isLiteral($property);
    }

    /**
     * @see \Triplestore\Model\Properties::isResource
     */
    public function isResource($property) {
        return $this->properties->isResource($property);
    }

    /**
     * @see \Triplestore\Model\Properties::hasMany
     */
    public function hasMany($property) {
        return $this->properties->hasMany($property);
    }

    public function hasProperty($property) {
        return $this->properties->hasProperty($property);
    }

    /**
     * @see \Triplestore\Model\Properties::isRequired
     */
    public function isRequired($property) {
        return $this->properties->isRequired($property);
    }

    /**
     * @see \Triplestore\Model\Properties::setMax
     */
    public function setMax($property, $max) {
        $this->properties->setMax($property, $max);
    }

    /**
     * @see \Triplestore\Model\Properties::setMin
     */
    public function setMin($property, $min) {
        $this->properties->setMin($property, $min);
    }

    /**
     * Sorts all the properties.
     */
    public function arrangeProperties() {
        $this->properties->arrangeProperties();
    }

    /**
     * Returns the data which of the properties is higher
     *
     * @param string $a
     * @param string $b
     *
     * @return int
     */
    public function sortByProperty($a, $b) {
        return $this->properties->compareProperty($a, $b);
    }

    /**
     * Makes the aliases of the properties
     */
    public function initAliases() {
        $this->properties->initAliases();
    }

    /**
     * Makes sure that if this class is echoed noting bad would happen
     *
     * @return string
     */
    public function __toString() {
        return '';
    }

} 