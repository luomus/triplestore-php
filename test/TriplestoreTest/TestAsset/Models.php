<?php

namespace TriplestoreTest\TestAsset;

use Triplestore\Model\Model;
use Triplestore\Model\Predicate;
use Triplestore\Service\ObjectManager;

class Models {

    const RESOURCE_KEY = '__resource__';
    const MULTI_KEY = 'JA.multi';

    public function getTestData() {
        return array(
            ObjectManager::PREDICATE_TYPE => array(self::RESOURCE_KEY => 'JA.testClass'),
            'JA.edited' => '2013-01-12T06:30:50+0200',
            'JA.dateCreated' => '2012-03-22T11:32:29+0200',
            'JA.hasPeople' => 'true',
            'JA.info' => 'Contour',
            self::MULTI_KEY => array(
                'item1',
                'item2',
                'item3',
                'item4',
            ),
            'JA.address' => array(
                'JA.address' => array( // key tels the type and that this is model
                    array(
                        'JA.street' => 'Mannerheimintie 35',
                        'JA.postNumber' => '00342',
                    ),
                    array(
                        'JA.street' => 'Jousimiehentie 22',
                        'JA.postNumber' => '00310'
                    )
                )
            ),
        );
    }

    public function getTestModel($subject, array $testData = null, $parentSubject = null, $type = null) {
        $model = new Model($subject);
        $data = $testData === null ? $this->getTestData() : $testData;
        if ($parentSubject !== null) {
            $predicate = new Predicate(ObjectManager::PREDICATE_PARENT);
            $predicate->addResourceValue($parentSubject);
            $model->setPredicate($predicate);
        }
        if ($type !== null) {
            $predicate = new Predicate(ObjectManager::PREDICATE_TYPE);
            $predicate->addResourceValue($type);
            $model->setPredicate($predicate);
        }
        foreach ($data as $key => $value) {
            $predicate = new Predicate($key);
            $model->setPredicate($predicate);
            if (is_array($value)) {
                foreach ($value as $key2=>$value2) {
                    if (self::RESOURCE_KEY === $key2) {
                        $predicate->addResourceValue($value2);
                    } else if (is_string($key2) && is_array($value2)) {
                        foreach ($value2 as $key3 => $value3) {
                            if (is_array($value3)) {
                                $predicate->addResourceValue($this->getTestModel($subject . '.' . $key3, $value3, $subject, $key2));
                            } else {
                                $predicate->addResourceValue($this->getTestModel($subject . '.' . $key2, $value2, $subject, $key2));
                                break;
                            }
                        }
                    } else {
                        if (is_array($value2)) {
                            throw new \Exception("Value cannot contain array. Maybe you wanted to add new model? if so use the key '" . self::RESOURCE_KEY . "' key to indicate it");
                        }
                        $predicate->addLiteralValue($value2);
                    }
                }
            } else {
                $predicate->addLiteralValue($value);
            }
        }
        return $model;
    }

    public function getTestClass($subject) {
        $class = new JATestClass();
        $class->setSubject($subject);

        return $class;
    }

    public function createTestModels(ObjectManager $om) {
        $testClass = 'JA.testClass';
        $model = new Model($testClass);
        $predicate = new Predicate('rdf:type');
        $predicate->addResourceValue('rdfs:Class');
        $model->setPredicate($predicate);
        $predicate = new Predicate('rdfs:label');
        $predicate->addLiteralValue('UnitTest class', 'en');
        $model->setPredicate($predicate);
        $om->save($model);

        $model = $this->getTestModel('JA.dateCreated', array(
            'rdf:type' => array(self::RESOURCE_KEY => 'rdf:Property'),
            'rdfs:range' => array(self::RESOURCE_KEY => 'xsd:dateTime'),
            'rdfs:domain' => array(self::RESOURCE_KEY => $testClass),
        ));
        $om->save($model);

        $model = $this->getTestModel('JA.hasPeople', array(
            'rdf:type' => array(self::RESOURCE_KEY => 'rdf:Property'),
            'rdfs:range' => array(self::RESOURCE_KEY => 'xsd:boolean'),
            'xsd:minOccurs' => '0',
            'rdfs:domain' => array(self::RESOURCE_KEY => $testClass),
        ));
        $om->save($model);

        $model = $this->getTestModel('JA.info', array(
            'rdf:type' => array(self::RESOURCE_KEY => 'rdf:Property'),
            'rdfs:range' => array(self::RESOURCE_KEY => 'xsd:string'),
            'xsd:minOccurs' => '0',
            'rdfs:domain' => array(self::RESOURCE_KEY => $testClass),
        ));
        $om->save($model);

        $model = $this->getTestModel(self::MULTI_KEY, array(
            'rdf:type' => array(self::RESOURCE_KEY => 'rdf:Property'),
            'rdfs:range' => array(self::RESOURCE_KEY => 'xsd:string'),
            'xsd:minOccurs' => '0',
            'xsd:maxOccurs' => ObjectManager::OCCURS_UNBOUND,
            'rdfs:domain' => array(self::RESOURCE_KEY => $testClass),
        ));
        $om->save($model);

        $childClass = 'JA.address';
        $model = new Model($childClass);
        $predicate = new Predicate('rdf:type');
        $predicate->addResourceValue('rdfs:Class');
        $model->setPredicate($predicate);
        $predicate = new Predicate('rdfs:label');
        $predicate->addLiteralValue('Phpunit test address', 'en');
        $model->setPredicate($predicate);
        $om->save($model);

        $model = $this->getTestModel('JA.postNumber', array(
            'rdf:type' => array(self::RESOURCE_KEY => 'rdf:Property'),
            'rdfs:range' => array(self::RESOURCE_KEY => 'xsd:integer'),
            'xsd:minOccurs' => 0,
            'rdfs:domain' => array(self::RESOURCE_KEY => $childClass),
        ));
        $om->save($model);

        $model = $this->getTestModel('JA.street', array(
            'rdf:type' => array(self::RESOURCE_KEY => 'rdf:Property'),
            'rdfs:range' => array(self::RESOURCE_KEY => 'xsd:string'),
            'xsd:minOccurs' => 0,
            'xsd:maxOccurs' => ObjectManager::OCCURS_UNBOUND,
            'rdfs:domain' => array(self::RESOURCE_KEY => $childClass),
        ));
        $om->save($model);

        $model = $this->getTestModel('JA.isPartOf', array(
            'rdf:type' => array(self::RESOURCE_KEY => 'rdf:Property'),
            'rdfs:range' => array(self::RESOURCE_KEY => $testClass),
            'rdfs:domain' => array(self::RESOURCE_KEY => $childClass),
        ));
        $om->save($model);


    }

} 