<?php
namespace TriplestoreTest\Model;

use Triplestore\Model\Model;
use Triplestore\Model\TripleObject;
use Triplestore\Model\Predicate;
use TriplestoreTest\TestAsset\JATestClass;

class ModelTest extends \PHPUnit_Framework_TestCase {

    public function setUp() {
        parent::setUp();
    }

    public function testModelSubject() {
        $subject  = "HT.1234";
        $subject2 = "HD.1800";
        $model = new Model($subject);
        $this->assertEquals($model->getSubject(), $subject);
        $model->setSubject($subject2);
        $this->assertEquals($model->getSubject(), $subject2);
    }

    public function testModelIterative() {
        $predicateCount = 50;
        $objectCount    = 3;

        $model = new Model();
        $results = $this->getDummyPredicates($predicateCount, $objectCount);
        foreach ($results as $key => $value) {
            $model->setPredicate($value['pred']);
            unset($results[$key]['pred']);
        }
        $this->assertCount($predicateCount, $model);

        foreach ($model as $predicate) {
            /** @var Predicate $predicate */
            $name = $predicate->getName();
            $this->assertCount($objectCount, $predicate);
            $this->assertArrayHasKey($name, $results);
            foreach($predicate as $object) {
                /** @var TripleObject $object */
                $value = $object->getValue();
                $this->assertArrayHasKey($value, $results[$name]);
                unset($results[$name][$value]);
            }
            $this->assertCount(0, $results[$name]);
            unset($results[$name]);
        }
        $this->assertCount(0, $results);
    }

    private function getDummyPredicates($predicates, $objects) {
        $ret = array();
        for ($i = 0; $i < $predicates; $i++) {
            $name = 'pred_' . $i;
            $ret[$name]['pred'] = new Predicate($name);
            for ($j = 0; $j < $objects; $j ++) {
                $value  = 'val_' . $j;
                $object = new TripleObject($value);
                $ret[$name]['pred']->addObject($object);
                $ret[$name][$value] = $object;
            }
        }

        return $ret;
    }

    public function testModelCloning() {
        $model = new Model();
        $pred  = 'testPred';
        $value = 'testValue';
        $model->getOrCreatePredicate($pred)->addLiteralValue($value);
        $clone = clone $model;
        $this->assertTrue($clone->hasPredicate($pred));
        $clonePred = $clone->getPredicate($pred);
        $modelPred = $model->getPredicate($pred);
        $this->assertNotSame($clonePred, $modelPred);
        $cloneObj = $clonePred->current();
        $modelObj = $modelPred->current();
        $this->assertNotSame($cloneObj, $modelObj);
        $this->assertEquals($cloneObj->getValue(), $modelObj->getValue());
        $this->assertEquals($cloneObj->getValue(), $value);
        $this->assertEquals($cloneObj->getLang(), $modelObj->getLang());
        $this->assertEquals($cloneObj->getStatementId(), $modelObj->getStatementId());
        $this->assertEquals($cloneObj->getType(), $modelObj->getType());
    }

    public function testGettingPredicate() {
        $model = new Model();
        $this->assertTrue($model->getOrCreatePredicate('nonExisting') instanceof Predicate);
        $this->assertNull($model->getPredicate('anotherNotYetInModel'));
    }

    public function testGettingExistingPredicate() {
        $model = new Model();
        $predicate = new Predicate('test');
        $model->setPredicate($predicate);
        $this->assertSame($model->getOrCreatePredicate('test'), $predicate);
    }

    /**
     * @expectedException \Triplestore\Exception\InvalidArgumentException
     */
    public function testAddingWithInvalidLanguage() {
        $model = new JATestClass();
        $this->assertNull($model->getJAHasPeople());
        $model->setJAHasPeople(false, 'non');
    }

    /**
     * @expectedException \Triplestore\Exception\InvalidArgumentException
     */
    public function testAddingInvalidLanuguageWithMethod() {
        $model = new JATestClass();
        $model->setJAHasPeopleEn(true);
    }

}
 