<?php

namespace TriplestoreTest\Service;

use Triplestore\Model\Metadata;
use Triplestore\Model\Properties;
use Triplestore\Factory\ObjectManagerFactory;
use TriplestoreTest\Bootstrap;
use TriplestoreTest\TestAsset\Models;

class MetadataServiceTest extends \PHPUnit_Framework_TestCase {
    protected $traceError = true;
    /** @var \Triplestore\Service\MetadataService */
    protected $metadataService;

    public static function setUpBeforeClass() {
        $serviceManager = Bootstrap::getServiceManager();
        $factory = new ObjectManagerFactory();
        $om = $factory->createService($serviceManager);
        $modelTool = new Models();
        $modelTool->createTestModels($om);
    }

    public function setUp()
    {
        parent::setUp();
        $serviceManager = Bootstrap::getServiceManager();
        $factory = new ObjectManagerFactory();
        $om = $factory->createService($serviceManager);
        $om->setClassNamespace('TriplestoreTest\TestAsset');
        $this->metadataService = $om->getMetadataService();
    }

    public function testGettingMetadataForAllClasses() {
        $all = $this->metadataService->getAllClasses();
        $this->assertArrayHasKey('JA.testClass', $all);
        $this->assertArrayNotHasKey('JATestClass', $all);
        $this->assertGreaterThan(2, count($all));
    }

    public function testGettingClassMetadata() {
        $metadata = $this->metadataService->getMetadataFor('JA.testClass');
        $this->assertInstanceOf('Triplestore\Model\Metadata', $metadata);

        return $metadata;
    }

    /**
     * @depends testGettingClassMetadata
     */
    public function testMetadataContent(Metadata $metadata) {
        $predicate = 'JA.multi';
        $this->assertTrue($metadata->isLiteral($predicate));
        $this->assertTrue($metadata->hasMany($predicate));
        $this->assertFalse($metadata->isRequired($predicate));
        $this->assertFalse($metadata->isConvertible($predicate));
        $this->assertFalse($metadata->isMultiLanguage($predicate));

        $predicate = 'JA.dateCreated';
        $this->assertTrue($metadata->isLiteral($predicate));
        $this->assertFalse($metadata->hasMany($predicate));
        $this->assertTrue($metadata->isRequired($predicate));
        $this->assertTrue($metadata->isConvertible($predicate));
    }

    public function testGetMetadataWithAlias() {
        $metadata = $this->metadataService->getMetadataFor('JAAddress');
        $this->assertInstanceOf('Triplestore\Model\Metadata', $metadata);

        $pred = 'JA.isPartOf';
        $this->assertFalse($metadata->isLiteral($pred));
        $this->assertTrue($metadata->isRequired($pred));
    }

    public function testMetadataPropertiesWakeFromCache() {
        Properties::instance()->destroy();
        $serviceManager = Bootstrap::getServiceManager();
        $factory = new ObjectManagerFactory();
        $om = $factory->createService($serviceManager);
        $metadataService = $om->getMetadataService();
        $metadata = $metadataService->getMetadataFor('JA.address');
        /** @var \Triplestore\Model\Properties $properties */
        $properties = $metadata->getAllProperties();
        $this->assertInstanceOf('Triplestore\Model\Properties', $properties);
        $this->assertGreaterThan(2, count($properties->getProperties()));

    }

}
 