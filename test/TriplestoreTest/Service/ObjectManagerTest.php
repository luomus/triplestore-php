<?php

namespace TriplestoreTest\Service;

use Triplestore\Db\TriplestoreInterface;
use Triplestore\Model\Model;
use Triplestore\Model\TripleObject;
use Triplestore\Model\Predicate;
use Triplestore\Service\ObjectManager;
use Triplestore\Stdlib\ModelTools;
use TriplestoreTest\Bootstrap;
use TriplestoreTest\TestAsset\JATestClass;
use TriplestoreTest\TestAsset\Models;

class ObjectManagerTest extends \PHPUnit_Framework_TestCase {
    protected $traceError = true;
    /** @var \Triplestore\Service\ObjectManager */
    protected $om;
    protected $testSubject = 'TEST.123';
    /** @var \TriplestoreTest\TestAsset\Models */
    protected $testAsset;

    public function setUp()
    {
        parent::setUp();
        $serviceManager = Bootstrap::getServiceManager();
        $this->om = $serviceManager->get('TripleStore\ObjectManager');
        $this->om->setClassNamespace('TriplestoreTest\TestAsset');
        $this->om->disableHydrator();
        $this->testAsset = new Models();
    }

    public function testFetchingNonExisting() {
        $item = $this->om->fetch($this->testSubject);
        $this->assertNull($item, "Fetching didn't return null with non existing subject");
    }

    /**
     * @expectedException \Triplestore\Exception\InvalidArgumentException
     */
    public function testAddingModelWithNoSubject() {
        $model = new Model();
        $this->om->save($model);
    }

    /**
     * @expectedException \Triplestore\Exception\InvalidArgumentException
     */
    public function testAddingModelWithEmptySubject() {
        $model = new Model('   ');
        $this->om->save($model);
    }

    public function testAddingModelWithNoPredicates() {
        $model = new Model($this->testSubject);
        $this->assertTrue($this->om->save($model));
        $this->assertNull($this->om->fetch($this->testSubject));
    }

    public function testAddingNewModel() {
        $model = new Model($this->testSubject);
        $predicate1 = new Predicate('MZ.dateEdited');
        $predicate2 = new Predicate('MZ.dateCreated');
        $object1 = new TripleObject('MY.status');
        $object2 = new TripleObject('literal', 'en', null, TripleObject::LITERAL);
        $predicate1->addObject($object1);
        $predicate1->addObject($object2);
        $predicate2->addObject($object1);
        $predicate2->addObject($object2);
        $model->setPredicate($predicate1);
        $model->setPredicate($predicate2);
        $this->assertTrue($this->om->save($model), 'Failed to save the model to the database');

        $resultModel = $this->om->fetch($this->testSubject);
        $this->assertTrue(ModelTools::compare($model, $resultModel), "Models didn't match");

        $resultModel = $this->om->fetch($this->testSubject, false);
        $this->assertTrue(ModelTools::compare($model, $resultModel), "Models didn't match");
        return $model;
    }

    /**
     * @depends testAddingNewModel
     */
    public function testFetchingTwoModelsOnce($testModel) {
        $testSubject = 'TEST.124';
        $model = new Model($testSubject);
        $predicate1 = new Predicate('MZ.dateEdited');
        $predicate2 = new Predicate('MZ.dateCreated');
        $object1 = new TripleObject('MY.status');
        $object2 = new TripleObject('literal2', 'en', null, TripleObject::LITERAL);
        $predicate1->addObject($object1);
        $predicate1->addObject($object2);
        $predicate2->addObject($object1);
        $predicate2->addObject($object2);
        $model->setPredicate($predicate1);
        $model->setPredicate($predicate2);
        $this->assertTrue($this->om->save($model), 'Failed to save the model to the database');

        $models = $this->om->fetch([$this->testSubject, $testSubject]);
        $this->assertCount(2, $models);
        $this->assertArrayHasKey($model->getSubject(), $models);
        $this->assertArrayHasKey($this->testSubject, $models);
        $this->assertTrue(ModelTools::compare($model,$models[$testSubject]), "Second model didn't match");
        $this->assertTrue(ModelTools::compare($testModel,$models[$testModel->getSubject()]), "Second model didn't match");

        $models = $this->om->fetch([$this->testSubject, $testSubject], false);
        $this->assertCount(2, $models);
        $this->assertArrayHasKey($model->getSubject(), $models);
        $this->assertArrayHasKey($this->testSubject, $models);
        $this->assertTrue(ModelTools::compare($model,$models[$testSubject]), "Second model didn't match");
        $this->assertTrue(ModelTools::compare($testModel,$models[$testModel->getSubject()]), "Second model didn't match");

        // Delete the test object
        $this->assertTrue($this->om->remove($model));
        $this->assertNull($this->om->fetch($testSubject));
    }

    /**
     * @depends testAddingNewModel
     */
    public function testAddingNewModelAgain($model) {
        $this->om->clear();
        $this->assertTrue($this->om->save($model));
        return $model;
    }

    /**
     * @depends testAddingNewModelAgain
     */
    public function testFetchingSinglePredicate(Model $model) {
        $subject = $model->getSubject();
        $predicateOld = $model->getPredicate('MZ.dateEdited');
        $predicateNew = $this->om->getPredicate($subject, 'MZ.dateEdited');
        $this->assertNotNull($predicateNew);
        $this->assertInstanceOf('Triplestore\Model\Predicate', $predicateNew);
        $this->assertTrue(ModelTools::comparePredicate($predicateOld, $predicateNew));

        return $model;
    }

    /**
     * @depends testFetchingSinglePredicate
     */
    public function testUpdatingSinglePredicate(Model $model) {
        $subject      = $model->getSubject();
        $predicateOld1 = $model->getPredicate('MZ.dateEdited');
        $predicateOld2 = $model->getPredicate('MZ.dateCreated');
        $predicateNew = new Predicate('MZ.dateEdited');
        $object       = new TripleObject('MZ.dateEdited');
        $predicateNew->addObject($object);
        $this->om->fetch($subject);

        $this->assertCount(2, $predicateOld1);
        $this->assertTrue($this->om->updatePredicate($subject, $predicateNew));

        $newModel = $this->om->fetch($subject);
        $newPredicate1 = $newModel->getPredicate('MZ.dateEdited');
        $newPredicate2 = $newModel->getPredicate('MZ.dateCreated');

        $this->assertFalse(ModelTools::comparePredicate($predicateOld1, $newPredicate1));
        $this->assertTrue(ModelTools::comparePredicate($predicateNew, $newPredicate1));
        $this->assertCount(2, $newModel);
        $this->assertTrue(ModelTools::comparePredicate($newPredicate2, $predicateOld2));

        return $model;
    }

    /**
     * @depends testUpdatingSinglePredicate
     */
    public function testDeletingWithModelThatHasNoStatementIds($model) {
        $this->assertInstanceOf('Triplestore\Model\Model', $model);
        $had = false;
        foreach ($model as $predicate) {
            foreach ($predicate as $object) {
                /** @var \Triplestore\Model\TripleObject $object */
                $this->assertNull($object->getStatementId());
                $had = true;
                break;
            }
            break;
        }
        $this->assertTrue($had, "Model didn't have any objects in it");
        $this->assertTrue($this->om->remove($model));
        $this->assertNull($this->om->fetch($this->testSubject));
    }

    public function testAddingModelWithAnotherInModel() {
        $model = $this->testAsset->getTestModel($this->testSubject);
        $this->makeSureAllResourcesExistInDb($model, $this->om->getConnection());
        $this->assertTrue($this->om->save($model));
        $resultModel = $this->om->fetch($this->testSubject);
        $this->assertNotNull($resultModel);
        $this->assertTrue(ModelTools::compare($model, $resultModel), "Models didn't match");
    }

    /**
     * @depends testDeletingWithModelThatHasNoStatementIds
     */
    public function testDeletingWithSubject() {
        $this->assertInstanceOf('Triplestore\Model\Model', $this->om->fetch($this->testSubject));
        $this->assertTrue($this->om->remove($this->testSubject));
        $this->assertNull($this->om->fetch($this->testSubject));
    }

    public function testExceptionShowsReasonForFailure() {
        $name = 'NON.existing';
        $model = new Model($this->testSubject);
        $predicate = new Predicate($name);
        $predicate->addObject(new TripleObject('TEST', null, null, TripleObject::LITERAL));
        $model->setPredicate($predicate);
        $this->assertNull($this->om->fetch($this->testSubject));
        $this->assertFalse($this->om->save($model));
        $this->assertContains($name, $this->om->getReasonForError());
    }

    /**
     * @depends testExceptionShowsReasonForFailure
     */
    public function testRollbackOnFailure() {
        $this->assertNull($this->om->fetch($this->testSubject));
    }

    public function testHydratedClass() {
        $this->om->enableHydrator();
        $testClass = new JATestClass();
        $testClass->setSubject($this->testSubject);
        $dateTime = new \DateTime();
        $multi = array(
            'test1',
            'test2',
            'test3'
        );
        $testClass->setJADateCreated($dateTime);
        $testClass->setJAHasPeople(true);
        $testClass->setJAMulti($multi);
        $this->assertTrue($this->om->save($testClass));

        /** @var JATestClass $storeClass */
        $storeClass = $this->om->fetch($this->testSubject);
        $this->assertInstanceOf('TriplestoreTest\TestAsset\JATestClass', $storeClass);
        $this->assertInstanceOf('DateTime', $storeClass->getJADateCreated());
        $this->assertTrue($storeClass->getJAHasPeople());
        $multiResult = $storeClass->getJAMulti();
        $checks = array_flip($multiResult);
        foreach ($multi as $value) {
            $this->assertArrayHasKey($value, $checks);
        }

        return $testClass;
    }

    /**
     * @depends testHydratedClass
     */
    public function testDeletingWithClass($class) {
        $this->assertTrue($this->om->remove($class));
        $this->assertNull($this->om->fetch($this->testSubject));
    }

    public function testGettingMetadataSerive() {
        $metadataService = $this->om->getMetadataService();
        $this->assertInstanceOf('Triplestore\Service\MetadataService', $metadataService);
    }

    public function testSetClassName() {
        $this->testSubject = 'JA.1';
        $model = $this->testAsset->getTestModel($this->testSubject);
        $this->om->save($model);
        $this->om->enableHydrator();
        $this->om->className('TriplestoreTest\TestAsset\JAAddress');
        $hydModel = $this->om->fetch($this->testSubject);
        $this->assertNull($hydModel);
        $this->om->className('TriplestoreTest\TestAsset\JATestClass');
        $hydModel = $this->om->fetch($this->testSubject);
        $this->assertInstanceOf('TriplestoreTest\TestAsset\JATestClass', $hydModel);
    }

    /**
     * @depends testSetClassName
     */
    public function testFindingWithLiteralCriteria() {
        $this->testSubject = array();
        $this->testSubject[] = 'JA.1';
        $this->testSubject[] = 'JA.2';
        $this->testSubject[] = 'JA.3';
        $this->testSubject[] = 'JA.4';
        $this->om->className('TriplestoreTest\TestAsset\JATestClass');
        $this->om->disableHydrator();
        $model2 = $this->testAsset->getTestModel('JA.2');
        $model2->getOrCreatePredicate(Models::MULTI_KEY)->setLiteralValue(array(
            'item1',
            'item3',
            'item5',
        ));
        $this->om->save($model2);
        $model3 = $this->testAsset->getTestModel('JA.3');
        $model3->getOrCreatePredicate(Models::MULTI_KEY)->setLiteralValue(array(
            'item1',
            'item2',
            'item5',
        ));
        $model3->getOrCreatePredicate(Models::MULTI_KEY)->addLiteralValue('item6');
        $this->om->save($model3);
        $this->om->enableHydrator();
        $models = $this->om->findBy(array(Models::MULTI_KEY => 'item1'));
        $this->assertCount(3, $models);

        $models = $this->om->findBy(array(Models::MULTI_KEY => 'item3'));
        $this->assertCount(2, $models);
        $this->assertArrayHasKey('JA.1', $models);
        $this->assertArrayHasKey('JA.2', $models);

        $models = $this->om->findBy(array(Models::MULTI_KEY => 'item5'));
        $this->assertCount(2, $models);
        $this->assertArrayHasKey('JA.2', $models);
        $this->assertArrayHasKey('JA.3', $models);

        $models = $this->om->findBy(array(Models::MULTI_KEY => 'item6'));
        $this->assertCount(1, $models);
        $this->assertArrayHasKey('JA.3', $models);
        $this->assertInstanceOf('TriplestoreTest\TestAsset\JATestClass', $models['JA.3']);

        $model4 = $this->testAsset->getTestModel('JA.4');
        $model4->getOrCreatePredicate(Models::MULTI_KEY)->addLiteralValue('item6');
        $model4->getOrCreatePredicate('JA.hasPeople')->setLiteralValue('false');
        $this->om->save($model4);

        $models = $this->om->findBy(array(Models::MULTI_KEY => 'item6'));
        $this->assertCount(2, $models);
        $this->assertArrayHasKey('JA.3', $models);
        $this->assertArrayHasKey('JA.4', $models);

        $models = $this->om->findBy(array(Models::MULTI_KEY => 'item1', 'JA.hasPeople' => 'true'));
        $this->assertCount(3, $models);

        $models = $this->om->findBy(array(Models::MULTI_KEY => 'item1', 'JA.hasPeople' => 'false'));
        $this->assertCount(1, $models);

        return $this->testSubject;
    }

    /**
     * @depends testFindingWithLiteralCriteria
     */
    public function testRemoveLoseEnds($subjects) {
        foreach ($subjects as $subject) {
            $this->om->remove($subject);
        }
    }

    /**
     * This makes sure that no lose ends are left in to the database
     * by unit tests.
     *
     * @param \Exception $e
     * @throws \Exception
     */
    protected function onNotSuccessfulTest(\Exception $e) {
        $remove = $this->testSubject;
        if (!is_array($remove)) {
            $remove = array($remove);
        }
        if ($this->om instanceof ObjectManager) {
            foreach ($remove as $subject) {
                $this->om->remove($subject);
            }
        }
        throw $e;
    }

    public function testModelWithääkköset() {
        $model = new Model($this->testSubject);
        $predicate = new Predicate('MZ.editor');
        $name = 'Ääkkönen Töpå';
        $object = new TripleObject($name, 'en', null, TripleObject::LITERAL);
        $predicate->addObject($object);
        $model->setPredicate($predicate);
        $this->assertTrue($this->om->save($model));

        $resultModel = $this->om->fetch($this->testSubject);
        $this->assertTrue(ModelTools::compare($model, $resultModel), "Models didn't match");
        $this->om->remove($this->testSubject);
    }

    private function makeSureAllResourcesExistInDb(Model $model, TriplestoreInterface $db) {
        foreach ($model as $predicate) {
            /** @var \Triplestore\Model\Predicate $predicate */
            $name = $predicate->getName();
            if (!$db->hasResource($name)) {
                $db->addResource($name);
            }
            foreach ($predicate as $object) {
                /** @var \Triplestore\Model\TripleObject $object */
                if (!$object->isLiteral()) {
                    $value = $object->getValue();
                    if ($value instanceof Model) {
                        $this->makeSureAllResourcesExistInDb($value, $db);
                    } else if (!$db->hasResource($value)) {
                        $db->addResource($value);
                    }
                }
            }
        }
    }

}
 