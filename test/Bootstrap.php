<?php

namespace TriplestoreTest;

use RuntimeException;
use Zend\Loader\AutoloaderFactory;
use Zend\Mvc\Service\ServiceManagerConfig;
use Zend\ServiceManager\ServiceManager;

error_reporting(E_ALL | E_STRICT);
chdir(__DIR__);

/**
 * Test bootstrap, for setting up autoloading
 */
class Bootstrap
{
    protected static $serviceManager;

    public static function init()
    {

        static::initAutoloader();

        $configFiles = [
            __DIR__ . '/TestConfiguration.php',
            __DIR__ . '/TestConfiguration.php.dist'
        ];

        foreach ($configFiles as $configFile) {
            if (file_exists($configFile)) {
                $config = require $configFile;
                break;
            }
        }

        $serviceManager = new ServiceManager(new ServiceManagerConfig());
        $serviceManager->setService('ApplicationConfig', $config);
        $serviceManager->get('ModuleManager')->loadModules();
        static::$serviceManager = $serviceManager;
    }

    public static function chroot()
    {
        chdir(__DIR__ . '/../');
    }

    public static function getServiceManager()
    {
        return static::$serviceManager;
    }

    protected static function initAutoloader()
    {
        $files = [
            __DIR__ . '/../vendor/autoload.php',
            __DIR__ . '/../../../autoload.php'
        ];

        foreach ($files as $file) {
            if (file_exists($file)) {
                $loader = require $file;
                break;
            }
        }

        if (! isset($loader)) {
            throw new RuntimeException('vendor/autoload.php could not be found. Did you install via composer?');
        }

        $loader->add('TriplestoreTest\\', __DIR__);
    }
}

Bootstrap::init();
Bootstrap::chroot();