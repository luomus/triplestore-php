<?php

return [
    'module_listener_options' => [
        'config_glob_paths'    => [
            __DIR__ . '/../config/testing.{global,local,phpunit}.php',
        ],
        'module_paths' => []
    ],
    'modules' => [
        'Triplestore'
    ]
];